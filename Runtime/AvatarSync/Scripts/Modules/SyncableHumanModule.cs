﻿using UnityEngine;

namespace AvatarSync
{
    public abstract class SyncableHumanModule : ScriptableObject, ISyncableModule<SyncableHumanAvatar>
    {
        public abstract void Execute(SyncableHumanAvatar syncable);
    }
}