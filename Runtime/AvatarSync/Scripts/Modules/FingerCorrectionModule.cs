﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AvatarSync
{
    [System.Serializable]
    public struct FingerRotationCorrection
    {
        public HumanBodyBones bone;
        public Quaternion rotation;
    }


    [CreateAssetMenu(menuName = "AvatarSync/Finger correction", fileName = "New finger correction settings")]
    public class FingerCorrectionModule : SyncableHumanModule
    {

        public FingerRotationCorrection[] corrections;

        public override void Execute(SyncableHumanAvatar avatar)
        {
            Transform bone = null;

            foreach (FingerRotationCorrection correction in corrections)
            {
                bone = avatar.animator.GetBoneTransform(correction.bone);
                if (bone != null)
                {
                    bone.localRotation = correction.rotation;
                }

            }

        }
    }
}