﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISyncableModule<T> where T: SyncableBase
{
    void Execute(T syncable);
    
}
