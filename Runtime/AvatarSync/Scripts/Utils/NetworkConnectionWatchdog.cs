﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkConnectionWatchdog : MonoBehaviour
{
    public string lastServerAddress;

    IEnumerator reconnectCoroutine = null;

    NetworkManager manager;    

    private void Awake()
    {
        manager = GameObject.FindObjectOfType<NetworkManager>();
    }

    public void OnClientStart()
    {
        Debug.Log("Watchdog: Client started");
        lastServerAddress = manager.networkAddress;
    }

    public void OnClientDisconnect()
    {
        Debug.Log("Watchdog: Client disconnected");
        TryReconnect();
    }
    

    void TryReconnect()
    {
        Debug.Log("Connection to server is lost or not possible, trying to reconnect");
        //MessagePanel.AddMessage("Connection to the server is lost. Trying to reconnect automatically...");
        if (reconnectCoroutine != null)
        {
            StopCoroutine(reconnectCoroutine);
        }

        reconnectCoroutine = TryReconnectSequence();
        StartCoroutine(reconnectCoroutine);
    }
    
    IEnumerator TryReconnectSequence()
    {
        manager.StopClient();          

        yield return new WaitForSeconds(2f);

        manager.StartClient();

        yield return new WaitForSeconds(10f);

        if ((!NetworkClient.active && !NetworkClient.isConnected))
        {
            Debug.Log("Watchdog: Still no connection to the server, will retry the process");
            TryReconnect();
        }
        
        if(NetworkClient.active && !NetworkClient.isConnected)
        {
            Debug.Log("Watchdog: Is trying to reconnect unsuccessfuly, retrying");
            TryReconnect();
        }
        

        if(NetworkClient.active && NetworkClient.isConnected && !NetworkClient.connection.isReady)
        {
            Debug.Log("Client not ready");                        
            TryReconnect();
        }

        Debug.Log("Watchdog network active:" + NetworkClient.active);
        Debug.Log("Watchdog connected:" + NetworkClient.isConnected);

        yield return null;
    }
    
}
