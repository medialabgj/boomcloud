﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionsMethods
{
    public static float Truncate(this float value, int digits)
    {
        double mult = System.Math.Pow(10.0, digits);
        double result = System.Math.Truncate(mult * value) / mult;     
        return (float)result;
    }

    public static Vector3 Truncate(this Vector3 value, int digits)
    {
        return new Vector3(value.x.Truncate(digits), value.y.Truncate(digits), value.z.Truncate(digits));
    }

    public static Quaternion Truncate(this Quaternion value, int digits)
    {
        return new Quaternion(value.x.Truncate(digits), value.y.Truncate(digits), value.z.Truncate(digits), value.w.Truncate(digits));
    }
}
