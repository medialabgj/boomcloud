﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.Events;
public class CustomNetworkManager : NetworkManager
{
    public UnityEvent ClientStarted;
    public UnityEvent ClientDisconnected;
    public UnityEvent ServerStarted;

    public static event System.Action<string> ClientStartAction;
    public static event System.Action ServerStartAction;
    public static event System.Action<string> ClientConnectAction;
    public static event System.Action ClientDisconnectAction;
    public static event System.Action<string> ServerDisconnectAction;
    public static event System.Action ServerStopAction;
    public static event System.Action ClientStopAction;

    public override void OnStartClient()
    {
        base.OnStartClient();
        ClientStarted?.Invoke();
        ClientStartAction?.Invoke(networkAddress);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);
        ServerDisconnectAction?.Invoke(conn.address);
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        ClientDisconnected?.Invoke();
        ClientDisconnectAction?.Invoke();
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        ServerStarted?.Invoke();
        ServerStartAction?.Invoke();
    }

    public override void OnStopClient()
    {
        base.OnStopClient();
        ClientStopAction?.Invoke();
    }

    public override void OnStopServer()
    {
        base.OnStopServer();
        ServerStartAction?.Invoke();
    }

}
