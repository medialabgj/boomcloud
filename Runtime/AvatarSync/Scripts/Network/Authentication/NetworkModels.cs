﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;


namespace Comedie.Network
{
    public enum ComedieRole { Guest, User, Admin }

    [System.Serializable]
    public class ComedieUser
    {
        public string nickname;
        public string username;
        public string password;
        public ComedieRole role;
    }

    [System.Serializable]
    public class ComedieAuthenticatedUser
    {
        public int connection;
        public string username;
        public string role;
        public string address;
    }
}

