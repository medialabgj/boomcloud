﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Comedie.Network;

public class TokenAuthenticator : NetworkAuthenticator
{
    [Header("Custom Properties")]
    public string token;
    public class AuthRequestMessage : MessageBase
    {
        public string authToken;
    }

    public class AuthResponseMessage : MessageBase
    {
        public byte code;
        public string message;
        public string role;
    }

    public override void OnStartClient()
    {
        NetworkClient.RegisterHandler<AuthResponseMessage>(OnAuthResponseMessage, false);
    }

    public override void OnStartServer()
    {
             
        NetworkServer.RegisterHandler<AuthRequestMessage>(OnAuthRequestMessage, false);
    }

    public override void OnServerAuthenticate(NetworkConnection conn)
    {

    }

    public override void OnClientAuthenticate(NetworkConnection conn)
    {
        AuthRequestMessage authRequestMessage = new AuthRequestMessage
        {
            authToken = token
        };

        conn.Send(authRequestMessage);
    }

    public virtual void OnAuthRequestMessage(NetworkConnection conn, AuthRequestMessage msg)
    {
        //Always accept in this version
        AuthResponseMessage authResponseMessage = new AuthResponseMessage
        {
            code = 100,
            message = "Success: Loggest as visitor",
            role = "visitor"            
        };

        conn.Send(authResponseMessage);

        // Invoke the event to complete a successful authentication
        OnServerAuthenticated.Invoke(conn);                   

    }

    public virtual void OnAuthResponseMessage(NetworkConnection conn, AuthResponseMessage msg)
    {        
        if (msg.code == 100)
        {
            Debug.LogFormat("Authentication Response: {0}", msg.message);
            OnClientAuthenticated.Invoke(conn);            
        }
        else
        {
            Debug.LogErrorFormat("Authentication Response: {0}", msg.message);           
            conn.isAuthenticated = false;
            conn.Disconnect();
        }
    }
}
