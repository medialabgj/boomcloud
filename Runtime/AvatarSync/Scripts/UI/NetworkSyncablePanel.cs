﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AvatarSync
{
    public class NetworkSyncablePanel : MonoBehaviour
    {
        private string _identifier;

        private Image background;

        public Text syncableNameText;
        public Text syncableTypeText;
        public Toggle isSenderToggle;
        public Toggle isReadyToggle;

        public SyncableBase syncable { get; private set; }

        //public Outline syncableOutline;

        public Transform cameraRig;
        Vector2 dragStartPoint;
        Vector2 dragEndPoint;

        private void Awake()
        {
            background = GetComponent<Image>();
            if (cameraRig == null)
            {
                cameraRig = GameObject.Find("CameraRig")?.transform;
            }
        }

        public void SetSyncable(SyncableBase _syncable)
        {
            syncable = _syncable;
            UpdateLink();
        }

        private void UpdateLink()
        {

            if (syncable != null)
            {
                syncableNameText.text = syncable.identifier;

                if ((syncable as SyncableAvatar) != null)
                {
                    syncableTypeText.text = "Avatar";
                }

                if ((syncable as SyncableProp) != null)
                {
                    syncableTypeText.text = "Props";
                }

                if ((syncable as SyncableHumanAvatar) != null)
                {
                    syncableTypeText.text = "Human";
                }

                isSenderToggle.isOn = syncable.isSender;
                isSenderToggle.onValueChanged.AddListener(OnIsSenderValueChange);

                isReadyToggle.isOn = syncable.isReady;
                isReadyToggle.onValueChanged.AddListener(OnIsReadyValueChange);

                UpdateAppearance();
            }
        }

        void OnIsSenderValueChange(bool value)
        {
            syncable.isSender = value;
            UpdateAppearance();
        }

        void OnIsReadyValueChange(bool value)
        {
            syncable.isReady = value;
            UpdateAppearance();
        }

        private void Start()
        {
            UpdateLink();
        }

        public void UpdateAppearance()
        {
            GetComponent<CanvasGroup>().alpha = (syncable.isSender) ? 1f : .8f;

            UpdateOutline();

            if (syncable.isSender)
            {
                syncableNameText.color = syncable.isReady ? Color.green : Color.red;
            }
            else
            {
                syncableNameText.color = Color.gray;
            }

            isReadyToggle.enabled = syncable.isSender;


        }

        public void UpdateOutline()
        {
            /*
            if (syncableOutline == null)
            {
                syncableOutline = syncable.gameObject.AddComponent<Outline>();
                syncableOutline.OutlineWidth = 1;
            }

            if (syncable.isSender)
            {
                if (syncable.isReady)
                {
                    syncableOutline.OutlineColor = Color.green;
                }
                else
                {
                    syncableOutline.OutlineColor = Color.red;
                }
            }
            else
            {
                syncableOutline.OutlineColor = Color.yellow;
            }
            */
        }

        /*
        private void Update()
        {
            if(cameraRig != null)
            {
                if(Input.GetMouseButtonDown(0))
                {
                    dragStartPoint = Input.mousePosition;
                }

                if (Input.GetMouseButton(0))
                {
                    dragEndPoint = Input.mousePosition;
                    Vector3 diff = Vector3.ClampMagnitude(Vector3.Scale(dragEndPoint - dragStartPoint, Vector3.one * .2f), 1f);
                    cameraRig.transform.position += new Vector3(diff.x, 0, -diff.y) * Time.deltaTime;
                }
            }
        }
        */
    }
}