﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AvatarSync
{
    public class SyncableLogUI : MonoBehaviour
    {
        public Text logText;
        public Text availabilityText;

        private void Awake()
        {
            logText.text = "";
            availabilityText.text = "";
        }

        void Log(string value)
        {
            logText.text += "\n" + "[" + System.DateTime.Now.ToShortTimeString() + "] " + value;
        }

        private void OnEnable()
        {
            AvatarSynchronizer.SyncableAvailabilityChange += OnSyncableAvailabilityChange;
            AvatarSynchronizer.ClientStarted += OnClientStarted;
            AvatarSynchronizer.ServerStarted += OnServerStarted;
            CustomNetworkManager.ServerDisconnectAction += OnServerDisconnectAction;
            CustomNetworkManager.ClientDisconnectAction += OnClientDisconnectAction;
            CustomNetworkManager.ServerStopAction += OnServerStop;
            CustomNetworkManager.ClientStopAction += OnClientStop;
        }

        private void OnDisable()
        {
            AvatarSynchronizer.SyncableAvailabilityChange -= OnSyncableAvailabilityChange;
            AvatarSynchronizer.ClientStarted -= OnClientStarted;
            AvatarSynchronizer.ServerStarted -= OnServerStarted;
            CustomNetworkManager.ServerDisconnectAction -= OnServerDisconnectAction;
            CustomNetworkManager.ClientDisconnectAction -= OnClientDisconnectAction;
            CustomNetworkManager.ServerStopAction -= OnServerStop;
            CustomNetworkManager.ClientStopAction -= OnClientStop;
        }

        void OnClientStarted()
        {
            Log("Client started successfully");
        }

        void OnServerStarted()
        {
            Log("Server started successfully");
        }

        void OnServerDisconnectAction(string address)
        {
            Log($"Client {address} disconnected from server");
        }

        void OnClientDisconnectAction()
        {
            Log("Disconnected from server");
        }

        void OnServerStop()
        {
            Log("Server stopped");
        }

        void OnClientStop()
        {
            Log("Client stopped");
        }

        void OnSyncableAvailabilityChange(string[] availabilities)
        {
            if (availabilities.Length >= 3)
            {
                availabilityText.text = "Avatars:" + availabilities[0] + "\nHumans:" + availabilities[1] + "\nProps:" + availabilities[2];
            }
        }
    }
}