﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

using Mirror;

namespace AvatarSync
{
    public class SyncableManagementUI : MonoBehaviour
    {
        [Header("Main Panel")]
        public Button startButton;
        public Button stopButton;
        public Text statusText;

        [Header("Setup")]
        public GameObject setupPanel;
        public InputField hostAddressInputField;
        public Dropdown networkModeDropdown;
        public Dropdown synchronizerModeDropdown;
        public Dropdown networkChannelDropdown;
        public Toggle compressDataToggle;
        public Toggle autoStartToggle;
        public Toggle logAnalyticsToggle;

        [Header("Syncables")]
        public GameObject networkSyncablePanelPrefab;
        public GameObject syncablesPanel;
        public RectTransform syncablePanelContainer;


        public AvatarSyncConfigurator configurator;

        [Header("Auto-linked")]
        public CustomNetworkManager networkManager;
        public AvatarSynchronizer avatarSynchronizer;

        bool globalAllReady = false;

        List<NetworkSyncablePanel> syncablePanels;

        private void Awake()
        {
            networkManager = GameObject.FindObjectOfType<CustomNetworkManager>();
            avatarSynchronizer = GameObject.FindObjectOfType<AvatarSynchronizer>();
            syncablePanels = new List<NetworkSyncablePanel>();
            networkChannelDropdown.ClearOptions();
            networkChannelDropdown.AddOptions(networkManager.GetComponent<IgnoranceThreaded>().Channels.Select(c => c.ToString()).ToList());
            LoadConfiguration();
        }

        void Start()
        {
            if (configurator.config.autoStart)
            {
                StartSync();
            }

            syncablesPanel.SetActive(false);

        }

        private void OnEnable()
        {
            SyncableAvatar.SyncableReady += OnSyncableReady;
            SyncableHumanAvatar.SyncableReady += OnSyncableReady;
            SyncableProp.SyncableReady += OnSyncableReady;
        }

        private void OnDisable()
        {
            SyncableAvatar.SyncableReady -= OnSyncableReady;
            SyncableHumanAvatar.SyncableReady -= OnSyncableReady;
            SyncableProp.SyncableReady -= OnSyncableReady;
        }

        void OnSyncableReady(SyncableBase syncable)
        {
            if (configurator.config.senders.Contains(syncable.identifier))
            {
                syncable.isSender = true;
            }
            NetworkSyncablePanel nsp = GameObject.Instantiate(networkSyncablePanelPrefab, syncablePanelContainer).GetComponent<NetworkSyncablePanel>();
            nsp.SetSyncable(syncable);
            syncablePanels.Add(nsp);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void FreezeReadyStates()
        {
            configurator.config.senders = GameObject.FindObjectsOfType<SyncableBase>().Where(s => s.isSender).Select(s => s.identifier).ToArray();
        }

        public void RestoreReadyStates()
        {
            if (configurator.config.senders != null)
            {
                SyncableBase[] syncables = GameObject.FindObjectsOfType<SyncableBase>();
                foreach (string sender in configurator.config.senders)
                {
                    SyncableBase syncable = syncables.Where(s => s.identifier == sender).FirstOrDefault();
                    if (syncable != null)
                    {
                        syncable.isSender = true;
                    }

                }
            }
        }

        public void ApplyConfiguration()
        {
            configurator.config.hostAddress = hostAddressInputField.text;
            configurator.config.networkMode = networkModeDropdown.value;
            configurator.config.syncMode = (SynchronizerMode)synchronizerModeDropdown.value;
            configurator.config.channelId = networkChannelDropdown.value;
            configurator.config.compressData = compressDataToggle.isOn;
            configurator.config.autoStart = autoStartToggle.isOn;
            configurator.config.logAnalytics = logAnalyticsToggle.isOn;
            UpdateComponentsProperties();
        }

        public void UpdateComponentsProperties()
        {
            if (avatarSynchronizer != null)
            {
                avatarSynchronizer.syncMode = configurator.config.syncMode;
                avatarSynchronizer.channelId = configurator.config.channelId;
                avatarSynchronizer.compressData = configurator.config.compressData;
                avatarSynchronizer.logAnalytics = configurator.config.logAnalytics;
            }
            else
            {
                Debug.LogWarning("No avatar synchronizer found on the scene");
            }

            if (networkManager != null)
            {
                networkManager.networkAddress = configurator.config.hostAddress;
            }
            else
            {
                Debug.LogWarning("No network manager found on the scene");
            }
        }

        public void StartSync()
        {
            switch (configurator.config.networkMode)
            {
                case 0:
                    networkManager.StartClient();
                    PushStatus($"Starting client on address {configurator.config.hostAddress}...");
                    StartCoroutine(WatchConnection());
                    break;
                case 1:
                    networkManager.StartServer();
                    PushStatus("Server started");
                    break;
                case 2:
                    networkManager.StartHost();
                    PushStatus("Host started");
                    break;
            }

            startButton.gameObject.SetActive(false);
            stopButton.gameObject.SetActive(true);
        }

        public void OnStartButtonClick()
        {
            StartSync();
        }

        public IEnumerator WatchConnection()
        {
            while (!NetworkClient.isConnected && NetworkClient.active)
            {
                yield return new WaitForEndOfFrame();
            }
            if (!NetworkClient.active)
            {
                PushStatus("Network error");
            }
            else
            {
                PushStatus("Connection successful");
            }

        }

        public void LoadConfiguration()
        {
            bool configurationExists = configurator.LoadConfiguration();
            hostAddressInputField.text = configurator.config.hostAddress;
            networkModeDropdown.value = configurator.config.networkMode;
            synchronizerModeDropdown.value = (int)configurator.config.syncMode;
            networkChannelDropdown.value = configurator.config.channelId;
            compressDataToggle.isOn = configurator.config.compressData;
            logAnalyticsToggle.isOn = configurator.config.logAnalytics;
            autoStartToggle.isOn = configurator.config.autoStart;

            UpdateComponentsProperties();

            PushStatus($"Configuration loaded with host at {configurator.config.hostAddress} sending {configurator.config.syncMode.ToString()}. {(configurator.config.senders != null ? configurator.config.senders.Length : 0)} senders.");

            if (configurationExists)
            {
                setupPanel.SetActive(false);
            }


        }

        public void SaveConfiguration()
        {
            configurator.SaveConfiguration();
        }

        public void StopSync()
        {
            if (NetworkServer.active && NetworkClient.isConnected)
            {
                networkManager.StopHost();
            }
            else if (NetworkClient.isConnected)
            {
                networkManager.StopClient();
            }
            else if (NetworkServer.active)
            {
                networkManager.StopServer();
            }

            PushStatus("Network has been halted");

            stopButton.gameObject.SetActive(false);
            startButton.gameObject.SetActive(true);
        }

        public void OnStopButtonClick()
        {
            StopSync();
        }

        public void OnApplySetupButtonClick()
        {
            ApplyConfiguration();
            PushStatus("Configuration applied!");
            setupPanel.SetActive(false);
        }

        public void OnSaveSetupButtonClick()
        {
            ApplyConfiguration();
            FreezeReadyStates();
            SaveConfiguration();
            PushStatus("Configuration saved!");
            setupPanel.SetActive(false);
        }

        public void OnSetupButtonClick()
        {
            setupPanel.SetActive(!setupPanel.activeSelf);
        }

        public void OnSyncablesButtonClick()
        {
            syncablesPanel.SetActive(!syncablesPanel.activeSelf);
        }

        public void OnValueChangedSendersOnlyToggle(bool value)
        {
            syncablePanels.Where(p => !p.syncable.isSender).ToList().ForEach(p => p.gameObject.SetActive(!value));
        }

        public void OnFilterDropdownValueChanged(int value)
        {
            switch (value)
            {
                case 1:
                    syncablePanels.ForEach(p => p.gameObject.SetActive(p.syncable.isSender));
                    break;
                case 2:
                    syncablePanels.ForEach(p => p.gameObject.SetActive(!p.syncable.isSender));
                    break;
                default:
                    syncablePanels.ForEach(p => p.gameObject.SetActive(true));
                    break;
            }
        }

        public void OnClickToggleAllReadyButton()
        {
            globalAllReady = !globalAllReady;
            foreach (NetworkSyncablePanel syncablePanel in syncablePanels.Where(p => p.syncable.isSender))
            {
                syncablePanel.isReadyToggle.isOn = globalAllReady;
            }
        }

        public void PushStatus(string content)
        {
            statusText.text = content;
        }

    }
}