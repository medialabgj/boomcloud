﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System.Linq;

namespace AvatarSync
{
    public enum SynchronizerMode { BothWays, SenderOnly, ReceiverOnly };
    public class AvatarSynchronizer : MonoBehaviour
    {
        public SynchronizerMode syncMode;        
    
        bool canSend => (syncMode == SynchronizerMode.BothWays || syncMode == SynchronizerMode.SenderOnly);

        bool canReceive => (syncMode == SynchronizerMode.BothWays || syncMode == SynchronizerMode.ReceiverOnly);

        [System.Serializable]
        public class CompressableMessage<T> : MessageBase
        {
            public string poseData;
            public bool dataIsCompressed;
            public T GetData()
            {
                return JsonUtility.FromJson<T>(dataIsCompressed ? SyncableUtils.Decompress(poseData) : poseData);
            }

            public void SetData(T data)
            {
                poseData = dataIsCompressed ? SyncableUtils.Compress(JsonUtility.ToJson(data)) : JsonUtility.ToJson(data);
            }
        }

        [System.Serializable]
        public class AvatarSyncMessage : MessageBase
        {
            public string identifier;
            public bool dataIsCompressed;
            public string poseData;
        }

        [System.Serializable]
        public class HumanAvatarSyncMessage : MessageBase
        {
            public string identifier;
            public bool dataIsCompressed;
            public string poseData;

            public HumanAvatarPose GetData()
            {
                return JsonUtility.FromJson<HumanAvatarPose>(dataIsCompressed ? SyncableUtils.Decompress(poseData) : poseData);
            }

            public void SetData(HumanAvatarPose data)
            {
                poseData = dataIsCompressed ? SyncableUtils.Compress(JsonUtility.ToJson(data)) : JsonUtility.ToJson(data);
            }
        }

        [System.Serializable]
        public struct AvatarFramePose
        {
            public string identifier;
            public AvatarPose pose;
        }

        [System.Serializable]
        public struct AvatarFrameContainer
        {
            public AvatarFramePose[] avatars;
        }

        public class AvatarFrameMessage : MessageBase
        {
            public bool dataIsCompressed;
            public string frameData;
        }


        [System.Serializable]
        public class PropSyncMessage : MessageBase
        {
            public string identifier;
            public bool dataIsCompressed;
            public string poseData;
        }

        public int channelId;

        public bool isSender = false;

        bool isServer = false;

        public bool compressData = true;

        public bool serverFeedback = false;

        public bool sendPacked = false;

        public bool logAnalytics = false;

        public List<SyncableAvatar> avatars = new List<SyncableAvatar>();
        public List<SyncableHumanAvatar> humanAvatars = new List<SyncableHumanAvatar>();
        public List<SyncableProp> props = new List<SyncableProp>();

        public Vector2 buttonStartPosition = new Vector2(32f, 160f);
        public Vector2 buttonSize = new Vector2(80f, 24f);
        public Vector2 buttonOffsetPosition = new Vector2(0, 48f);

        public string availableAvatars;
        public string availableHumans;
        public string availableProps;

        float logTimer;

        float frameTimer;

        [Range(1f, 60f)]
        public float refreshRate = 1f;

        public static event System.Action<string[]> SyncableAvailabilityChange;
        public static event System.Action ServerStarted;
        public static event System.Action ClientStarted;
        public static event System.Action ListenerRegistered;

        private void OnEnable()
        {
            SyncableAvatar.AvatarReady += OnAvatarReady;
            SyncableHumanAvatar.HumanAvatarReady += OnHumanAvatarReady;
            SyncableProp.PropReady += OnPropReady;
        }

        private void OnDisable()
        {
            SyncableAvatar.AvatarReady -= OnAvatarReady;
            SyncableHumanAvatar.HumanAvatarReady -= OnHumanAvatarReady;
            SyncableProp.PropReady -= OnPropReady;
        }

        void OnAvatarReady(SyncableAvatar newAvatar)
        {
            if (!avatars.Contains(newAvatar))
            {
                avatars.Add(newAvatar);                
            }
        }

        void OnHumanAvatarReady(SyncableHumanAvatar newHumanAvatar)
        {
            if(!humanAvatars.Contains(newHumanAvatar))
            {
                humanAvatars.Add(newHumanAvatar);                
            }
        }

        void OnPropReady(SyncableProp newProp)
        {
            if (!props.Contains(newProp))
            {
                props.Add(newProp);                
            }
        }

        public void OnServerStart()
        {
            NetworkServer.RegisterHandler<AvatarSyncMessage>(OnServerAvatarSyncMessage, false);
            NetworkServer.RegisterHandler<HumanAvatarSyncMessage>(OnServerHumanAvatarSyncMessage, false);
            NetworkServer.RegisterHandler<AvatarFrameMessage>(OnServerAvatarFrameMessage, false);
            NetworkServer.RegisterHandler<PropSyncMessage>(OnServerPropSyncMessage, false);
            isServer = true;
            ToggleServerFeedback(serverFeedback);
            ServerStarted?.Invoke();
        }

        public void OnClientStart()
        {            
            RegisterNetworkListener();
            isServer = false;
            ClientStarted?.Invoke();
        }

        public void ToggleServerFeedback(bool value)
        {
            serverFeedback = value;

            foreach (SyncableAvatar avatar in avatars)
            {
                avatar.gameObject.SetActive(!isServer || value);
            }
        }

        public void RegisterNetworkListener()
        {            
            if (!canReceive)
            {                
                NetworkClient.UnregisterHandler<AvatarSyncMessage>();
                NetworkClient.UnregisterHandler<HumanAvatarSyncMessage>();
                NetworkClient.UnregisterHandler<PropSyncMessage>();
            } else if (canReceive)
            {                
                NetworkClient.RegisterHandler<AvatarSyncMessage>(OnClientAvatarSyncMessage, false);
                NetworkClient.RegisterHandler<HumanAvatarSyncMessage>(OnClientHumanAvatarSyncMessage, false);
                NetworkClient.RegisterHandler<PropSyncMessage>(OnClientPropSyncMessage, false);
                ListenerRegistered?.Invoke();
            }            
        }

        public void ToggleIsSender(bool value)
        {
            isSender = value;            
            RegisterNetworkListener();
        }

        public void ToggleIsReady(bool value)
        {
            foreach (SyncableAvatar avatar in avatars)
            {
                avatar.isReady = value;
            }
        }

        public void ToggleCompression(bool value)
        {
            compressData = value;
        }

        public string AvatarPoseToString(AvatarPose pose, bool dataIsCompressed = false)
        {
            return dataIsCompressed ? SyncableUtils.Compress(JsonUtility.ToJson(pose)) : JsonUtility.ToJson(pose);
        }

        public AvatarPose AvatarPoseFromString(string value, bool dataIsCompressed = false)
        {
            return JsonUtility.FromJson<AvatarPose>(dataIsCompressed ? SyncableUtils.Decompress(value) : value);
        }

        /*
        public string HumanAvatarPoseToString(HumanAvatarPose pose, bool dataIsCompressed = false)
        {
            return dataIsCompressed ? SyncableUtils.Compress(JsonUtility.ToJson(pose)) : JsonUtility.ToJson(pose);
        }

        public HumanAvatarPose HumanAvatarPoseFromString(string value, bool dataIsCompressed = false)
        {
            return JsonUtility.FromJson<HumanAvatarPose>(dataIsCompressed ? SyncableUtils.Decompress(value) : value);
        }
        */

        public string FrameContainerToString(AvatarFrameContainer frameContainer, bool dataIsCompressed = false)
        {
            return dataIsCompressed ? SyncableUtils.Compress(JsonUtility.ToJson(frameContainer)) : JsonUtility.ToJson(frameContainer);
        }

        public AvatarFrameContainer FrameContainerFromString(string value, bool dataIsCompressed = false)
        {
            return JsonUtility.FromJson<AvatarFrameContainer>(dataIsCompressed ? SyncableUtils.Decompress(value) : value);
        }

        public string PropPoseToString(PropPose pose, bool dataIsCompressed = false)
        {
            return dataIsCompressed ? SyncableUtils.Compress(JsonUtility.ToJson(pose)) : JsonUtility.ToJson(pose);
        }

        public PropPose PropPoseFromString(string value, bool dataIsCompressed = false)
        {
            return JsonUtility.FromJson<PropPose>(dataIsCompressed ? SyncableUtils.Decompress(value) : value);
        }

        public void OnServerAvatarSyncMessage(NetworkConnection connection, AvatarSyncMessage message)
        {
            if (serverFeedback)
            {
                AvatarPose newPose = AvatarPoseFromString(message.poseData, message.dataIsCompressed);
                foreach (SyncableAvatar avatar in avatars)
                {
                    if (avatar.identifier == message.identifier)
                    {
                        avatar.ApplyPose(newPose);
                    }
                }
            }

            NetworkServer.SendToAll(message, channelId, true);
        }

        public void OnServerHumanAvatarSyncMessage(NetworkConnection connection, HumanAvatarSyncMessage message)
        {
            if(serverFeedback)
            {
                HumanAvatarPose newPose = message.GetData();
                foreach (SyncableHumanAvatar humanAvatar in humanAvatars)
                {
                    if (humanAvatar.identifier == message.identifier)
                    {
                        humanAvatar.ApplyPose(newPose);
                    }
                }
            }
            
            NetworkServer.SendToAll(message, channelId, true);
        }

        public void OnServerAvatarFrameMessage(NetworkConnection connection, AvatarFrameMessage message)
        {
            NetworkServer.SendToAll(message, channelId, true);
        }

        public void OnClientAvatarSyncMessage(NetworkConnection connection, AvatarSyncMessage message)
        {
            if (!canReceive)
            {
                return;
            }

            if (avatars != null && avatars.Count > 0)
            {
                AvatarPose newPose = AvatarPoseFromString(message.poseData, message.dataIsCompressed);
                foreach (SyncableAvatar avatar in avatars)
                {
                    if (avatar.identifier == message.identifier)
                    {
                        avatar.ApplyPose(newPose);
                    }
                }
            }
        }

        public void OnClientHumanAvatarSyncMessage(NetworkConnection connection, HumanAvatarSyncMessage message)
        {
            if (!canReceive)
            {
                return;
            }

            if (humanAvatars != null && humanAvatars.Count > 0)
            {
                
                HumanAvatarPose newHumanPose = message.GetData();
                foreach (SyncableHumanAvatar humanAvatar in humanAvatars)
                {
                    if (humanAvatar.identifier == message.identifier)
                    {                        
                        humanAvatar.ApplyPose(newHumanPose);
                    }
                }
                
            }
        }

        public void OnClientAvatarFrameMessage(NetworkConnection connection, AvatarFrameMessage message)
        {
            if (!canReceive)
            {
                return;
            }

            if (avatars != null && avatars.Count > 0)
            {
                AvatarFrameContainer container = FrameContainerFromString(message.frameData, message.dataIsCompressed);
                foreach (SyncableAvatar avatar in avatars)
                {
                    foreach (AvatarFramePose framePose in container.avatars)
                    {
                        if (framePose.identifier == avatar.identifier)
                        {
                            avatar.ApplyPose(framePose.pose);
                            continue;
                        }
                    }
                }
            }

        }

        public void OnServerPropSyncMessage(NetworkConnection connection, PropSyncMessage message)
        {
            NetworkServer.SendToAll(message, channelId, true);
            /*
            foreach (KeyValuePair<int, NetworkConnectionToClient> keyValue in NetworkServer.connections)
            {
                if (keyValue.Key == connection.connectionId || !connection.isAuthenticated)
                {
                    continue;
                }

                keyValue.Value.Send(message, channelId);
            }
            */
        }

        public void OnClientPropSyncMessage(NetworkConnection connection, PropSyncMessage message)
        {
            if (!canReceive)
            {
                return;
            }

            if (props != null && props.Count > 0)
            {
                PropPose newPose = PropPoseFromString(message.poseData, message.dataIsCompressed);
                foreach (SyncableProp prop in props)
                {
                    if (prop.identifier == message.identifier)
                    {
                        prop.ApplyPose(newPose);
                    }
                }
            }
        }

        void LateUpdate()
        {
            frameTimer -= Time.deltaTime;

            if (frameTimer < 0)
            {
                frameTimer = 1f / refreshRate;

                if (canSend && NetworkClient.isConnected)
                {
                    if (sendPacked)
                    {
                        if (avatars != null)
                        {
                            SyncableAvatar[] readyAvatars = avatars.Where(a => a.isReady).ToArray();

                            if (readyAvatars.Length > 0)
                            {
                                AvatarFramePose[] poses = new AvatarFramePose[readyAvatars.Length];
                                for (int i = 0; i < poses.Length; i++)
                                {
                                    poses[i].identifier = readyAvatars[i].identifier;
                                    poses[i].pose = readyAvatars[i].pose;
                                }

                                AvatarFrameContainer frameContainer = new AvatarFrameContainer
                                {
                                    avatars = poses
                                };

                                AvatarFrameMessage frameMessage = new AvatarFrameMessage
                                {
                                    dataIsCompressed = compressData,
                                    frameData = FrameContainerToString(frameContainer, compressData)
                                };

                                if (logAnalytics)
                                {
                                    Debug.Log("Analytics: Packed avatars size: " + FrameContainerToString(frameContainer, compressData).Length);
                                }

                                NetworkClient.Send(frameMessage, channelId);
                            }
                        }
                    }
                    else
                    {

                        if (avatars != null)
                        {
                            foreach (SyncableAvatar avatar in avatars)
                            {
                                //Debug.Log(AvatarPoseToString(avatar.pose, compressData));
                                //Debug.Log(AvatarPoseToString(avatar.pose, compressData).Length * sizeof(System.Char));
                                if (avatar.isReady)
                                {
                                    NetworkClient.Send(new AvatarSyncMessage
                                    {
                                        identifier = avatar.identifier,
                                        dataIsCompressed = compressData,
                                        poseData = AvatarPoseToString(avatar.pose, compressData)
                                    }, channelId);

                                    if(logAnalytics)
                                    {
                                        Debug.Log(AvatarPoseToString(avatar.pose, false));
                                        Debug.Log("Analytics: Unpacked avatar size: " + AvatarPoseToString(avatar.pose, compressData).Length);
                                    }
                                }
                            }
                        }

                        if(humanAvatars != null)
                        {
                            foreach (SyncableHumanAvatar humanAvatar in humanAvatars)
                            {                                
                                if (humanAvatar.isReady)
                                {
                                    var message = new HumanAvatarSyncMessage
                                    {
                                        identifier = humanAvatar.identifier,
                                        dataIsCompressed = compressData                                        
                                    };

                                    message.SetData(humanAvatar.pose);

                                    NetworkClient.Send(message, channelId);

                                    /*
                                    if (logAnalytics)
                                    {
                                        Debug.Log(AvatarPoseToString(avatar.pose, false));
                                        Debug.Log("Analytics: Unpacked avatar size: " + AvatarPoseToString(avatar.pose, compressData).Length);
                                    }
                                    */
                                }
                            }
                        }

                        if (props != null)
                        {
                            foreach (SyncableProp prop in props)
                            {
                                NetworkClient.Send(new PropSyncMessage
                                {
                                    identifier = prop.identifier,
                                    dataIsCompressed = compressData,
                                    poseData = PropPoseToString(prop.pose, compressData)
                                }, channelId);
                            }
                        }
                    }
                }
            }


            logTimer -= Time.deltaTime;

            if(logTimer < 0)
            {
                logTimer = 1f;
                availableAvatars = string.Join(",", avatars.Where(a => Time.time - a.lastUpdateTime < 1f).OrderBy(a => a.identifier).Select(a => a.identifier).ToArray().Distinct());
                availableHumans = string.Join(",", humanAvatars.Where(a => Time.time - a.lastUpdateTime < 1f).OrderBy(a => a.identifier).Select(a => a.identifier).ToArray().Distinct());
                availableProps = string.Join(",", props.Where(a => Time.time - a.lastUpdateTime < 1f).OrderBy(a => a.identifier).Select(a => a.identifier).ToArray().Distinct());
                SyncableAvailabilityChange?.Invoke(new string[] { availableAvatars, availableHumans, availableProps});
            }
        }
     

      
    }
}