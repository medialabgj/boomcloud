﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NetworkSyncableUpdateMethod { LateUpdate, OnAnimatorIK, Custom }
public class SyncableBase : MonoBehaviour
{
    [Header("Network syncable")]
    public string identifier;

    [HideInInspector]
    public float lastUpdateTime;

    protected bool _isSender;

    public NetworkSyncableUpdateMethod updateMethod;

    public virtual bool isSender
    {
        get
        {
            return _isSender;
        }

        set
        {
            _isSender = value;
        }
    }

    public bool isReady;
    public bool useRootMotion;    

    public virtual void WritePose()
    {
        
    }

    public virtual void WritePoseIK()
    {

    }

    public virtual void ReadPose()
    {
        
    }

    protected virtual void ExecuteModules()
    {

    }
}
