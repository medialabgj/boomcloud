﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace AvatarSync
{
    public class AvatarSyncConfigurator : MonoBehaviour
    {
        public AvatarSyncableConfiguration config;
        const string fileName = "/Configurations/local.json";
        string path => Application.streamingAssetsPath + fileName;



        public void SaveConfiguration()
        {
            File.WriteAllText(path, JsonUtility.ToJson(config));
        }

        public bool LoadConfiguration()
        {
            if (File.Exists(path))
            {
                config = JsonUtility.FromJson<AvatarSyncableConfiguration>(File.ReadAllText(path));
                return true;
            }

            return false;
        }

    }
}