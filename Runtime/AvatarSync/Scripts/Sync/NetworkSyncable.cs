﻿using UnityEngine;

namespace AvatarSync
{
    public class NetworkSyncable<T> : SyncableBase where T : Pose
    {

        public float autoHideTime = 5f;



        protected bool isVisible = true;

        public static event System.Action<NetworkSyncable<T>> SyncableReady;

        public T pose;

        protected virtual void Start()
        {
            SyncableReady?.Invoke(this);
        }

        public virtual void ApplyPose(T newPose)
        {
            pose = newPose;
            lastUpdateTime = Time.time;

            if (!isVisible)
            {
                Show();
            }
        }


        public virtual void Show()
        {
            isVisible = true;
            foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
            {
                renderer.enabled = true;
            }
        }

        public virtual void Hide()
        {
            isVisible = false;
            foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
            {
                renderer.enabled = false;
            }
        }

        protected void Update()
        {
            if (!isSender && isVisible && autoHideTime != 0 && Time.time - lastUpdateTime > autoHideTime)
            {
                Hide();
            }
            else if (isSender && !isVisible)
            {
                Show();
            }
        }

        protected void LateUpdate()
        {
            if (isSender)
            {
                ExecuteModules();
                ReadPose();
            }
            else if (!isSender && updateMethod == NetworkSyncableUpdateMethod.LateUpdate)
            {
                WritePose();
            }
        }

        protected void OnAnimatorIK(int layerIndex)
        {
            if (!isSender && updateMethod == NetworkSyncableUpdateMethod.OnAnimatorIK)
            {
                WritePoseIK();
            }
        }

    }
}