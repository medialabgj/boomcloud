﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AvatarSync
{

    [System.Serializable]
    public struct PropPose : Pose
    {
        public Vector3 position;
        public Quaternion rotation;
    }


    public class SyncableProp : NetworkSyncable<PropPose>
    {
        public static event System.Action<SyncableProp> PropReady;

        protected override void Start()
        {
            base.Start();
            PropReady?.Invoke(this);
        }

        public override void WritePose()
        {
            base.WritePose();
            transform.position = pose.position;
            transform.rotation = pose.rotation;
        }

        public override void ReadPose()
        {
            base.ReadPose();
            pose.position = transform.position;
            pose.rotation = transform.rotation;
        }
    }
}