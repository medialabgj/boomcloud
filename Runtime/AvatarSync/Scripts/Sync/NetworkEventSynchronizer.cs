﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Mirror;

public class NetworkEventSynchronizer : MonoBehaviour
{

    public int channelId;
    public bool blockSameOrigin = true;
    public static string origin;


    public static System.Action<string, string> PushEventStringReceived;
    public static System.Action<string, int> PushEventIntReceived;
    public static System.Action<string, float> PushEventFloatReceived;
    public static System.Action<string, int[]> PushEventIntArrayReceived;
    public static System.Action<string, string[]> PushEventStringArrayReceived;

    [Serializable]
    public class PushEventStateMessage : MessageBase
    {
        public string origin;
        public string identifier;
        public string value;
    }


    [Serializable]
    public class PushFloatEventStateMessage : MessageBase
    {
        public string origin;
        public string identifier;
        public float value;
    }


    [Serializable]
    public class PushIntArrayEventStateMessage : MessageBase
    {
        public string origin;
        public string identifier;
        public int[] value;
    }

    [Serializable]
    public class PushStringArrayEventStateMessage : MessageBase
    {
        public string origin;
        public string identifier;
        public string[] value;
    }

    private void Awake()
    {
        origin = SystemInfo.deviceName;
    }

    public void OnServerStart()
    {
        NetworkServer.RegisterHandler<PushEventStateMessage>(OnServerPushEventStateMessage, false);
        NetworkServer.RegisterHandler<PushFloatEventStateMessage>(OnServerPushEventStateMessage, false);
        NetworkServer.RegisterHandler<PushIntArrayEventStateMessage>(OnServerPushEventStateMessage, false);
        NetworkServer.RegisterHandler<PushStringArrayEventStateMessage>(OnServerPushEventStateMessage, false);
    }

    void OnServerPushEventStateMessage(NetworkConnection conn, MessageBase message)
    {
        NetworkServer.SendToAll(message, channelId, true);
    }

    public void OnClientStart()
    {        
        NetworkClient.RegisterHandler<PushEventStateMessage>(OnClientPushEventStateMessage, false);
        NetworkClient.RegisterHandler<PushFloatEventStateMessage>(OnClientPushFloatEventStateMessage, false);
        NetworkClient.RegisterHandler<PushIntArrayEventStateMessage>(OnClientPushIntArrayEventStateMessage, false);
        NetworkClient.RegisterHandler<PushStringArrayEventStateMessage>(OnClientPushStringArrayEventStateMessage, false);
    }

    void OnClientPushEventStateMessage(NetworkConnection conn, PushEventStateMessage message)
    {
        if(blockSameOrigin && message.origin == origin)
        {
            Debug.LogWarning("Message ignore because of same origin");
            return;
        }

        if (int.TryParse(message.value.Trim(), out int intValue))
        {
            PushEventIntReceived?.Invoke(message.identifier, intValue);
        }
        else
        {
            PushEventStringReceived?.Invoke(message.identifier, message.value);
        }
    }

    void OnClientPushFloatEventStateMessage(NetworkConnection conn, PushFloatEventStateMessage message)
    {
        if (blockSameOrigin && message.origin == origin)
        {
            Debug.LogWarning("Message ignore because of same origin");
            return;
        }

        PushEventFloatReceived?.Invoke(message.identifier, message.value);
    }

    void OnClientPushIntArrayEventStateMessage(NetworkConnection conn, PushIntArrayEventStateMessage message)
    {
        if (blockSameOrigin && message.origin == origin)
        {
            Debug.LogWarning("Message ignore because of same origin");
            return;
        }

        PushEventIntArrayReceived?.Invoke(message.identifier, message.value);
    }

    void OnClientPushStringArrayEventStateMessage(NetworkConnection conn, PushStringArrayEventStateMessage message)
    {
        if (blockSameOrigin && message.origin == origin)
        {
            Debug.LogWarning("Message ignore because of same origin");
            return;
        }

        PushEventStringArrayReceived?.Invoke(message.identifier, message.value);
    }

    public void PushEvent(string identifier, string value)
    {
        var pushMessage = new PushEventStateMessage
        {
            origin = origin,
            identifier = identifier,
            value = value
        };

        SendPushMessage(pushMessage);
    }

    public void PushEvent(string identifier, int value)
    {
        PushEvent(identifier, value.ToString());
    }

    public void PushEvent(string identifier, float value)
    {
        var pushMessage = new PushFloatEventStateMessage
        {
            origin = origin,
            identifier = identifier,
            value = value
        };

        SendPushMessage(pushMessage);
    }

    public void PushEvent(string identifier, int[] values)
    {
        var pushMessage = new PushIntArrayEventStateMessage
        {
            origin = origin,
            identifier = identifier,
            value = values
        };

        SendPushMessage(pushMessage);
    }

    public void PushEvent(string identifier, string[] values)
    {
        var pushMessage = new PushStringArrayEventStateMessage
        {
            origin = origin,
            identifier = identifier,
            value = values
        };

        SendPushMessage(pushMessage);
    }

    void SendPushMessage(MessageBase pushMessage)
    {
        if (NetworkClient.isConnected)
        {
            NetworkClient.Send(pushMessage, channelId);
        }
        else if (NetworkServer.active)
        {
            NetworkServer.SendToAll(pushMessage, channelId, true);
        }
        else
        {
            Debug.LogWarning("Network is not available");
        }
    }
}
