﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AvatarSync
{
    [System.Serializable]
    public struct HumanAvatarPose : Pose
    {
        public IntVector3 pos;
        public IntVector3 rot;
        public byte p;
        public int[] m;
        //public float[] muscles;
    }

    [RequireComponent(typeof(Animator))]
    public class SyncableHumanAvatar : NetworkSyncable<HumanAvatarPose>
    {

        [Header("Modules")]
        public SyncableHumanModule[] modules;

        [Header("Syncable Human Avatar")]

        [Range(0, 6)]
        public byte positionFloatPrecision = 3;

        [Range(0, 6)]
        public byte rotationFloatPrecision = 3;


        [HideInInspector]
        public Animator animator;

        [HideInInspector]
        public Transform root;

        [HideInInspector]
        public HumanPoseHandler poseHandler;

        [HideInInspector]
        public HumanPose humanPose;



        public override bool isSender
        {
            get => base.isSender; set
            {
                _isSender = value;
                animator.enabled = isSender || updateMethod == NetworkSyncableUpdateMethod.OnAnimatorIK;
            }
        }

        public static event System.Action<SyncableHumanAvatar> HumanAvatarReady;

        void Awake()
        {

            animator = GetComponent<Animator>();

            root = this.transform;

            if (root != null)
            {
                poseHandler = new HumanPoseHandler(animator.avatar, root);
            }
            else
            {
                Debug.LogWarning($"No root assigned for {gameObject.name}");
            }

            ReadPose();

            //animator.enabled = isSender || updateMethod == NetworkSyncableUpdateMethod.OnAnimatorIK;
        }

        protected override void Start()
        {
            base.Start();
            HumanAvatarReady?.Invoke(this);
        }

        public override void ReadPose()
        {
            base.ReadPose();

            poseHandler.GetHumanPose(ref humanPose);

            pose.pos = IntVector3.FromVector3(humanPose.bodyPosition, positionFloatPrecision);
            pose.rot = IntVector3.FromVector3(humanPose.bodyRotation.eulerAngles, rotationFloatPrecision);

            pose.p = rotationFloatPrecision;

            if (pose.m == null || pose.m.Length != humanPose.muscles.Length)
            {
                pose.m = new int[humanPose.muscles.Length];
            }

            for (int i = 0; i < pose.m.Length; i++)
            {
                pose.m[i] = IntVector3.RoundWithPrecision(humanPose.muscles[i], pose.p);
            }

        }

        public override void WritePose()
        {
            humanPose.bodyPosition = pose.pos.ToVector3();
            humanPose.bodyRotation = Quaternion.Euler(pose.rot.ToVector3());

            if (humanPose.muscles == null || humanPose.muscles.Length != pose.m.Length)
            {
                humanPose.muscles = new float[pose.m.Length];
            }

            for (int i = 0; i < humanPose.muscles.Length; i++)
            {
                humanPose.muscles[i] = IntVector3.Restore(pose.m[i], pose.p);
            }

            //humanPose.muscles = (float[])pose.muscles.Clone();

            poseHandler.SetHumanPose(ref humanPose);
        }

        public override void ApplyPose(HumanAvatarPose newPose)
        {
            pose = newPose;
            lastUpdateTime = Time.time;

            if (!isVisible)
            {
                Show();
            }
        }

        protected override void ExecuteModules()
        {
            base.ExecuteModules();

            if (modules == null || modules.Length == 0)
            {
                return;
            }

            foreach (SyncableHumanModule module in modules)
            {
                module.Execute(this);
            }
        }
    }
}