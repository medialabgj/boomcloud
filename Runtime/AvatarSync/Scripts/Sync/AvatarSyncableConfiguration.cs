﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AvatarSync
{

    [System.Serializable]
    public struct AvatarSyncableConfiguration
    {
        public string hostAddress;
        public SynchronizerMode syncMode;
        public int networkMode;
        public int channelId;
        public bool compressData;
        public bool autoStart;
        public bool logAnalytics;
        public bool showFeedback;
        public string[] senders;
    }

}