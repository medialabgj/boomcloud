﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AvatarSync
{

    public interface Pose
    {

    }

    [System.Serializable]
    public struct AvatarPose : Pose
    {
        public IntVector3 position;
        public IntVector3 rotation;
        public byte pos_p;
        public byte rot_p;
        public bool useRootMotion;
        public BonePose[] bones;

    }

    [System.Serializable]
    public struct BonePose
    {
        public int id;
        public IntVector3 rotation;
        public IntVector3 position;

    }

    [System.Serializable]
    public struct IntVector3
    {
        public long x, y, z;
        public byte p;

        public IntVector3(int _x, int _y, int _z, byte _p)
        {
            x = _x;
            y = _y;
            z = _z;
            p = _p;
        }

        public static int RoundWithPrecision(float value, byte _p)
        {
            return Mathf.RoundToInt(value * Mathf.Pow(10, _p));
        }

        public static float Restore(long value, int _p)
        {
            return value / Mathf.Pow(10, _p);
        }

        public static IntVector3 FromVector3(Vector3 value, byte _p)
        {
            return new IntVector3(RoundWithPrecision(value.x, _p), RoundWithPrecision(value.y, _p), RoundWithPrecision(value.z, _p), _p);
        }

        public Vector3 ToVector3()
        {
            return new Vector3(Restore(x, p), Restore(y, p), Restore(z, p));
        }
    }

    [RequireComponent(typeof(Animator))]
    public class SyncableAvatar : NetworkSyncable<AvatarPose>
    {
        [Header("Syncable Avatar")]

        [Range(0, 6)]
        public byte positionFloatPrecision = 3;

        [Range(0, 6)]
        public byte rotationFloatPrecision = 3;

        [HideInInspector]
        public Animator animator;

        [Header("Debug info")]

        public Dictionary<HumanBodyBones, Transform> bones;

        public override bool isSender
        {
            get => base.isSender; set
            {
                _isSender = value;
                animator.enabled = isSender || updateMethod == NetworkSyncableUpdateMethod.OnAnimatorIK;
            }
        }


        HumanBodyBones[] bonesToSynchronize = new HumanBodyBones[] {
        HumanBodyBones.Neck,
        HumanBodyBones.Head,
        HumanBodyBones.Hips,
        HumanBodyBones.Spine,
        HumanBodyBones.Chest,
        HumanBodyBones.UpperChest,
        HumanBodyBones.LeftShoulder,
        HumanBodyBones.LeftUpperArm,
        HumanBodyBones.LeftLowerArm,
        HumanBodyBones.LeftHand,
        HumanBodyBones.RightShoulder,
        HumanBodyBones.RightUpperArm,
        HumanBodyBones.RightLowerArm,
        HumanBodyBones.RightHand,
        HumanBodyBones.LeftUpperLeg,
        HumanBodyBones.LeftLowerLeg,
        HumanBodyBones.LeftFoot,
        HumanBodyBones.LeftToes,
        HumanBodyBones.RightUpperLeg,
        HumanBodyBones.RightLowerLeg,
        HumanBodyBones.RightFoot,
        HumanBodyBones.RightToes,
    };

        public static event System.Action<SyncableAvatar> AvatarReady;

        void Awake()
        {
            bones = new Dictionary<HumanBodyBones, Transform>();
            animator = GetComponent<Animator>();
            pose.bones = new BonePose[bonesToSynchronize.Length];

            int i = 0;
            foreach (HumanBodyBones humanBodyBones in bonesToSynchronize)
            {
                pose.bones[i].id = (int)humanBodyBones;
                bones.Add(humanBodyBones, animator.GetBoneTransform(humanBodyBones));
                i++;
            }

            ReadPose();

            animator.enabled = isSender || updateMethod == NetworkSyncableUpdateMethod.OnAnimatorIK;
        }

        public override void ApplyPose(AvatarPose newPose)
        {
            pose = newPose;
            lastUpdateTime = Time.time;

            if (!isVisible)
            {
                Show();
            }
        }

        public override void ReadPose()
        {
            pose.pos_p = positionFloatPrecision;
            pose.rot_p = rotationFloatPrecision;
            pose.position = IntVector3.FromVector3(transform.localPosition, pose.pos_p);
            pose.rotation = IntVector3.FromVector3(transform.localRotation.eulerAngles, pose.rot_p);
            for (int i = 0; i < pose.bones.Length; i++)
            {
                pose.bones[i].rotation = IntVector3.FromVector3(bones[(HumanBodyBones)(pose.bones[i].id)].localRotation.eulerAngles, pose.rot_p);
                if (!pose.useRootMotion)
                    pose.bones[i].position = IntVector3.FromVector3(bones[(HumanBodyBones)(pose.bones[i].id)].localPosition, pose.pos_p);

            }
        }

        public override void WritePose()
        {
            transform.localPosition = pose.position.ToVector3();
            transform.localRotation = Quaternion.Euler(pose.rotation.ToVector3());
            pose.useRootMotion = useRootMotion;
            for (int i = 0; i < pose.bones.Length; i++)
            {
                bones[(HumanBodyBones)pose.bones[i].id].localRotation = Quaternion.Euler(pose.bones[i].rotation.ToVector3());
                if (!useRootMotion)
                    bones[(HumanBodyBones)pose.bones[i].id].localPosition = pose.bones[i].position.ToVector3();
            }
        }

        public override void WritePoseIK()
        {
            transform.localPosition = pose.position.ToVector3();
            transform.localRotation = Quaternion.Euler(pose.rotation.ToVector3());
            pose.useRootMotion = useRootMotion;
            for (int i = 0; i < pose.bones.Length; i++)
            {
                animator.SetBoneLocalRotation((HumanBodyBones)pose.bones[i].id, Quaternion.Euler(pose.bones[i].rotation.ToVector3()));

                if (!useRootMotion)
                    bones[(HumanBodyBones)pose.bones[i].id].localPosition = pose.bones[i].position.ToVector3();
            }
        }

        protected override void Start()
        {
            base.Start();
            AvatarReady?.Invoke(this);
        }

    }
}