﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SyncableStageValue : MonoBehaviour
{
    public string identifier;

    [Range(1f, 60f)]
    public float refreshRate = 30f;

    public bool isSender;

    float frameTimer;
    float previousValue;
    public float value;

    public UnityEvent<float> OnValueUpdate;

    public static System.Action<SyncableStageValue, float> OnValueChange;

    public static System.Action<SyncableStageValue> SyncableStageValueReady;

    private void Start()
    {
        SyncableStageValueReady?.Invoke(this);
    }

    private void LateUpdate()
    {
        frameTimer -= Time.unscaledDeltaTime;
        if (frameTimer < 0)
        {
            frameTimer = 1f / refreshRate;

            if (value != previousValue)
            {
                OnValueChange?.Invoke(this, value);
            }
            previousValue = value;
        }
    }

    public virtual void WriteValue(float newValue)
    {
        value = newValue;
        OnValueUpdate?.Invoke(value);
    }

    public void Reset()
    {
        value = 0;
        OnValueUpdate?.Invoke(value);
    }

}
