﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using Mirror;
using System.Linq;



    [Serializable]
    public struct SaveableStageState
    {
        public SyncableStageStatus status;
        public string identifier;
    }

    [Serializable]
    public struct SaveableAudioState
    {
        public float status;
        public string identifier;
    }

    [Serializable]
    public struct StageSynchronizerState
    {
        public SaveableStageState[] stages;
        public SaveableAudioState[] audios;
    }


    public class StageSynchronizer : MonoBehaviour
    {

        public bool isSender = false;
        public int channelId;

        List<SyncableStage> stages = new List<SyncableStage>();

        public Dictionary<string, SyncableStageStatus> stagesStatus = new Dictionary<string, SyncableStageStatus>();

        List<SyncableAudioSource> audios = new List<SyncableAudioSource>();

        List<SyncableStageValue> values = new List<SyncableStageValue>();

        public Dictionary<string, float> valuesStatus = new Dictionary<string, float>();

        public Dictionary<string, float> audiosStatus = new Dictionary<string, float>();

        public static System.Action<string, string> PushEventStringReceived;
        public static System.Action<string, int> PushEventIntReceived;
        public static System.Action<string, float> PushEventFloatReceived;
        public static System.Action<string, int[]> PushEventIntArrayReceived;
        public static System.Action<string, string[]> PushEventStringArrayReceived;

        [Serializable]
        public class PushEventStateMessage : MessageBase
        {
            public string identifier;
            public string value;
        }


        [Serializable]
        public class PushFloatEventStateMessage : MessageBase
        {
            public string identifier;
            public float value;
        }


        [Serializable]
        public class PushIntArrayEventStateMessage : MessageBase
        {
            public string identifier;
            public int[] value;
        }

        [Serializable]
        public class PushStringArrayEventStateMessage : MessageBase
        {
            public string identifier;
            public string[] value;
        }


        [Serializable]
        public class StageStateMessage : MessageBase
        {
            public string identifier;
            public SyncableStageStatus status;
        }

        [Serializable]
        public class AudioControlMessage : MessageBase
        {
            public string identifier;
            //public float duration;
            public bool status;
        }

        [Serializable]
        public class AudioSyncMessage : MessageBase
        {
            public string identifier;
            public float time;
        }

        [Serializable]
        public class ResetStageMessage : MessageBase
        {

        }

        [Serializable]
        public class ValueSyncMessage : MessageBase
        {
            public string identifier;
            public float value;
        }

        void OnEnable()
        {
            //ComedieNetworkManager.ServerConnected += OnServerConnect;
            SyncableStage.SyncableStageReady += OnSyncableStageReady;
            SyncableStage.SyncableStageUpdate += OnSyncableStageUpdate;
            SyncableAudioSource.SyncableAudioSourceReady += OnSyncableAudioSourceReady;
            SyncableAudioSource.SyncableAudioSourceUpdate += OnSyncableAudioSourceUpdate;
            SyncableStageValue.SyncableStageValueReady += OnSyncableStageValueReady;
            if (isSender)
            {
                SyncableStageValue.OnValueChange += OnSyncableValueChange;
            }
        }


        void OnDisable()
        {
            //ComedieNetworkManager.ServerConnected -= OnServerConnect;
            SyncableStage.SyncableStageReady -= OnSyncableStageReady;
            SyncableStage.SyncableStageUpdate -= OnSyncableStageUpdate;
            SyncableAudioSource.SyncableAudioSourceReady -= OnSyncableAudioSourceReady;
            SyncableAudioSource.SyncableAudioSourceUpdate -= OnSyncableAudioSourceUpdate;
            SyncableStageValue.SyncableStageValueReady -= OnSyncableStageValueReady;
            if (isSender)
            {
                SyncableStageValue.OnValueChange -= OnSyncableValueChange;
            }
        }

        void OnSyncableStageReady(SyncableStage stage)
        {
            stages.Add(stage);
            if (!isSender)
            {
                stage.ResetStage();
            }
        }

        void OnSyncableStageUpdate(string identifier, SyncableStageStatus stageStatus)
        {
            Debug.Log("Stage update triggered");
            if (isSender && NetworkClient.isConnected)
            {
                StageStateMessage stageStateMessage = new StageStateMessage
                {
                    identifier = identifier,
                    status = stageStatus
                };
                Debug.Log("Stage sync data sent");
                NetworkClient.Send(stageStateMessage, channelId);
            }
        }

        void OnSyncableAudioSourceReady(SyncableAudioSource audio)
        {
            audios.Add(audio);
        }

        void OnSyncableAudioSourceUpdate(string identifier, bool status)
        {
            Debug.Log("Audio update triggered");
            if (isSender && NetworkClient.isConnected)
            {
                NetworkClient.Send(new AudioControlMessage
                {
                    identifier = identifier,
                    status = status
                });
            }
        }

        void OnSyncableStageValueReady(SyncableStageValue syncableStageValue)
        {
            Debug.Log("Syncable value is ready and added");
            values.Add(syncableStageValue);
            syncableStageValue.isSender = isSender;
        }


        void OnSyncableValueChange(SyncableStageValue syncableStageValue, float value)
        {
            if (isSender && NetworkClient.isConnected)
            {
                ValueSyncMessage message = new ValueSyncMessage
                {
                    identifier = syncableStageValue.identifier,
                    value = value
                };
                NetworkClient.Send(message, channelId);
            }
        }

        void Start()
        {

        }

        public void OnServerStart()
        {
            NetworkServer.RegisterHandler<StageStateMessage>(OnServerStageStateMessage, false);
            NetworkServer.RegisterHandler<AudioControlMessage>(OnServerAudioControlMessage, false);
            NetworkServer.RegisterHandler<ValueSyncMessage>(OnServerValueSyncMessage, false);
            NetworkServer.RegisterHandler<PushEventStateMessage>(OnServerPushEventStateMessage, false);
            NetworkServer.RegisterHandler<PushFloatEventStateMessage>(OnServerPushEventStateMessage, false);
            NetworkServer.RegisterHandler<PushIntArrayEventStateMessage>(OnServerPushEventStateMessage, false);
            NetworkServer.RegisterHandler<PushStringArrayEventStateMessage>(OnServerPushEventStateMessage, false);
    }

        public void OnServerConnect(NetworkConnection conn)
        {
            foreach (KeyValuePair<string, SyncableStageStatus> keyPair in stagesStatus)
            {
                StageStateMessage stageStateMessage = new StageStateMessage
                {
                    identifier = keyPair.Key,
                    status = keyPair.Value
                };
                conn.Send(stageStateMessage, channelId);
            }

            foreach (KeyValuePair<string, float> keyPair in audiosStatus)
            {
                if (keyPair.Value >= 0)
                {
                    AudioSyncMessage audioSyncMessage = new AudioSyncMessage
                    {
                        identifier = keyPair.Key,
                        time = Time.realtimeSinceStartup - keyPair.Value
                    };
                    conn.Send(audioSyncMessage, channelId);
                }
                else
                {
                    AudioControlMessage audioControlMessage = new AudioControlMessage
                    {
                        identifier = keyPair.Key,
                        status = false
                    };
                    conn.Send(audioControlMessage, channelId);
                }
            }

            foreach (KeyValuePair<string, float> keyPair in valuesStatus)
            {
                ValueSyncMessage message = new ValueSyncMessage
                {
                    identifier = keyPair.Key,
                    value = keyPair.Value
                };
                conn.Send(message, channelId);
            }
        }

        void OnServerStageStateMessage(NetworkConnection conn, StageStateMessage stageStateMessage)
        {
            Debug.Log("Stage update received");
            //if (ComedieNetworkManager.IsConnectionAllowedToControlServer(conn))
            //{
                Debug.Log("Stage update accepted");
                stagesStatus[stageStateMessage.identifier] = stageStateMessage.status;
                NetworkServer.SendToAll(stageStateMessage, channelId, true);
            //}
            
        }

        void OnServerValueSyncMessage(NetworkConnection conn, ValueSyncMessage message)
        {
            //if (ComedieNetworkManager.IsConnectionAllowedToControlServer(conn))
            //{
                //Debug.Log($"Stage value change for {message.identifier} at {message.value}");
                valuesStatus[message.identifier] = message.value;
                NetworkServer.SendToAll(message, channelId, true);
            //}
        }

        void OnServerPushEventStateMessage(NetworkConnection conn, MessageBase message)
        {
            NetworkServer.SendToAll(message, channelId, true);
        }

        /*
        void OnServerPushIntArrayEventStateMessage(NetworkConnection conn, PushIntArrayEventStateMessage message)
        {
            NetworkServer.SendToAll(message, channelId, true);
        }

        void OnServerPushStringArrayEventStateMessage(NetworkConnection conn, PushStringArrayEventStateMessage message)
        {
            NetworkServer.SendToAll(message, channelId, true);
        }
        */

        public void ResetAllStages()
        {
            Debug.Log("Resetting all stages");
            foreach (string identifier in stagesStatus.Keys)
            {
                for (int i = 0; i < stagesStatus[identifier].objects.Length; i++)
                {
                    stagesStatus[identifier].objects[i].active = false;
                }

                StageStateMessage stageStateMessage = new StageStateMessage
                {
                    identifier = identifier,
                    status = stagesStatus[identifier]
                };

                //NetworkServer.SendToAll(stageStateMessage, ComedieChannels.Stage, true);
            }

            foreach (string identifier in audiosStatus.Keys)
            {

                audiosStatus[identifier] = -1f;

                AudioControlMessage audioControlMessage = new AudioControlMessage
                {
                    identifier = identifier,
                    status = false
                };

                //NetworkServer.SendToAll(audioControlMessage, ComedieChannels.Stage, true);
            }

            foreach (string identifier in valuesStatus.Keys)
            {
                valuesStatus[identifier] = 0;
            }

            ResetStageMessage resetStageMessage = new ResetStageMessage();
            NetworkServer.SendToAll(resetStageMessage, channelId, true);            
        }

        public void OnClientStart()
        {
            if (!isSender)
            {
                NetworkClient.RegisterHandler<StageStateMessage>(OnClientStageStateMessage, false);
                NetworkClient.RegisterHandler<AudioControlMessage>(OnClientAudioControlMessage, false);
                NetworkClient.RegisterHandler<AudioSyncMessage>(OnClientAudioSyncMessage, false);
                NetworkClient.RegisterHandler<ResetStageMessage>(OnClientResetStageMessage, false);
                NetworkClient.RegisterHandler<ValueSyncMessage>(OnClientValueSyncMessage, false);                
            }
            NetworkClient.RegisterHandler<PushEventStateMessage>(OnClientPushEventStateMessage, false);
            NetworkClient.RegisterHandler<PushFloatEventStateMessage>(OnClientPushFloatEventStateMessage, false);
            NetworkClient.RegisterHandler<PushIntArrayEventStateMessage>(OnClientPushIntArrayEventStateMessage, false);
            NetworkClient.RegisterHandler<PushStringArrayEventStateMessage>(OnClientPushStringArrayEventStateMessage, false);
    }


        void OnClientStageStateMessage(NetworkConnection conn, StageStateMessage stageStateMessage)
        {
            if (isSender)
            {
                return;
            }

            foreach (SyncableStage stage in stages)
            {
                if (stage.identifier == stageStateMessage.identifier)
                {
                    stage.WriteStage(stageStateMessage.status);
                    break;
                }
            }
        }

        void OnServerAudioControlMessage(NetworkConnection conn, AudioControlMessage audioControlMessage)
        {
            Debug.Log("Recieved audio control message");
            //if (ComedieNetworkManager.IsConnectionAllowedToControlServer(conn))
            //{
                if (audioControlMessage.status)
                {
                    audiosStatus[audioControlMessage.identifier] = Time.realtimeSinceStartup;
                }
                else
                {
                    audiosStatus[audioControlMessage.identifier] = -1f;
                }

                NetworkServer.SendToAll(audioControlMessage, channelId, true);
            //}
            
        }

        void OnClientAudioControlMessage(NetworkConnection conn, AudioControlMessage audioControlMessage)
        {
            if (isSender)
            {
                return;
            }

            foreach (SyncableAudioSource audio in audios)
            {
                if (audio.identifier == audioControlMessage.identifier)
                {
                    if (audioControlMessage.status)
                    {
                        audio.PlayAt(0);
                    }
                    else
                    {
                        audio.Stop();
                    }
                    break;
                }
            }
        }

        void OnClientAudioSyncMessage(NetworkConnection conn, AudioSyncMessage audioSyncMessage)
        {
            if (isSender)
            {
                return;
            }

            foreach (SyncableAudioSource audio in audios)
            {
                if (audio.identifier == audioSyncMessage.identifier)
                {
                    audio.PlayAt(audioSyncMessage.time);
                    break;
                }
            }
        }

        void OnClientResetStageMessage(NetworkConnection conn, ResetStageMessage resetStageMessage)
        {
            if (isSender)
            {
                return;
            }

            foreach (SyncableAudioSource audio in audios)
            {
                audio.Stop();
            }

            foreach (SyncableStage stage in stages)
            {
                stage.ResetStage();
            }

            foreach (SyncableStageValue value in values)
            {
                value.Reset();
            }

        }

        void OnClientValueSyncMessage(NetworkConnection conn, ValueSyncMessage message)
        {
            if (isSender)
            {
                return;
            }

            if (values != null && values.Count > 0)
            {
                SyncableStageValue syncableValue = values.FirstOrDefault(v => v.identifier == message.identifier);
                if (syncableValue != null)
                {
                    syncableValue.WriteValue(message.value);
                }
            }
        }

        void OnClientPushEventStateMessage(NetworkConnection conn, PushEventStateMessage message)
        {
        
            if (int.TryParse(message.value.Trim(), out int intValue))
            {
                PushEventIntReceived?.Invoke(message.identifier, intValue);
            }
            else
            {
                PushEventStringReceived?.Invoke(message.identifier, message.value);
            }            
        }

        void OnClientPushFloatEventStateMessage(NetworkConnection conn, PushFloatEventStateMessage message)
        {
            PushEventFloatReceived?.Invoke(message.identifier, message.value);
        }
    
        void OnClientPushIntArrayEventStateMessage(NetworkConnection conn, PushIntArrayEventStateMessage message)
        {            
            PushEventIntArrayReceived?.Invoke(message.identifier, message.value);
        }

        void OnClientPushStringArrayEventStateMessage(NetworkConnection conn, PushStringArrayEventStateMessage message)
        {
            PushEventStringArrayReceived?.Invoke(message.identifier, message.value);
        }

        public void PushEvent(string identifier, string value)
        {
            var pushMessage = new PushEventStateMessage
            {
                identifier = identifier,
                value = value
            };

            SendPushMessage(pushMessage);
        }

        public void PushEvent(string identifier, int value)
        {        
            PushEvent(identifier, value.ToString());
        }

        public void PushEvent(string identifier, float value)
        {
            var pushMessage = new PushFloatEventStateMessage
            {
                identifier = identifier,
                value = value
            };

            SendPushMessage(pushMessage);
        }

        public void PushEvent(string identifier, int[] values)
        {
            var pushMessage = new PushIntArrayEventStateMessage
            {
                identifier = identifier,
                value = values
            };

            SendPushMessage(pushMessage);
        }

        public void PushEvent(string identifier, string[] values)
        {
            var pushMessage = new PushStringArrayEventStateMessage
            {
                identifier = identifier,
                value = values
            };

            SendPushMessage(pushMessage);
        }

        void SendPushMessage(MessageBase pushMessage)
        {
            if (NetworkClient.isConnected)
            {
                NetworkClient.Send(pushMessage, channelId);
            }
            else if (NetworkServer.active)
            {
                NetworkServer.SendToAll(pushMessage, channelId, true);
            }
            else
            {
                Debug.LogWarning("Network is not available");
            }
        }
}

