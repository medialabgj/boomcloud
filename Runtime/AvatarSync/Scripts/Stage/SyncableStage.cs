﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct SyncableStageStatus
{
    public SyncableStageObjectStatus[] objects;
}

[Serializable]
public struct SyncableStageObjectStatus
{
    public string path;
    public bool active;
}

public class SyncableStage : MonoBehaviour
{
   // public SceneController sceneController;
    public Dictionary<string, GameObject> objectReferences = new Dictionary<string, GameObject>();

    public static System.Action<SyncableStage> SyncableStageReady;

    public static System.Action<string, SyncableStageStatus> SyncableStageUpdate;

    public string identifier;

    public SyncableStageStatus stageStatus;


    void Awake()
    {
        /*
        if (sceneController == null)
        {
            Debug.Log("Scene controller hasn't been assigned to syncable stage");
            return;
        }
        foreach (ActionCuesList actionCue in sceneController.actionCuesList)
        {

            foreach (GameObject objectToHide in actionCue.objectToHideOnAllScreen)
            {

                if (!objectReferences.ContainsValue(objectToHide))
                {
                    objectReferences.Add(GetGameObjectPath(objectToHide.transform), objectToHide);
                }
            }

            foreach (GameObject objectToDisplay in actionCue.objectToDisplpayOnAllScreen)
            {
                if (!objectReferences.ContainsValue(objectToDisplay))
                {
                    objectReferences.Add(GetGameObjectPath(objectToDisplay.transform), objectToDisplay);
                }
            }

        }
        */

        ReadStage();

        SyncableStageReady?.Invoke(this);
    }

    void LateUpdate()
    {
        foreach (SyncableStageObjectStatus objectStatus in stageStatus.objects)
        {
            if (objectReferences.ContainsKey(objectStatus.path))
            {
                if (objectReferences[objectStatus.path].activeSelf != objectStatus.active)
                {
                    ReadStage();
                    SyncableStageUpdate?.Invoke(identifier, stageStatus);
                    break;
                }
            }
        }
    }

    public void ReadStage()
    {
        if (stageStatus.objects == null || stageStatus.objects.Length < objectReferences.Count)
        {
            stageStatus.objects = new SyncableStageObjectStatus[objectReferences.Count];
        }

        int i = 0;

        foreach (KeyValuePair<string, GameObject> objectState in objectReferences)
        {
            stageStatus.objects[i].path = objectState.Key;
            stageStatus.objects[i].active = objectState.Value.activeSelf;
            i++;
        }

    }

    public void WriteStage(SyncableStageStatus stageStatus)
    {
        foreach (SyncableStageObjectStatus objectStatus in stageStatus.objects)
        {
            if (objectReferences.ContainsKey(objectStatus.path))
            {
                objectReferences[objectStatus.path].SetActive(objectStatus.active);
            }
        }
    }

    public void ResetStage()
    {
        foreach (GameObject stageObject in objectReferences.Values)
        {
            stageObject.gameObject.SetActive(false);
        }
    }

    private static string GetGameObjectPath(Transform transform)
    {
        string path = transform.name;
        while (transform.parent != null)
        {
            transform = transform.parent;
            path = transform.name + "/" + path;
        }
        return path;
    }
}
