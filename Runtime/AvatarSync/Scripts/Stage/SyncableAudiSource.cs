﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SyncableAudioSource : MonoBehaviour
{
    AudioSource source;

    bool wasPlaying = false;

    public string identifier;

    public bool isPlaying => source != null ? source.isPlaying : false;

    public static System.Action<SyncableAudioSource> SyncableAudioSourceReady;
    public static System.Action<string, bool> SyncableAudioSourceUpdate;

    IEnumerator fadeCoroutine;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
        if (string.IsNullOrEmpty(identifier))
        {
            identifier = gameObject.name;
        }
    }

    public void PlayAt(float time)
    {
        Debug.Log("Play at time");
        if (time < source.clip.length)
        {
            source.time = time;
            source.Play();
            Fade(true);
        }
        else
        {
            Stop();
        }

    }

    public void Stop()
    {
        Debug.Log("Stop order");
        Fade(false);
    }

    void Fade(bool fadeIn)
    {
        if (fadeCoroutine != null)
        {
            StopCoroutine(fadeCoroutine);
        }
        fadeCoroutine = FadeSequence(fadeIn);
        StartCoroutine(fadeCoroutine);
    }


    IEnumerator FadeSequence(bool fadeIn, float animationTime = 1f)
    {
        float elapsedTime = 0;

        float start, target;

        if (fadeIn)
        {
            start = 0;
            target = 1f;
        }
        else
        {
            start = 1f;
            target = 0;
        }

        while (elapsedTime < animationTime)
        {
            elapsedTime += Time.unscaledDeltaTime;
            source.volume = Mathf.Lerp(start, target, elapsedTime / animationTime);
            yield return new WaitForEndOfFrame();
        }

        source.volume = target;
        if (target == 0)
        {
            source.Stop();
        }
        yield return null;
    }

    void Start()
    {
        SyncableAudioSourceReady?.Invoke(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (!wasPlaying && source.isPlaying)
        {
            SyncableAudioSourceUpdate?.Invoke(identifier, source.isPlaying);
        }
        else if (wasPlaying && !source.isPlaying)
        {
            SyncableAudioSourceUpdate?.Invoke(identifier, source.isPlaying);
        }
        wasPlaying = source.isPlaying;
    }
}
