﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorAvatar : MonoBehaviour
{
    public SyncableBase syncable;
    Vector3 mirrorScale;

    private void Awake()
    {
        mirrorScale = transform.localScale;
    }

    private void OnValidate()
    {       
        syncable = GetComponentInChildren<SyncableBase>();
        if(syncable != null)
        {
            syncable.updateMethod = NetworkSyncableUpdateMethod.Custom;
        }
    }

    void LateUpdate()
    {
        transform.localScale = Vector3.one;
        syncable.WritePose();
        transform.localScale = mirrorScale;
    }
}
