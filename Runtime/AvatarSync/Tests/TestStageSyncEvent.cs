﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStageSyncEvent : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        NetworkEventSynchronizer.PushEventStringReceived += OnPushStringEventReceived;
        NetworkEventSynchronizer.PushEventIntReceived += OnPushIntEventReceived;
        NetworkEventSynchronizer.PushEventFloatReceived += OnPushEventFloatReceived;
        NetworkEventSynchronizer.PushEventStringArrayReceived += OnPushEventStringArrayReceived;
        NetworkEventSynchronizer.PushEventIntArrayReceived += OnPushEventIntArrayReceived;
    }

    private void OnDisable()
    {
        NetworkEventSynchronizer.PushEventStringReceived -= OnPushStringEventReceived;
        NetworkEventSynchronizer.PushEventIntReceived -= OnPushIntEventReceived;
        NetworkEventSynchronizer.PushEventFloatReceived -= OnPushEventFloatReceived;
        NetworkEventSynchronizer.PushEventStringArrayReceived -= OnPushEventStringArrayReceived;
        NetworkEventSynchronizer.PushEventIntArrayReceived -= OnPushEventIntArrayReceived;
    }

    void OnPushStringEventReceived(string identifier, string value)
    {
        Debug.Log($"String event {identifier} received with value: {value}");
    }

    void OnPushIntEventReceived(string identifier, int value)
    {
        Debug.Log($"Int event {identifier} received with value: {value}");
    }

    void OnPushEventFloatReceived(string identifier, float value)
    {
        Debug.Log($"Float event {identifier} received with value: {value}");
    }

    void OnPushEventIntArrayReceived(string identifier, int[] value)
    {
        string serialized = string.Join(",", value);
        Debug.Log($"Array event {identifier} received with value: {serialized}");
    }

    void OnPushEventStringArrayReceived(string identifier, string[] value)
    {
        string serialized = string.Join(",", value);
        Debug.Log($"Array event {identifier} received with value: {serialized}");
    }

    private void OnGUI()
    {
        if(GUI.Button(new Rect(0, 0, 120, 30), "Push string event"))
        {
            GameObject.FindObjectOfType<NetworkEventSynchronizer>().PushEvent("MyEvent", "Blue");
        }


        if (GUI.Button(new Rect(0, 30, 120, 30), "Push int event"))
        {
            GameObject.FindObjectOfType<NetworkEventSynchronizer>().PushEvent("MyEvent", 30);
        }

        if (GUI.Button(new Rect(0, 60, 120, 30), "Push float event"))
        {
            GameObject.FindObjectOfType<NetworkEventSynchronizer>().PushEvent("MyEvent", 30.5f);
        }

        if (GUI.Button(new Rect(0, 90, 120, 30), "Push string array event"))
        {
            GameObject.FindObjectOfType<NetworkEventSynchronizer>().PushEvent("MyEvent", new string[] { "bob", "cat" });
        }

        if (GUI.Button(new Rect(0, 120, 120, 30), "Push int array event"))
        {
            GameObject.FindObjectOfType<NetworkEventSynchronizer>().PushEvent("MyEvent", new int[] { 0, 2, 3, 4, });
        }
    }
}
