//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.3.0
//     from Packages/com.ciegillesjobin.boomcloud/Runtime/BoomCloud/Utility/Input/KeyboardInput.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @KeyboardInput : IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @KeyboardInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""KeyboardInput"",
    ""maps"": [
        {
            ""name"": ""KeyboardController"",
            ""id"": ""dd127bbc-6cce-4c29-86fc-04d548020d7a"",
            ""actions"": [
                {
                    ""name"": ""keypad1"",
                    ""type"": ""Button"",
                    ""id"": ""01db8d14-d797-42fe-8a3c-f520b0624dea"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""keypad2"",
                    ""type"": ""Button"",
                    ""id"": ""3f939693-f116-4425-8bbc-c0b77f7d95b9"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""keypad0"",
                    ""type"": ""Button"",
                    ""id"": ""f8e56298-63bb-4e7a-9c87-e6180785d53e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""NextArrow"",
                    ""type"": ""Button"",
                    ""id"": ""44f7a0b8-b250-4106-ab6a-e3142b8bbf98"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""PreviousArrow"",
                    ""type"": ""Button"",
                    ""id"": ""72dd5e4d-d8c1-433a-aa41-36246a8df555"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""J"",
                    ""type"": ""Button"",
                    ""id"": ""ba354f11-27b8-47e7-87b4-c57361bb326e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""K"",
                    ""type"": ""Button"",
                    ""id"": ""4399a438-d0f9-415d-b51d-fb3b66ef765e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""L"",
                    ""type"": ""Button"",
                    ""id"": ""504a7408-d09c-474e-9a91-342a8414f472"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""6b123a1e-0cc9-4886-977e-df69430c2c4f"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""keypad1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5f3089f9-38bd-435b-a702-f2c7472bdb64"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""keypad2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""97083106-c4a9-4cee-830d-67b15b8e4fb9"",
                    ""path"": ""<Keyboard>/0"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""keypad0"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a5082451-bf05-42fe-9697-d06df3d044f1"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""NextArrow"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1b74649a-3f16-4551-af30-5e3be840f0b8"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PreviousArrow"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2f7f4fbb-2ff0-4827-9df7-6835ddd7888f"",
                    ""path"": ""<Keyboard>/j"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""J"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c042c0e5-e410-4010-bbf6-d72add43fe50"",
                    ""path"": ""<Keyboard>/k"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""K"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f7edca28-91b3-44f1-b22c-2635edba4c88"",
                    ""path"": ""<Keyboard>/l"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""L"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // KeyboardController
        m_KeyboardController = asset.FindActionMap("KeyboardController", throwIfNotFound: true);
        m_KeyboardController_keypad1 = m_KeyboardController.FindAction("keypad1", throwIfNotFound: true);
        m_KeyboardController_keypad2 = m_KeyboardController.FindAction("keypad2", throwIfNotFound: true);
        m_KeyboardController_keypad0 = m_KeyboardController.FindAction("keypad0", throwIfNotFound: true);
        m_KeyboardController_NextArrow = m_KeyboardController.FindAction("NextArrow", throwIfNotFound: true);
        m_KeyboardController_PreviousArrow = m_KeyboardController.FindAction("PreviousArrow", throwIfNotFound: true);
        m_KeyboardController_J = m_KeyboardController.FindAction("J", throwIfNotFound: true);
        m_KeyboardController_K = m_KeyboardController.FindAction("K", throwIfNotFound: true);
        m_KeyboardController_L = m_KeyboardController.FindAction("L", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }
    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }
    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // KeyboardController
    private readonly InputActionMap m_KeyboardController;
    private IKeyboardControllerActions m_KeyboardControllerActionsCallbackInterface;
    private readonly InputAction m_KeyboardController_keypad1;
    private readonly InputAction m_KeyboardController_keypad2;
    private readonly InputAction m_KeyboardController_keypad0;
    private readonly InputAction m_KeyboardController_NextArrow;
    private readonly InputAction m_KeyboardController_PreviousArrow;
    private readonly InputAction m_KeyboardController_J;
    private readonly InputAction m_KeyboardController_K;
    private readonly InputAction m_KeyboardController_L;
    public struct KeyboardControllerActions
    {
        private @KeyboardInput m_Wrapper;
        public KeyboardControllerActions(@KeyboardInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @keypad1 => m_Wrapper.m_KeyboardController_keypad1;
        public InputAction @keypad2 => m_Wrapper.m_KeyboardController_keypad2;
        public InputAction @keypad0 => m_Wrapper.m_KeyboardController_keypad0;
        public InputAction @NextArrow => m_Wrapper.m_KeyboardController_NextArrow;
        public InputAction @PreviousArrow => m_Wrapper.m_KeyboardController_PreviousArrow;
        public InputAction @J => m_Wrapper.m_KeyboardController_J;
        public InputAction @K => m_Wrapper.m_KeyboardController_K;
        public InputAction @L => m_Wrapper.m_KeyboardController_L;
        public InputActionMap Get() { return m_Wrapper.m_KeyboardController; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(KeyboardControllerActions set) { return set.Get(); }
        public void SetCallbacks(IKeyboardControllerActions instance)
        {
            if (m_Wrapper.m_KeyboardControllerActionsCallbackInterface != null)
            {
                @keypad1.started -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnKeypad1;
                @keypad1.performed -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnKeypad1;
                @keypad1.canceled -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnKeypad1;
                @keypad2.started -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnKeypad2;
                @keypad2.performed -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnKeypad2;
                @keypad2.canceled -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnKeypad2;
                @keypad0.started -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnKeypad0;
                @keypad0.performed -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnKeypad0;
                @keypad0.canceled -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnKeypad0;
                @NextArrow.started -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnNextArrow;
                @NextArrow.performed -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnNextArrow;
                @NextArrow.canceled -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnNextArrow;
                @PreviousArrow.started -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnPreviousArrow;
                @PreviousArrow.performed -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnPreviousArrow;
                @PreviousArrow.canceled -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnPreviousArrow;
                @J.started -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnJ;
                @J.performed -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnJ;
                @J.canceled -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnJ;
                @K.started -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnK;
                @K.performed -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnK;
                @K.canceled -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnK;
                @L.started -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnL;
                @L.performed -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnL;
                @L.canceled -= m_Wrapper.m_KeyboardControllerActionsCallbackInterface.OnL;
            }
            m_Wrapper.m_KeyboardControllerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @keypad1.started += instance.OnKeypad1;
                @keypad1.performed += instance.OnKeypad1;
                @keypad1.canceled += instance.OnKeypad1;
                @keypad2.started += instance.OnKeypad2;
                @keypad2.performed += instance.OnKeypad2;
                @keypad2.canceled += instance.OnKeypad2;
                @keypad0.started += instance.OnKeypad0;
                @keypad0.performed += instance.OnKeypad0;
                @keypad0.canceled += instance.OnKeypad0;
                @NextArrow.started += instance.OnNextArrow;
                @NextArrow.performed += instance.OnNextArrow;
                @NextArrow.canceled += instance.OnNextArrow;
                @PreviousArrow.started += instance.OnPreviousArrow;
                @PreviousArrow.performed += instance.OnPreviousArrow;
                @PreviousArrow.canceled += instance.OnPreviousArrow;
                @J.started += instance.OnJ;
                @J.performed += instance.OnJ;
                @J.canceled += instance.OnJ;
                @K.started += instance.OnK;
                @K.performed += instance.OnK;
                @K.canceled += instance.OnK;
                @L.started += instance.OnL;
                @L.performed += instance.OnL;
                @L.canceled += instance.OnL;
            }
        }
    }
    public KeyboardControllerActions @KeyboardController => new KeyboardControllerActions(this);
    public interface IKeyboardControllerActions
    {
        void OnKeypad1(InputAction.CallbackContext context);
        void OnKeypad2(InputAction.CallbackContext context);
        void OnKeypad0(InputAction.CallbackContext context);
        void OnNextArrow(InputAction.CallbackContext context);
        void OnPreviousArrow(InputAction.CallbackContext context);
        void OnJ(InputAction.CallbackContext context);
        void OnK(InputAction.CallbackContext context);
        void OnL(InputAction.CallbackContext context);
    }
}
