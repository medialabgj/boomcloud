﻿using System.Collections.Generic;
using UnityEngine;
using QTMRealTimeSDK;
using QualisysRealTime.Unity;
using UnityEngine.UI;
using QTMRealTimeSDK.Data;
using QTMRealTimeSDK.Settings;

public class QualisysAutoConnect : MonoBehaviour
{
    private static QualisysAutoConnect instance;
    public Text QualisysStatusColorText;
    private bool QTMshutDown = false;
    public static QualisysAutoConnect GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;
    }


    private string connectionStatus = "Not Connected";

    //private short portUDP = -1;

    private bool connected = false;
   // private bool streaming = false;
    public bool stream6d = false;
    public bool stream3d = false;
    public bool stream3dNoLabels = false;
    public bool streamGaze = false;
    public bool streamAnalog = false;
    public bool streamSkeleton = true;

    protected RTClient rtClient;

    [Header("Qualisys-PC: 10.0.0.2 / local 127.0.0.1")]
    [SerializeField]
    private string qualisysServerIP = null;

    private List<DiscoveryResponse> discoveryResponses;

    private int attemptReconnection;
    DiscoveryResponse? selectedDiscoveryResponse = null;


    private void Start()
    {
        discoveryResponses = RTClient.GetInstance().GetServers();
        rtClient = RTClient.GetInstance();

        if (Application.isPlaying)
        {
            selectedDiscoveryResponse = null;
            if (discoveryResponses != null)
            {
                foreach (var discoveryResponse in discoveryResponses)
                {
                    Debug.Log("QTM Server found: " + discoveryResponse.HostName + " Server IP: " + discoveryResponse.IpAddress + " port: " + discoveryResponse.Port + " QTM Version: " + discoveryResponse.InfoText);

                    if (string.Equals(discoveryResponse.IpAddress, qualisysServerIP))
                    {
                        Debug.Log("QTM Matching Server found: " + discoveryResponse.HostName + " Server IP: " + discoveryResponse.IpAddress + " port: " + discoveryResponse.Port  + " QTM Version: " + discoveryResponse.InfoText);
                        selectedDiscoveryResponse = discoveryResponse;
                        QualisysStatusColorText.color = Color.yellow;

                        Connect();
                    }
                }
            }
            else
            {
                QualisysStatusColorText.color = Color.red;
                Debug.Log("QTM no Servers where discovered on the network");
                return;
            }

            if (selectedDiscoveryResponse == null)
            {
                QualisysStatusColorText.color = Color.red;
                Debug.Log("QTM no Servers with ip " + qualisysServerIP + " where found");
            }
        }
    }

    public void Events(RTPacket packet)
    {
        QTMEvent currentEvent = packet.GetEvent();
        Debug.Log("AAAAAAAAAAAAAAAAAA Event occurred! : " + currentEvent);

        if (currentEvent == QTMEvent.QTMShuttingDown) {
            QTMshutDown = true;
        }
    }

    public void reco()
    {
        //Start();
        if (!QTMshutDown)
        {
            RTClient.GetInstance().Disconnect();
            Connect();
        }
        else
        {
            restart();
        }
    }

    public void restart()
    {
        RTClient.GetInstance().Disconnect();
        Start();
        //Connect();
    }

    public void Connect()
    {

        if (selectedDiscoveryResponse.HasValue)
        {
            connected = RTClient.GetInstance().Connect(selectedDiscoveryResponse.Value, -1,false,false,false,false,false,true);
            if (connected)
            {
                connectionStatus = "Connected";
                QualisysStatusColorText.color = Color.green;
                Debug.Log("QTM Connection success");
            }
        }
        /*
        if (connected)
        {
            RTClient.GetInstance().TakeControl();

            streaming = RTClient.GetInstance().ConnectStream(portUDP, StreamRate.RateAllFrames, stream6d, stream3d, stream3dNoLabels, streamGaze, streamAnalog, streamSkeleton);
            if (streaming)
            {
                connectionStatus = "Streaming";
                Debug.Log("QTM Streaming success");

                SkeletonStatus();
                ObjectStatus();
            }
        }
        */
        else
        {
            connectionStatus = "Connection error - " + RTClient.GetInstance().GetErrorString();
            QualisysStatusColorText.color = Color.red;

            Debug.Log("QTM Error: " + connectionStatus + " Auto Reconnecting attempt: " + attemptReconnection);
            if(attemptReconnection < 10)
            {
               // Invoke("Connect", 1f);
            }
            attemptReconnection++;
        }


        /*
        if (connected)
        {
           // RTClient.GetInstance().TakeControl();

            streaming = RTClient.GetInstance().ConnectStream(portUDP, StreamRate.RateAllFrames, stream6d, stream3d, stream3dNoLabels, streamGaze, streamAnalog, streamSkeleton);
            if (streaming)
            {
                connectionStatus = "Streaming";
                QualisysStatusColorText.color = Color.green;
                //UIController.GetInstance().qualisysStatus.text = "Streaming";
                //UIController.GetInstance().qualisysStatus.color = Color.green;
                //Log.LogMe("QTM Streaming success");

                SkeletonStatus();
                ObjectStatus();
            }
        }
        */
    }

    private void SkeletonStatus()
    {
        var skeletons = RTClient.GetInstance().Skeletons;
        if (skeletons != null)
        {
            foreach (var skeleton in skeletons)
            {
                Debug.Log("QTM Found skeleton: " + skeleton.Name);
            }
        }
        else
        {
            Debug.Log("QTM no skeleton found");
        }
    }

    private void ObjectStatus()
    {
        var bodies = RTClient.GetInstance().Bodies;

        if(bodies != null)
        foreach(var bodie in bodies)
        {
                Debug.Log("QTM Found object: " + bodie.Name);
            }
        else
        {
            Debug.Log("QTM no object found");
        }
        //  var body = rtClient.Bodies[].Name;$
    }

    private void OnDestroy()
    {
        var instance = RTClient.GetInstance();
        if (instance.ConnectionState == RTConnectionState.Connected)
        {
            instance.Disconnect();
        }
        connected = false;
    }
}
