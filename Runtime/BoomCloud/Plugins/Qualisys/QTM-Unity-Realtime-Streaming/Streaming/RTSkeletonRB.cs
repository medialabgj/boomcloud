﻿// Unity SDK for Qualisys Track Manager. Copyright 2015-2018 Qualisys AB
//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using QTMRealTimeSDK;
using System;

namespace QualisysRealTime.Unity
{
    public class RTSkeletonRB : MonoBehaviour
    {
        public string SkeletonName = "Put QTM skeleton name here";

        private Avatar mSourceAvatar;
        public Avatar DestinationAvatar;

        private HumanPose mHumanPose = new HumanPose();
        private GameObject mStreamedRootObject;
        private Dictionary<uint, GameObject> mQtmSegmentIdToGameObject;
        private Dictionary<string, string> mMecanimToQtmSegmentNames = new Dictionary<string, string>();
        private Dictionary<string, string> mMecanimFingers = new Dictionary<string, string>();

        private HumanPoseHandler mSourcePoseHandler;
        private HumanPoseHandler mDestiationPoseHandler;

        protected RTClient rtClient;
        private Skeleton mQtmSkeletonCache;

        public bool PoseFingers = true;
        public Vector3 mFingerBoneTranslation = new Vector3(0.0f, 0.04f, 0.0f);
        public Vector3 mLeftThumbBoneTranslation = new Vector3(-0.04f, 0.0f, 0.04f);
        public Vector3 mRightThumbBoneTranslation = new Vector3(0.04f, 0.0f, -0.04f);

        private class GameObjectPosition
        {
            public uint SegmentId;
            public Vector3 Position = Vector3.zero;
            public Quaternion Rotation = Quaternion.identity;
        }

        public int FrameDelay = 0;
        private Queue<List<GameObjectPosition>> StoredPositions = new Queue<List<GameObjectPosition>>();

        private void OnValidate()
        {
            FrameDelay = Math.Max(FrameDelay, 0);
        }

        void Update()
        {
            if (rtClient == null) rtClient = RTClient.GetInstance();

            var skeleton = rtClient.GetSkeleton(SkeletonName);
            if (skeleton == null)
                return;

            if (mQtmSkeletonCache == null)
            {
               // mQtmSkeletonCache = skeleton.Name();
                if (mQtmSkeletonCache == null)
                    return;

                if (PoseFingers)
                {
                    CreateFingerMappings(SkeletonName);
                    CreateDefaultFingerSegments("Left", 30000, mLeftThumbBoneTranslation, mFingerBoneTranslation);
                    CreateDefaultFingerSegments("Right", 31000, mRightThumbBoneTranslation, mFingerBoneTranslation);
                }

                CreateMecanimToQtmSegmentNames(SkeletonName);

                if (mStreamedRootObject != null)
                    GameObject.Destroy(mStreamedRootObject);

                mStreamedRootObject = new GameObject(this.SkeletonName);
                mStreamedRootObject.SetActive(true);

                mQtmSegmentIdToGameObject = new Dictionary<uint, GameObject>(mQtmSkeletonCache.Segments.Count);

                
                foreach (var segment in mQtmSkeletonCache.Segments.ToList())
                {
                    var gameObject = new GameObject(this.SkeletonName + "_" + segment.Value.Name);
                    gameObject.transform.parent = segment.Value.ParentId == 0 ? mStreamedRootObject.transform : mQtmSegmentIdToGameObject[segment.Value.ParentId].transform;
                    gameObject.transform.localPosition = segment.Value.TPosition;
                    mQtmSegmentIdToGameObject[segment.Value.Id] = gameObject;
                }
                

                BuildMecanimAvatarFromQtmTPose();

                mStreamedRootObject.transform.SetParent(this.transform, false);
                mStreamedRootObject.transform.Rotate(new Vector3(0, 90, 0), Space.Self);
                return;
            }

            if (mQtmSkeletonCache == null)
                return;

            foreach (var segment in skeleton.Segments.ToList())
            {
                var cachedSegment = mQtmSkeletonCache.Segments[segment.Key];

                cachedSegment.Position = segment.Value.Position;
                cachedSegment.Rotation = segment.Value.Rotation;
            }

            // Update all the game objects
            List<GameObjectPosition> storedSegmentsList = new List<GameObjectPosition>(mQtmSegmentIdToGameObject.Count);
            foreach (var segmentPair in mQtmSkeletonCache.Segments.ToList())
            {
                storedSegmentsList.Add(new GameObjectPosition()
                {
                    SegmentId = segmentPair.Key,
                    Position = segmentPair.Value.Position,
                    Rotation = segmentPair.Value.Rotation,
                });
            }
            StoredPositions.Enqueue(storedSegmentsList);

            if (StoredPositions.Count > FrameDelay)
            {
                var storedPosition = StoredPositions.Dequeue();
                foreach (var segment in storedPosition)
                {
                    GameObject gameObject;
                    if (mQtmSegmentIdToGameObject.TryGetValue(segment.SegmentId, out gameObject))
                    {
                        gameObject.transform.localPosition = segment.Position;
                        gameObject.transform.localRotation = segment.Rotation;

                        //Debug.DrawLine(gameObject.transform.parent.transform.position, gameObject.transform.position, Color.red);
                    }
                }
                while (StoredPositions.Count > FrameDelay)
                    StoredPositions.Dequeue();
            }

            if (mSourcePoseHandler != null && mDestiationPoseHandler != null)
            {
                mSourcePoseHandler.GetHumanPose(ref mHumanPose);
                mDestiationPoseHandler.SetHumanPose(ref mHumanPose);
            }
        }

        private void BuildMecanimAvatarFromQtmTPose()
        {
            var humanBones = new List<HumanBone>(mQtmSkeletonCache.Segments.Count);
            for (int index = 0; index < HumanTrait.BoneName.Length; index++)
            {
                var humanBoneName = HumanTrait.BoneName[index];
                if (mMecanimToQtmSegmentNames.ContainsKey(humanBoneName))
                {
                    var bone = new HumanBone()
                    {
                        humanName = humanBoneName,
                        boneName = mMecanimToQtmSegmentNames[humanBoneName],
                    };
                    bone.limit.useDefaultValues = true;
                    humanBones.Add(bone);
                }
            }

            // Set up the T-pose and game object name mappings.
            var skeletonBones = new List<SkeletonBone>(mQtmSkeletonCache.Segments.Count + 1);
            skeletonBones.Add(new SkeletonBone()
            {
                name = this.SkeletonName,
                position = Vector3.zero,
                rotation = Quaternion.identity,
                scale = Vector3.one,
            });

            // Create remaining T-Pose bone definitions from Qtm segments
            foreach (var segment in mQtmSkeletonCache.Segments.ToList())
            {
                skeletonBones.Add(new SkeletonBone()
                {
                    name = this.SkeletonName + "_" + segment.Value.Name,
                    position = segment.Value.TPosition,
                    rotation = segment.Value.TRotation,
                    scale = Vector3.one,
                });
            }

            mSourceAvatar = AvatarBuilder.BuildHumanAvatar(mStreamedRootObject,
                new HumanDescription()
                {
                    human = humanBones.ToArray(),
                    skeleton = skeletonBones.ToArray(),
                }
            );
            if (mSourceAvatar.isValid == false || mSourceAvatar.isHuman == false)
            {
                this.enabled = false;
                return;
            }
            mSourcePoseHandler = new HumanPoseHandler(mSourceAvatar, mStreamedRootObject.transform);
            mDestiationPoseHandler = new HumanPoseHandler(DestinationAvatar, this.transform);
        }

        private void CreateMecanimToQtmSegmentNames(string skeletonName)
        {
            mMecanimToQtmSegmentNames.Clear();
            mMecanimToQtmSegmentNames.Add("RightShoulder", skeletonName + "_RightShoulder");
            mMecanimToQtmSegmentNames.Add("RightUpperArm", skeletonName + "_RightArm");
            mMecanimToQtmSegmentNames.Add("RightLowerArm", skeletonName + "_RightForeArm");
            mMecanimToQtmSegmentNames.Add("RightHand", skeletonName + "_RightHand");
            mMecanimToQtmSegmentNames.Add("LeftShoulder", skeletonName + "_LeftShoulder");
            mMecanimToQtmSegmentNames.Add("LeftUpperArm", skeletonName + "_LeftArm");
            mMecanimToQtmSegmentNames.Add("LeftLowerArm", skeletonName + "_LeftForeArm");
            mMecanimToQtmSegmentNames.Add("LeftHand", skeletonName + "_LeftHand");

            mMecanimToQtmSegmentNames.Add("RightUpperLeg", skeletonName + "_RightUpLeg");
            mMecanimToQtmSegmentNames.Add("RightLowerLeg", skeletonName + "_RightLeg");
            mMecanimToQtmSegmentNames.Add("RightFoot", skeletonName + "_RightFoot");
            mMecanimToQtmSegmentNames.Add("RightToeBase", skeletonName + "_RightToeBase");
            mMecanimToQtmSegmentNames.Add("LeftUpperLeg", skeletonName + "_LeftUpLeg");
            mMecanimToQtmSegmentNames.Add("LeftLowerLeg", skeletonName + "_LeftLeg");
            mMecanimToQtmSegmentNames.Add("LeftFoot", skeletonName + "_LeftFoot");
            mMecanimToQtmSegmentNames.Add("LeftToeBase", skeletonName + "_LeftToeBase");

            mMecanimToQtmSegmentNames.Add("Hips", skeletonName + "_Hips");
            mMecanimToQtmSegmentNames.Add("Spine", skeletonName + "_Spine");
            mMecanimToQtmSegmentNames.Add("Chest", skeletonName + "_Spine1");
            mMecanimToQtmSegmentNames.Add("UpperChest", skeletonName + "_Spine2");
            mMecanimToQtmSegmentNames.Add("Neck", skeletonName + "_Neck");
            mMecanimToQtmSegmentNames.Add("Head", skeletonName + "_Head");

            mMecanimToQtmSegmentNames.Add("Left Thumb Proximal", skeletonName + "_LeftHandThumb1");
            mMecanimToQtmSegmentNames.Add("Left Thumb Intermediate", skeletonName + "_LeftHandThumb2");
            mMecanimToQtmSegmentNames.Add("Left Thumb Distal", skeletonName + "_LeftHandThumb3");
            mMecanimToQtmSegmentNames.Add("Left Index Proximal", skeletonName + "_LeftHandIndex1");
            mMecanimToQtmSegmentNames.Add("Left Index Intermediate", skeletonName + "_LeftHandIndex2");
            mMecanimToQtmSegmentNames.Add("Left Index Distal", skeletonName + "_LeftHandIndex3");
            mMecanimToQtmSegmentNames.Add("Left Middle Proximal", skeletonName + "_LeftHandMiddle1");
            mMecanimToQtmSegmentNames.Add("Left Middle Intermediate", skeletonName + "_LeftHandMiddle2");
            mMecanimToQtmSegmentNames.Add("Left Middle Distal", skeletonName + "_LeftHandMiddle3");
            mMecanimToQtmSegmentNames.Add("Left Ring Proximal", skeletonName + "_LeftHandRing1");
            mMecanimToQtmSegmentNames.Add("Left Ring Intermediate", skeletonName + "_LeftHandRing2");
            mMecanimToQtmSegmentNames.Add("Left Ring Distal", skeletonName + "_LeftHandRing3");
            mMecanimToQtmSegmentNames.Add("Left Little Proximal", skeletonName + "_LeftHandPinky1");
            mMecanimToQtmSegmentNames.Add("Left Little Intermediate", skeletonName + "_LeftHandPinky2");
            mMecanimToQtmSegmentNames.Add("Left Little Distal", skeletonName + "_LeftHandPinky3");

            mMecanimToQtmSegmentNames.Add("Right Thumb Proximal", skeletonName + "_RightHandThumb1");
            mMecanimToQtmSegmentNames.Add("Right Thumb Intermediate", skeletonName + "_RightHandThumb2");
            mMecanimToQtmSegmentNames.Add("Right Thumb Distal", skeletonName + "_RightHandThumb3");
            mMecanimToQtmSegmentNames.Add("Right Index Proximal", skeletonName + "_RightHandIndex1");
            mMecanimToQtmSegmentNames.Add("Right Index Intermediate", skeletonName + "_RightHandIndex2");
            mMecanimToQtmSegmentNames.Add("Right Index Distal", skeletonName + "_RightHandIndex3");
            mMecanimToQtmSegmentNames.Add("Right Middle Proximal", skeletonName + "_RightHandMiddle1");
            mMecanimToQtmSegmentNames.Add("Right Middle Intermediate", skeletonName + "_RightHandMiddle2");
            mMecanimToQtmSegmentNames.Add("Right Middle Distal", skeletonName + "_RightHandMiddle3");
            mMecanimToQtmSegmentNames.Add("Right Ring Proximal", skeletonName + "_RightHandRing1");
            mMecanimToQtmSegmentNames.Add("Right Ring Intermediate", skeletonName + "_RightHandRing2");
            mMecanimToQtmSegmentNames.Add("Right Ring Distal", skeletonName + "_RightHandRing3");
            mMecanimToQtmSegmentNames.Add("Right Little Proximal", skeletonName + "_RightHandPinky1");
            mMecanimToQtmSegmentNames.Add("Right Little Intermediate", skeletonName + "_RightHandPinky2");
            mMecanimToQtmSegmentNames.Add("Right Little Distal", skeletonName + "_RightHandPinky3");
        }

        private void CreateFingerMappings(string skeletonName)
        {
            mMecanimFingers.Clear();
            mMecanimFingers.Add("Left Thumb Proximal", "LeftHandThumb1");
            mMecanimFingers.Add("Left Thumb Intermediate", "LeftHandThumb2");
            mMecanimFingers.Add("Left Thumb Distal", "LeftHandThumb3");
            mMecanimFingers.Add("Left Index Proximal", "LeftHandIndex1");
            mMecanimFingers.Add("Left Index Intermediate", "LeftHandIndex2");
            mMecanimFingers.Add("Left Index Distal", "LeftHandIndex3");
            mMecanimFingers.Add("Left Middle Proximal", "LeftHandMiddle1");
            mMecanimFingers.Add("Left Middle Intermediate", "LeftHandMiddle2");
            mMecanimFingers.Add("Left Middle Distal", "LeftHandMiddle3");
            mMecanimFingers.Add("Left Ring Proximal", "LeftHandRing1");
            mMecanimFingers.Add("Left Ring Intermediate", "LeftHandRing2");
            mMecanimFingers.Add("Left Ring Distal", "LeftHandRing3");
            mMecanimFingers.Add("Left Little Proximal", "LeftHandPinky1");
            mMecanimFingers.Add("Left Little Intermediate", "LeftHandPinky2");
            mMecanimFingers.Add("Left Little Distal", "LeftHandPinky3");

            mMecanimFingers.Add("Right Thumb Proximal", "RightHandThumb1");
            mMecanimFingers.Add("Right Thumb Intermediate", "RightHandThumb2");
            mMecanimFingers.Add("Right Thumb Distal", "RightHandThumb3");
            mMecanimFingers.Add("Right Index Proximal", "RightHandIndex1");
            mMecanimFingers.Add("Right Index Intermediate", "RightHandIndex2");
            mMecanimFingers.Add("Right Index Distal", "RightHandIndex3");
            mMecanimFingers.Add("Right Middle Proximal", "RightHandMiddle1");
            mMecanimFingers.Add("Right Middle Intermediate", "RightHandMiddle2");
            mMecanimFingers.Add("Right Middle Distal", "RightHandMiddle3");
            mMecanimFingers.Add("Right Ring Proximal", "RightHandRing1");
            mMecanimFingers.Add("Right Ring Intermediate", "RightHandRing2");
            mMecanimFingers.Add("Right Ring Distal", "RightHandRing3");
            mMecanimFingers.Add("Right Little Proximal", "RightHandPinky1");
            mMecanimFingers.Add("Right Little Intermediate", "RightHandPinky2");
            mMecanimFingers.Add("Right Little Distal", "RightHandPinky3");
        }

        private void CreateDefaultFingerSegments(string side, uint startingFingerSegmentsId, Vector3 thumbTranslation, Vector3 fingerTranslation)
        {
            // Add dummy Segments for right hand fingers to mQtmSkeletonCache (QTM streamed skeleton)
            foreach (var segmentPair in mQtmSkeletonCache.Segments)
            {
                var segment = segmentPair.Value;
                var handName = side + "Hand";
                if (segment.Name == side + "Hand")
                {
                    // Found hand, add fingers.
                    var parentId = segment.Id;

                    foreach (var fingerPair in mMecanimFingers.ToList())
                    {
                        var mecanimBoneName = fingerPair.Key;
                        var mappedBoneName = fingerPair.Value;
                        var fingersParentId = parentId;

                        if (!mecanimBoneName.Contains("Proximal"))
                            fingersParentId = startingFingerSegmentsId - 1;

                        if (mecanimBoneName.Contains(side))
                        {
                            Segment FingerSegment = new Segment
                            {
                                Name = mappedBoneName,
                                Id = startingFingerSegmentsId,
                                ParentId = fingersParentId,
                                Position = Vector3.zero,
                                Rotation = Quaternion.identity,
                                TPosition = mecanimBoneName.Contains("Thumb") ? thumbTranslation : fingerTranslation,
                                TRotation = Quaternion.identity
                            };
                            mQtmSkeletonCache.Segments.Add((uint)mQtmSkeletonCache.Segments.Count, FingerSegment);
                            startingFingerSegmentsId++;
                        }
                    }
                    break;
                }
            }
        }
    }
}