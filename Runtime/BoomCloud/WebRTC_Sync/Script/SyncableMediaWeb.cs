﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuplex;
using Vuplex.WebView;

public class SyncableMediaWeb : NetworkSyncableMedia
{
    
    IWebView webView;

    Texture2D outputTexture;
    public Renderer[] targetRenderers;
    public Vector2Int grid = new Vector2Int(1, 1);
    public Vector2 ratio = new Vector2(1f, 0.5625f);

    public bool autoSetGrid = false;
    MaterialPropertyBlock[] properties;

    public string[] shaderKeywords = { "_BaseMap" };
    public string shaderSTKeyword = "_MainTex_ST";

    public override void Initialize()
    {
        base.Initialize();
        webView = Web.CreateWebView();
        InitWebTexture();
    }

    public override void OnValueChange()
    {
        base.OnValueChange();
        if(webView != null && webView.IsInitialized)
        {
            webView.LoadUrl(value);
        }
    }    

    async void InitWebTexture()
    {
        outputTexture = await Web.CreateTexture(1f, 1f);
        outputTexture.filterMode = FilterMode.Point;
        //GetComponent<Renderer>().material.SetTexture("_BaseMap", texture);
        UpdateMaterialProperties();
        var webView = Web.CreateWebView();
        webView.Init(outputTexture, ratio.x, ratio.y);
        webView.LoadUrl(value);
    }


    void UpdateMaterialProperties()
    {

        if (targetRenderers == null || targetRenderers.Length == 0)
        {
            targetRenderers = GetComponentsInChildren<Renderer>();
        }

        if (properties == null && targetRenderers != null && targetRenderers.Length > 0)
        {
            properties = new MaterialPropertyBlock[targetRenderers.Length];
            for (int i = 0; i < properties.Length; i++)
            {
                properties[i] = new MaterialPropertyBlock();
            }
        }

        if (targetRenderers == null || targetRenderers.Length == 0)
        {
            Debug.LogWarning("No renderer found on object");
            return;
        }

        Vector2 size = new Vector2(1f / grid.x, 1f / grid.y);

        for (int i = 0; i < targetRenderers.Length; i++)
        {
            if (targetRenderers[i] == null)
                continue;

            Vector2 coords = new Vector2((i % grid.x) * size.x, (i / grid.y) * size.y);
            Debug.Log(coords);

            targetRenderers[i].GetPropertyBlock(properties[i]);

            if (autoSetGrid)
            {
                properties[i].SetVector(shaderSTKeyword, new Vector4(size.x, size.y, coords.x, coords.y));
            }

            foreach (string keyword in shaderKeywords)
            {
                properties[i].SetTexture(keyword, outputTexture);
            }

            targetRenderers[i].SetPropertyBlock(properties[i]);
        }

    }
}
