using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TV_Interruptor : MonoBehaviour
{
    private bool isApplicationQuitting = false;
    bool sceneIsUnloading = false;

    public enum Fade { In, Out}
    public Fade fade;
    private TV_Fading tV;

    private void Start()
    {
        tV = FindObjectOfType<TVObjectFinderHelper>().gameObject.GetComponent<TV_Fading>();
    }
    private void Awake()
    {
        Application.quitting += preventError;
        LoadingController.sceneIsLoading += preventError;
    }

    private void preventError()
    {
        sceneIsUnloading = true;
    }
    private void OnDisable()
    {
        if (isApplicationQuitting || sceneIsUnloading)
            return;

        doTheFade();
        Invoke("ReNable", 0.1f);
    }

    private void ReNable()
    {
        this.gameObject.SetActive(true);
    }


    public void doTheFade()
    {
        if(fade == Fade.In)
        {
            tV.StartFadeIn();
        }

        if (fade == Fade.Out)
        {
            tV.StartFadeOut();
        }
    }

    void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }
}
