﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkSyncableMedia : MonoBehaviour
{
    public static System.Action<NetworkSyncableMedia> SyncableMediaReady;

    public string identifier;

    [TextArea(3, 10)]
    public string value;

    string previousValue;

    protected virtual void Start()
    {
        SyncableMediaReady?.Invoke(this);
        if(value != "")
        {
            Initialize();
        }            
    }

    public virtual void Initialize()
    {

    }

    public void Update()
    {
        if(previousValue != value)
        {
            OnValueChange();
        }
        previousValue = value;
    }

    public virtual void OnValueChange()
    {

    }
}
