using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TV_Fading : MonoBehaviour
{
    public int fadingTime = 4;
    private Material TVShader;
    private float blendValue = 1;

    void OnEnable()
    {

        StageSynchronizer.PushEventStringReceived += ReceivedEvent;
    }

    void OnDisable()
    {

        StageSynchronizer.PushEventStringReceived -= ReceivedEvent;
    }


    private void Start()
    {
        TVShader = GetComponent<MeshRenderer>().material;
        blendValue = GetComponent<MeshRenderer>().material.GetFloat("_Opacity");
    }


    private void ReceivedEvent(string identifier, string value)
    {

        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.sender)
        {
            if (identifier == "TVFading")
            {
                switch (value)
                {
                    case "StartFadeIN":
                        StartFadeIn(true);
                        break;
                    case "StartFadeOUT":
                        StartFadeOut(true);
                        break;
                }
            }


            Debug.Log("STAGESYNC EVENT RECEIVED: " + identifier + " cue index: " + value);
        }

    }

    private void SendEvent(string identifier, string val)
    {
        if (!BoomCloudSettings.GetInstance().isStageSyncLinked)
            return;

        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.receiver)
        {
            BoomCloudSettings.GetInstance().stageSynchronizer.PushEvent(identifier, val);

            Debug.Log("CUE EVENT SENT" + identifier + " " + val);
        }
    }

    public void StartFadeIn(bool fromEvent = false)
    {
        if (!fromEvent)
            SendEvent("TVFading", "StartFadeIN");

        StartCoroutine(StartBlending(0, 1, fadingTime));
    }

    public void StartFadeOut(bool fromEvent = false)
    {
        if (!fromEvent)
            SendEvent("TVFading", "StartFadeOUT");

        StartCoroutine(StartBlending(1, 0, fadingTime));
    }


    IEnumerator StartBlending(float v_start, float v_end, float duration)
    {
        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            blendValue = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed += Time.deltaTime;
            TVShader.SetFloat("_Opacity", blendValue);
            yield return null;
        }
        if (v_end == 0)
        {
            TVShader.SetFloat("_Opacity", 0);
        }

        if (v_end == 1)
        {
            TVShader.SetFloat("_Opacity", 1);
        }

    }


}
