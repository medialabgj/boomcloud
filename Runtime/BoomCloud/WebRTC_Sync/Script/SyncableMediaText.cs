﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SyncableMediaText : NetworkSyncableMedia
{
    TextMeshPro textMesh;
    TextMeshPro[] textMeshs;
    TextMeshProUGUI[] textMeshsUGUI;
    void Awake()
    {        
        textMesh = GetComponent<TextMeshPro>();
        textMeshs = GetComponentsInChildren<TextMeshPro>();
        textMeshsUGUI = GetComponentsInChildren<TextMeshProUGUI>();
    }

    public override void OnValueChange()
    {
        if(textMesh != null)
        {
            textMesh.text = value;
        }

        if(textMeshs != null)
        {
            foreach(TextMeshPro tm in textMeshs)
            {
                tm.text = value;
            }
        }

        if(textMeshsUGUI != null)
        {
            foreach(TextMeshProUGUI tm in textMeshsUGUI)
            {
                tm.text = value;
            }
        }
    }
}
