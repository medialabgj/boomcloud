﻿BOOMCLOUD V20201102
-------------------

Boomcloud is a Cue, Event manger/trigger that is intended for controlling live show and multi-projection though unity.
----------------------------------------------------------------------------------------------------------------------


*Requierement:
- MidiPad Controller (Launpad Mk3 is supported by default, but other pad should work with some tweaking)
- Xbox Remote control (Xbox supported by default, other might work as well)
- Cinemachinee  (2.5.0)
- Input System (1.0)
- Universal Rp (7.3.1)



Features list:
-------------

- Camera Cue system for controling camera live with Remote (xBox)
- Switching Cameras System, can add as many cue as needed, can be cut cue or predefined movement (advanced)
- Can handle multiple Camera Output thgout spout. And Cue system for each camera
- Action cue system for displaying/hiding object, can be use also for interuptor systeme for any action (onDisable)
- Audio Cue System for playing, stoping, managing transition and fading of audio
- Cue can be sync 
- Scene loading Cue System
- basic UI with ACtion cue INFO and Camera cue, and audio Cue (Missing scene cue at the moment)
- Integration with Qualisys mocap Systeme, and Rokoko by default
- Spout is integrated, and with combining with resolume arena or similar software, you can do multi-screen projection. (Be carefull though spout is super heavy in process.. )


Important concept:
------------------

Some important concept for BoomCloud to work is to understand camera settings. Camera use Cinemachine to work, and it mean that for multiple camera to work, each camera needs to be on there own layer. The camera brain, and virtual camera. Also the culling of the camera needs to hide other camera. 
Also the settings for now are a bit all over the place, but important one are the following:
	- Create a layer for each Camera (VERY important)
	- Set the culling of camera to see only their own layer!
	- Assign in BoomCloudMaster/BoomCloud the row (on midi controller) and layer for camera cue to be displayed
	- Assign also row for Audio/Scene/Action cue and you can assign color as well if your midi device support it
	- Assign also Layer in BoomCloudMaster/BoomCloudUI/UIManager in cmaera outline for preview assign camera layer for the outline to recognize what camera is what


BoomCloud is composed of many prefab and component, they need to be all the time in the same scene and not move. You need to add BoomCloudSceneLoader on each different scene. This is the component that will load all the cue And camera and update the UI, if you have multiple scene it need to be on each scene you load. 
On the boomCloud Scene loader here are the feature:
	- Camera cue, you can name your cue and add virtual camera for the cue. 
	- Also you can start with cue right after loading a scene (default is -1 meaning the setting will be ignored)
	- Qualisys prob is for qualisys object (this feature is outdated at the moment)
	- Action cue can Hide/show object and start audio cue as well if need to sync action to audio. You can also customize the action by adding your own (check code for that) but also you can use object that will trigger action (interuptor) by using onDisable on an object and assigning any task to it.
	- Audio Cue to start/stop audio and defining fade time and volume


The system is still in developpement you might encounter bug and issue here and there. Settings are a bit all over the place at the moment, it would need a good global rework to put setting all at the same place (but trickier than it look)

RoadMap
-------
	- Improve UI backend and frontend 
	- Simplify settings and installation
	- Integrate more midi device by default
	- ...


Contact:
--------

For any question/request contact me camilo@cadema.io