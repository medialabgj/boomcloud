﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Playables;
public class ObjectController : MonoBehaviour
{
    private static ObjectController instance;
  //  public static event System.Action<PlayableDirector> TimelineEvent;


    [HideInInspector]
    public List<GameObject> grids;
    public List<GameObject> AvatarToTest;
    public List<GameObject> randomAvatar;

    private GameObject[] parents;
    bool gridOnOff = true;

    private int previousAvatarIndex = -1;
    private int previousInComingIndex = -1;

    List<GameObject> previousAvatarsSUS;
    List<GameObject> previousAvatarsMAE;

    GameObject avatar;


    public static ObjectController GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;
        MidiDeviceController.ToggleGrid += ToggleGrid;

        previousAvatarsSUS = new List<GameObject>();
        previousAvatarsMAE = new List<GameObject>();


        grids = new List<GameObject>();
    }

    private void OnEnable()
    {
        SceneController.InitScene += init;
        //RemoteObjectController.RemoteObjects += ReceivedObjects;
    }

    private void OnDisable()
    {
        SceneController.InitScene -= init;
        //RemoteObjectController.RemoteObjects -= ReceivedObjects;

    }
    private void init(DataScene dataScene)
    {
        initGridStatus(dataScene.Grid);
    }
    public void initGridStatus(List<GameObject> gridObject)
    {
        grids = gridObject;
        MidiLedController.GetInstance().ledColorStatus(gridOnOff);
        hideGrid();       //hide gride by default
    }

    private void ReceivedObjects(PlayableDirector timeline)
    {
       // TimelineEvent?.Invoke(timeline);
    }
    public void navigateThroughAvatar(float index, string avatarName)    //Receive index from slider 0 to 1
    {
        if (AvatarToTest == null)
        {
            Log.LogMe("No avatar to test");
            return;
        }

        int denormalizedIndex = (int)Mathf.Lerp(-1, AvatarToTest.Count - 1, index);

        if (denormalizedIndex == previousInComingIndex)
        {
            return;
        }

        previousInComingIndex = denormalizedIndex;

        if (denormalizedIndex != previousAvatarIndex)
        {
            parents = GameObject.FindGameObjectsWithTag("AvatarParent");
            foreach (GameObject parent in parents)  //Applique les instances sur tous les avatar présent dans la scene
            {
                if (denormalizedIndex > -1)          //Show regular avatar
                {

                    foreach (Transform child in parent.transform)
                    {
                        if (child.CompareTag("clone" + avatarName))
                        {
                            Destroy(child.gameObject);
                        }
                    }

                    avatar = Instantiate(AvatarToTest[denormalizedIndex], parent.transform.position, parent.transform.rotation, parent.transform);

                    /* QUALIYS COMPONENT 
                    avatar.AddComponent<RTSkeletonRB>();
                    avatar.GetComponent<RTSkeletonRB>().DestinationAvatar = avatar.GetComponent<Animator>().avatar;
                    avatar.GetComponent<RTSkeletonRB>().SkeletonName = avatarName;
                    */

                   // avatar.AddComponent<SyncableAvatar>();
                   // avatar.GetComponent<SyncableAvatar>().identifier = avatarName;

                    avatar.tag = "clone" + avatarName;

                    foreach (Transform child in parent.transform)
                    {
                        if (child.name == avatarName)
                        {
                            child.gameObject.SetActive(false);
                        }
                    }
                }
                else
                {
                    foreach (Transform child in parent.transform)
                    {
                        if (child.CompareTag("clone" + avatarName))
                        {
                            Destroy(child.gameObject);
                        }
                        if (child.name == avatarName)
                        {
                            child.gameObject.SetActive(true);
                        }
                    }
                }
            }
        }
        previousAvatarIndex = denormalizedIndex;
    }
    public void randomizeAlienAvatar()
    {
        if (randomAvatar == null)
        {
            Log.LogMe("No Alineavatar");
            return;
        }


        parents = GameObject.FindGameObjectsWithTag("alienAvatarParent");
        foreach (GameObject parent in parents)
        {
            
            foreach (Transform child in parent.transform)  //Applique les instances sur tous les avatar présent dans la scene
            {
                int randomApplication = (int)Random.Range(0, randomAvatar.Count);

                if (child.name == "SUS")
                {
                  //  Debug.Log("FOUND SUS");
                    child.gameObject.SetActive(false);

                    avatar = Instantiate(randomAvatar[randomApplication], parent.transform.position, parent.transform.rotation, parent.transform);
                 //   avatar.AddComponent<RTSkeletonRB>();
                //    avatar.GetComponent<RTSkeletonRB>().DestinationAvatar = avatar.GetComponent<Animator>().avatar;
                //    avatar.GetComponent<RTSkeletonRB>().SkeletonName = child.name;
                    avatar.tag = "clonedAlien";
                }

                if (child.name == "MAE")
                {
                //    Debug.Log("FOUND MAE");
                    child.gameObject.SetActive(false);

                    avatar = Instantiate(randomAvatar[randomApplication], parent.transform.position, parent.transform.rotation, parent.transform);
                //    avatar.AddComponent<RTSkeletonRB>();
                //    avatar.GetComponent<RTSkeletonRB>().DestinationAvatar = avatar.GetComponent<Animator>().avatar;
                ///    avatar.GetComponent<RTSkeletonRB>().SkeletonName = child.name;
                    avatar.tag = "clonedAlien";
                   // child.gameObject.SetActive(false);
                }
                /*
                /*
                if (child.CompareTag("clonedAlien"))
                {
                    Destroy(child.gameObject);
                }
                */
                /*
                avatar = Instantiate(alienAvatar[randomApplication], parent.transform.position, parent.transform.rotation, parent.transform);
              
                avatar.AddComponent<RTSkeletonRB>();
                avatar.GetComponent<RTSkeletonRB>().DestinationAvatar = avatar.GetComponent<Animator>().avatar;
                avatar.GetComponent<RTSkeletonRB>().SkeletonName = child.name;
                avatar.tag = "clonedAlien" + child.name;
                */
                /*
                if (child.name == child.name)
                {
                    child.gameObject.SetActive(false);
                }
                */
                
            }
        }


            //   }

            /*
            else
            {
                foreach (Transform child in parent.transform)
                {
                    if (child.CompareTag("clone" + avatarName))
                    {
                        Destroy(child.gameObject);
                    }
                    if (child.name == avatarName)
                    {
                        child.gameObject.SetActive(true);
                    }
                }
            }
            */
        }



        public void ToggleGrid()
        {
            if (gridOnOff)
            {
                hideGrid();
            }
            else
            {
                displayGrid();
            }
        }

        private void displayGrid()
        {
            foreach (GameObject grid in grids)
            {
                grid.SetActive(true);
            }
            gridOnOff = true;
            MidiLedController.GetInstance().ledColorStatus(gridOnOff);

        }

        private void hideGrid()
        {
        if (grids == null)
            return;

        foreach (GameObject grid in grids)
            {
                grid.SetActive(false);
            }

            gridOnOff = false;
            MidiLedController.GetInstance().ledColorStatus(gridOnOff);

        }

        public void DuplicationStart(string DanserSelection)
        {
            GameObject[] ParentObject;
            List<object> duplicatorObject = new List<object>();

            ParentObject = GameObject.FindGameObjectsWithTag("AvatarParent");
            DuplicatorController[] duplicators = GameObject.FindObjectsOfType<DuplicatorController>();

            duplicators.Where(d => d.DanserName == DanserSelection).ToList().ForEach(d =>
            {
                d.StartDuplication();
            });


            /*
            foreach (GameObject avatar in ParentObject)
            {
                duplicatorObject.Add(FindObjectsOfType<DuplicatorController>());
            }



            foreach (DuplicatorController componentDuplication in duplicatorObject)
            {
                if (componentDuplication.DanserName == DanserSelection)
                {
                    componentDuplication.StartDuplication();
                }
            }

        */

        }
    }
