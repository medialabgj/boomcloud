﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidiTesting : MonoBehaviour
{

    List<int> A = new List<int> { 00000000, 00111100, 01100110, 01100110, 01111110, 01100110, 01100110, 01100110 };
    List<int> B = new List<int> { 01111000, 01001000, 01001000, 01110000, 01001000, 01000100, 01000100, 01111100 };
    List<int> C = new List<int> { 00000000, 00011110, 00100000, 01000000, 01000000, 01000000, 00100000, 00011110 };
    List<int> D = new List<int> { 00000000, 00111000, 00100100, 00100010, 00100010, 00100100, 00111000, 00000000 };
    List<int> E = new List<int> { 00000000, 00111100, 00100000, 00111000, 00100000, 00100000, 00111100, 00000000 };
    List<int> F = new List<int> { 00000000, 00111100, 00100000, 00111000, 00100000, 00100000, 00100000, 00000000 };
    List<int> G = new List<int> { 00000000, 00111110, 00100000, 00100000, 00101110, 00100010, 00111110, 00000000 };
    List<int> H = new List<int> { 00000000, 00100100, 00100100, 00111100, 00100100, 00100100, 00100100, 00000000 };
    List<int> I = new List<int> { 00000000, 00111000, 00010000, 00010000, 00010000, 00010000, 00111000, 00000000 };
    List<int> J = new List<int> { 00000000, 00011100, 00001000, 00001000, 00001000, 00101000, 00111000, 00000000 };
    List<int> K = new List<int> { 00000000, 00100100, 00101000, 00110000, 00101000, 00100100, 00100100, 00000000 };
    List<int> L = new List<int> { 00000000, 00100000, 00100000, 00100000, 00100000, 00100000, 00111100, 00000000 };
    List<int> M = new List<int> { 00000000, 00000000, 01000100, 10101010, 10010010, 10000010, 10000010, 00000000 };
    List<int> N = new List<int> { 00000000, 00100010, 00110010, 00101010, 00100110, 00100010, 00000000, 00000000 };
    List<int> O = new List<int> { 00000000, 00111100, 01000010, 01000010, 01000010, 01000010, 00111100, 00000000 };
    List<int> P = new List<int> { 00000000, 00111000, 00100100, 00100100, 00111000, 00100000, 00100000, 00000000 };
    List<int> Q = new List<int> { 00000000, 00111100, 01000010, 01000010, 01000010, 01000110, 00111110, 00000001 };
    List<int> R = new List<int> { 00000000, 00111000, 00100100, 00100100, 00111000, 00100100, 00100100, 00000000 };
    List<int> S = new List<int> { 00000000, 00111100, 00100000, 00111100, 00000100, 00000100, 00111100, 00000000 };
    List<int> T = new List<int> { 00000000, 01111100, 00010000, 00010000, 00010000, 00010000, 00010000, 00000000 };
    List<int> U = new List<int> { 00000000, 01000010, 01000010, 01000010, 01000010, 00100100, 00011000, 00000000 };
    List<int> V = new List<int> { 00000000, 00100010, 00100010, 00100010, 00010100, 00010100, 00001000, 00000000 };
    List<int> W = new List<int> { 00000000, 10000010, 10010010, 01010100, 01010100, 00101000, 00000000, 00000000 };
    List<int> X = new List<int> { 00000000, 01000010, 00100100, 00011000, 00011000, 00100100, 01000010, 00000000 };
    List<int> Y = new List<int> { 00000000, 01000100, 00101000, 00010000, 00010000, 00010000, 00010000, 00000000 };
    List<int> Z = new List<int> { 00000000, 00111100, 00000100, 00001000, 00010000, 00100000, 00111100, 00000000 };




    private void Start()
    {
        Invoke("lateStarte", 2f);
    }

    void lateStarte()
    {
         StartCoroutine("test");
       // displayLetter(G);
    }


    private MidiLedController midiSend()
    {
        return MidiLedController.GetInstance();
    }

    IEnumerator  test()
    {
        //MidiLedController.GetInstance().manualMidiSend(4, 6);
        int i = 0;
        int d = (int)LogicController.dataScene.matrix.y;
        bool switcherino = false;
        while (i < d)
        {
            //MARCHE QUE POUR MATRIX CARREEEE!

            int randColor = Random.Range(0, 127);
            for (int e = 0; e < LogicController.dataScene.matrix.x; e++)
            {
                if (switcherino)
                {
                    midiSend().manualMidiSend(i, e, randColor);

                }
                else
                {
                    midiSend().manualMidiSend(e, i, randColor);
                }

                yield return new WaitForSeconds(0.01f);
            }
            yield return new WaitForSeconds(0.01f);

            i++;

            if (i >= d)
            {
                i = 0;
                switcherino = toggle(switcherino);
            }
        }
        yield return null;
    }

    void displayLetter(List<int> letter)
    {
        midiSend().ClearLedPanel();

        int i = 0;
        int d = (int)LogicController.dataScene.matrix.y;
        while (i < d)
        {
            int randColor = Random.Range(0, 127);
            for (int e = 0; e < LogicController.dataScene.matrix.x; e++)
            {
                if (nthdigit(letter[i], e) == 1)
                {
                    midiSend().manualMidiSend(e, i, randColor);
                }
            }

            i++;
        }
    }

    int nthdigit(int x, int n)
    {
        while (n-- > 0)
        {
            x /= 10;
        }
        return (x % 10);
    }

    private bool toggle(bool boole)
    {
        if (boole == true)
        {
            boole = false;
        }
        else
        {
            boole = true;
        }

        return boole;
    }

}
