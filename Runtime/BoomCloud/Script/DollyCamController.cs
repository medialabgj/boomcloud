using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;
using RtMidi.LowLevel;
using StageTools;
using UnityEngine.Playables;
using System.Linq;
using System;
public class DollyCamController : MonoBehaviour
{

    private static DollyCamController instance;

    
    private List<LayerMask> cameraLayer = new List<LayerMask>();

    private Dictionary<int, DollyData> DictDolly { get; set; } = new Dictionary<int, DollyData>();
    private Dictionary<int, float> DictFaderCam { get; set; } = new Dictionary<int, float>();

    private Dictionary<int, float> DictTiltCam { get; set; } = new Dictionary<int, float>();

    private Dictionary<int, float> DictPathValueCam { get; set; } = new Dictionary<int, float>();
    private Dictionary<int, bool> DictRev { get; set; } = new Dictionary<int, bool>();

    private bool isReverseTimeline = false;
    private PlayableDirector Actiontimeline = new PlayableDirector();
    private float timelineFaderValue = 0;
    private float timeTagetValue = 0;

    public int speedDivider = 1;

    public bool playTimeline = false;

    private bool isReloading = false;

    private DollyData dd;

    [SerializeField]
    private string targetDevice = "APC40 mkII";
    MidiProbe _probe;
    List<MidiOutPort> _ports = new List<MidiOutPort>();
    private enum PortSelection { port0, port1, port2 };
    [SerializeField]
    private PortSelection portSelection;
    bool noMidi = true;
    MidiOutPort midiDevicePort;



    public static DollyCamController GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;
        dd = new DollyData(null, DollyData.PathType.NaN, false);



    }



    private void OnEnable()
    {
        CameraController.DollyCamEvent += DollyCamManager;
        SceneController.InitScene += InitDollyController;
        TimelineCtrlTrigger.TimelineEvent += InitTimeline;
        //TimelineIDInteruptor.TimelineReadyEvent += TimelineIDReady;
        StageSynchronizer.PushEventFloatReceived += ReceivedFaderEvent;
        StageSynchronizer.PushEventIntReceived += ReceivedRevertEvent;
        StageSynchronizer.PushEventStringReceived += ReceivedMsgEvent;
        DollySpeedAjuster.DollySpeedChanged += ChangeDollySpeed;
    }

    private void OnDisable()
    {
        CameraController.DollyCamEvent -= DollyCamManager;
        SceneController.InitScene -= InitDollyController;
        TimelineCtrlTrigger.TimelineEvent -= InitTimeline;
        //TimelineIDInteruptor.TimelineReadyEvent -= TimelineIDReady;
        StageSynchronizer.PushEventFloatReceived -= ReceivedFaderEvent;
        StageSynchronizer.PushEventIntReceived -= ReceivedRevertEvent;
        DollySpeedAjuster.DollySpeedChanged -= ChangeDollySpeed;


        ClearLedPanel();
    }

    private void Start()
    {
        //Get camera layer settings
        BoomCloudSettings.GetInstance().cameraSettings.ForEach(c => cameraLayer.Add(c.layer));


        _probe = new MidiProbe(MidiProbe.Mode.Out);
        ScanPorts();
        // Datascene().setMatrixSize(matrixSize);

        if (_ports.Count > 0)
        {
            midiDevicePort = _ports[(int)portSelection];
            noMidi = false;
            Log.LogMe("Device: " + targetDevice + " founded  on port" + _ports[(int)portSelection]);
        }
        else
        {
            noMidi = true;
            Debug.LogWarning("BoomCloud: No MIDI Fader device found!");
        }

        if (noMidi)
            return;


        InputSystem.onDeviceChange += (device, change) =>
        {
            var midiDevice = device as Minis.MidiDevice;
            if (midiDevice == null) return;

            midiDevice.onWillNoteOn += (note, velocity) => {

                if (string.IsNullOrEmpty(targetDevice) || !note.device.description.product.Contains(targetDevice))
                {
                    return;
                }


              //  Debug.Log("Device " + note.device.description.product + " note matching target " + targetDevice + " " + note);


            };
        };

        for (int i = 0; i < 127; i++)    //Erase all led
        {
            if (noMidi)
                return;

            midiDevicePort.SendNoteOn(0, i, 127);
        }



    }

    private void InitDollyController(DataScene data)
    {

        /*
         A TESTER
        DictDolly.Clear();
        DictFaderCam.Clear();
        DictRev.Clear();
        DictPathValueCam.Clear();
        DictTiltCam.Clear();
         
         */
        ResetDict();

        data.GetAllDifferentCamera().ForEach(layer => {
            DictDolly.Add(layer, dd);
            DictFaderCam.Add(layer, new float());
            DictRev.Add(layer, false);
            DictPathValueCam.Add(layer, 0);
            DictTiltCam.Add(layer, new float());
        });
        OnResetAllDollyPosition();
        ClearLedPanel();
    }

    public void ResetDict()
    {
        

        DictDolly.Clear();
        DictFaderCam.Clear();
        DictRev.Clear();
        DictPathValueCam.Clear();
        DictTiltCam.Clear();
    }


    public void StopUpdate()
    {
        isReloading = true;
    }

    public void ResumeUpdate()
    {
        isReloading = false;
    }

    private void InitTimeline(PlayableDirector timeline, float timesclae)
    {
        if(timeline != null)
        {

            Actiontimeline = timeline;

            if (playTimeline)
            {
                Actiontimeline.time = 0;
                Actiontimeline.Play();
                Actiontimeline.playableGraph.GetRootPlayable(0).SetSpeed(timesclae);
            }

            if (noMidi)
                return;
            midiDevicePort.SendNoteOn(0, 22, 127);
        }
        else
        {
            Actiontimeline = null;

            if (noMidi)
                return;
            midiDevicePort.SendNoteOn(0, 22, 0);
        }
    }

    /*
    private void TimelineIDReady(string timelineName)
    {
        if(Actiontimeline == null)
        {
            Debug.LogWarning("BOOMCLOUD timeline not initiated!");
            return;
        }

        if(Actiontimeline.playableAsset.name == timelineName)
        {
            isTimelineIDMatched = true;
            Debug.Log("BoomCloud Timeline Matching! " + Actiontimeline.playableAsset.name + " " + timelineName);
        }
        else
        {
            Debug.Log("BoomCloud Timeline NOT Matching! " + Actiontimeline.playableAsset.name + " " + timelineName);
        }
    }
    */

    private void ChangeDollySpeed(int speed)
    {
        speedDivider = speed;
       // Debug.Log("Dolly Speed Changed to " + speedDivider);
    }

    int cameraLayerValue(int cameraNumber)
    {
        return LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[cameraNumber], 2)));
    }

    private void ReceivedFaderEvent(string identifier, float value)
    {
        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.sender)
        {

            if (identifier == "Fader" + cameraLayerValue(0).ToString())
            {
                DictPathValueCam[cameraLayerValue(0)] = value;
            }

            if (identifier == "Fader" + cameraLayerValue(1))
            {
                DictPathValueCam[cameraLayerValue(1)] = value;
            }

            if (identifier == "Fader" + cameraLayerValue(2))
            {
                DictPathValueCam[cameraLayerValue(2)] = value;
            }

            if (identifier == "Fader" + cameraLayerValue(3))
            {
                DictPathValueCam[cameraLayerValue(3)] = value;
            }
            

            if (identifier == "Tilt1")
            {
                DictTiltCam[cameraLayerValue(0)] = value;
            }

            if (identifier == "Tilt2")
            {
                DictTiltCam[cameraLayerValue(1)] = value;
            }

            if (identifier == "Tilt3")
            {
                DictTiltCam[cameraLayerValue(2)] = value;
            }


            if (identifier == "TimelineFader")
            {
                timeTagetValue = value;
            }
        }

      //  Debug.Log("RECEVIED FADER EVENT");
    }

    public bool intToBool(int Number)
    {
        return (Number == 0 ? false : true);
    }

    private void ReceivedRevertEvent(string identifier, int BoolInt)
    {

        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.sender)
        {
            if (identifier == "Rev1")
            {
                DictRev[cameraLayerValue(0)] = intToBool(BoolInt);
            }

            if (identifier == "Rev2")
            {
                DictRev[cameraLayerValue(1)] = intToBool(BoolInt);
            }

            if (identifier == "Rev3")
            {
                DictRev[cameraLayerValue(2)] = intToBool(BoolInt);
            }

            if (identifier == "Rev4")
            {
                DictRev[cameraLayerValue(3)] = intToBool(BoolInt);
            }

            if (identifier == "RevTimeline")
            {
                isReverseTimeline = intToBool(BoolInt);
            }
        }

       // Debug.Log("RECEVIED REVERT EVENT");

    }

    private void ReceivedMsgEvent(string identifier, string Message)
    {
        if(identifier == "DollyCam" && Message == "ResetDolly")
        {
            ResetDollyPosition();
        }
    }

    private void SendMsgEvent(string Message)
    {
        if (!BoomCloudSettings.GetInstance().isStageSyncLinked)
            return;

        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.receiver)
        {
            BoomCloudSettings.GetInstance().stageSynchronizer.PushEvent("DollyCam", Message);
        }
    }

    private void SendFaderEvent(string identifier, float Fadervalue)
    {
        if (!BoomCloudSettings.GetInstance().isStageSyncLinked)
            return;

        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.receiver)
        {
            BoomCloudSettings.GetInstance().stageSynchronizer.PushEvent(identifier, Fadervalue);
        }

        //Debug.Log("FADER EVENT SENT");
    }

    private void SendRevEvent(string identifier, int BoolValue)
    {
        if (!BoomCloudSettings.GetInstance().isStageSyncLinked)
            return;

        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.receiver)
        {
            BoomCloudSettings.GetInstance().stageSynchronizer.PushEvent(identifier, BoolValue);
        }
       // Debug.Log("REVERT EVENT SENT");

    }
    void ClearLedPanel()
    {
        for (int i = 0; i < 127; i++)    //Erase all led
        {
            if (noMidi)
                return;

            midiDevicePort.SendNoteOn(0, i, 0);
        }
    }

    void ScanPorts()
    {
        for (var i = 0; i < _probe.PortCount; i++)
        {
            var name = _probe.GetPortName(i);
            if (IsRealPort(name))
            {
                _ports.Add(new MidiOutPort(i));
            }

        }
    }

    bool IsRealPort(string name)
    {
        return !name.Contains("Through") && !name.Contains("RtMidi") && name.Contains(targetDevice);
    }


    private void DollyCamManager(CinemachineVirtualCamera camera, int layer, DollyData.PathType pathType, bool onOff)
    {
        DictDolly[layer] = new DollyData(camera, pathType, onOff);
        DictPathValueCam[layer] = 0;
        DictFaderCam[layer] = 0;
        if (pathType == DollyData.PathType.follow)
            return;

        Ledmanger(layer, onOff);

    }

    void Ledmanger(int layer, bool OnOff)
    {
        if (noMidi)
            return;

        int color = 0;

        if (OnOff)
            color = 127;



        if (cameraLayer.Count >= 1 &&  layer == cameraLayerValue(0))
        {
            midiDevicePort.SendNoteOn(0, 1, color);
        }

        if (cameraLayer.Count >= 2 && layer == cameraLayerValue(1))
        {
            midiDevicePort.SendNoteOn(0, 4, color);
        }

        if (cameraLayer.Count >= 3 && layer == cameraLayerValue(2))
        {
            midiDevicePort.SendNoteOn(0, 7, color);
        }

        if (cameraLayer.Count >= 4 && layer == cameraLayerValue(3))
        {
            midiDevicePort.SendNoteOn(0, 10, color);
        }
    }

    void OnResetAllDollyPosition()
    {
        ResetDollyPosition();
        SendMsgEvent("ResetDolly");
    }

    void ResetDollyPosition()
    {
        FindObjectsOfType<CinemachineVirtualCamera>().ToList().ForEach(o => {
            if (o.GetCinemachineComponent<CinemachineTrackedDolly>())
            {
                o.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition = 0;
            }

        });
    }

    void OnFader1(InputValue value)
    {
        DictFaderCam[cameraLayerValue(0)] = value.Get<float>();
       // SendFaderEvent("Fader1", value.Get<float>());
    }

    void OnTilt1(InputValue value)
    {
        DictTiltCam[cameraLayerValue(0)] = value.Get<float>();
        SendFaderEvent("Tilt1", value.Get<float>());
    }

    void OnRev1()
    {
        int camNumber = 0;

        DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))] = !DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))];

        if (DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))])
        {
            midiDevicePort.SendNoteOn(0, 3,127);
        }
        else
        {
            midiDevicePort.SendNoteOn(0, 3,0);
        }

        SendRevEvent("Rev1", Convert.ToInt32(DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))]));

    }

    void OnFader2(InputValue value)
    {
        DictFaderCam[cameraLayerValue(1)] = value.Get<float>();
       // SendFaderEvent("Fader2", value.Get<float>());
    }

    void OnTilt2(InputValue value)
    {
        DictTiltCam[cameraLayerValue(1)] = value.Get<float>();
        SendFaderEvent("Tilt2", value.Get<float>());
    }


    void OnRev2()
    {
        int camNumber = 1;

        DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))] = !DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))];

        if (DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))])
        {
            midiDevicePort.SendNoteOn(0, 6, 127);
        }
        else
        {
            midiDevicePort.SendNoteOn(0, 6, 0);
        }

        SendRevEvent("Rev2", Convert.ToInt32(DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))]));

    }


    void OnFader3(InputValue value)
    {
        DictFaderCam[cameraLayerValue(2)] = value.Get<float>();
       // SendFaderEvent("Fader3", value.Get<float>());

    }

    void OnTilt3(InputValue value)
    {
        DictTiltCam[cameraLayerValue(2)] = value.Get<float>();
        SendFaderEvent("Tilt3", value.Get<float>());
    }


    void OnRev3()
    {
        int camNumber = 2;

        DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))] = !DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))];

        if (DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))])
        {
            midiDevicePort.SendNoteOn(0, 9, 127);
        }
        else
        {
            midiDevicePort.SendNoteOn(0, 9, 0);
        }

        SendRevEvent("Rev3", Convert.ToInt32(DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))]));

    }

    void OnFader4(InputValue value)
    {
        DictFaderCam[cameraLayerValue(3)] = value.Get<float>();
        //SendFaderEvent("Fader4", value.Get<float>());
    }

    void OnRev4()
    {
        int camNumber = 3;

        DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))] = !DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))];

        if (DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))])
        {
            midiDevicePort.SendNoteOn(0, 12, 127);
        }
        else
        {
            midiDevicePort.SendNoteOn(0, 12, 0);
        }

        SendRevEvent("Rev4", Convert.ToInt32(DictRev[LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(cameraLayer[camNumber], 2)))]));

    }

    void OnTimelineFader(InputValue value)
    {
        timelineFaderValue = value.Get<float>();
        //SendFaderEvent("TimelineFader", timelineFaderValue);
    }

    void OnRevTimelineFader()
    {
        isReverseTimeline = !isReverseTimeline;

        if (isReverseTimeline)
        {
            midiDevicePort.SendNoteOn(0, 24, 127);
        }
        else
        {
            midiDevicePort.SendNoteOn(0, 24, 0);
        }

        SendRevEvent("RevTimeline", Convert.ToInt32(isReverseTimeline));

    }

    private void Update()
    {
        if (isReloading)
            return;
            
        foreach(KeyValuePair<int, DollyData> keysValues in DictDolly)
        {
            if (keysValues.Value.isEnabled)
            {
                /*
                if(keysValues.Value.typeOfPath == DollyData.PathType.cinemachine)
                {
                    var inc = DictFaderCam[keysValues.Key]/speedDivider;

                    if (DictRev[keysValues.Key])
                        inc = inc * -1;

                    keysValues.Value.camera.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition += inc;
                    if (keysValues.Value.camera.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition < 0)
                        keysValues.Value.camera.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition = 0;
                    if (keysValues.Value.camera.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition > keysValues.Value.camera.GetCinemachineComponent<CinemachineTrackedDolly>().m_Path.PathLength)
                        keysValues.Value.camera.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition = keysValues.Value.camera.GetCinemachineComponent<CinemachineTrackedDolly>().m_Path.PathLength;
                }
                */

                if (keysValues.Value.typeOfPath == DollyData.PathType.stageTools)
                {
                    var inc = DictFaderCam[keysValues.Key] / speedDivider;
                 //   keysValues.Value.camera.GetComponent<PathWayPointManualCameraFinder>().stageCameraPath.la
                    if (DictRev[keysValues.Key])
                        inc = inc * -1;

                    DictPathValueCam[keysValues.Key] += inc;
                    keysValues.Value.camera.GetComponent<PathWayPointManualCameraFinder>().stageCameraPath.UpdateTargetFromTime(DictPathValueCam[keysValues.Key]);
                    //keysValues.Value.camera.GetComponent<PathWayPointManualCameraFinder>().stageCameraPath.GetStateFromGlobalRealTime(DictPathValueCam[keysValues.Key]);
                    SendFaderEvent("Fader" + keysValues.Key.ToString(), DictPathValueCam[keysValues.Key]);

                    if (DictPathValueCam[keysValues.Key] < 0)
                        DictPathValueCam[keysValues.Key] = 0;
                    if (DictPathValueCam[keysValues.Key] > keysValues.Value.camera.GetComponent<PathWayPointManualCameraFinder>().stageCameraPath.GetTotalTime())
                        DictPathValueCam[keysValues.Key] = keysValues.Value.camera.GetComponent<PathWayPointManualCameraFinder>().stageCameraPath.GetTotalTime();
                }

                if (keysValues.Value.typeOfPath == DollyData.PathType.follow)
                {
                    keysValues.Value.camera.GetComponent<WaypointFollowOtherPath>().stageCameraPath.UpdateTargetFromTime(DictPathValueCam[cameraLayerValue(1)]);
                    //keysValues.Value.camera.GetComponent<WaypointFollowOtherPath>().stageCameraPath.UpdateTargetFromTime(keysValues.Value.camera.GetComponent<WaypointFollowOtherPath>().PathToFollow.);

                }

                if (keysValues.Value.camera.GetComponent<CinemachineRecomposer>() != null)
                {
                    float transitionTitleValue =  DictTiltCam[keysValues.Key] * -30;
                    keysValues.Value.camera.GetComponent<CinemachineRecomposer>().m_Tilt = Mathf.Lerp(keysValues.Value.camera.GetComponent<CinemachineRecomposer>().m_Tilt, transitionTitleValue, Time.deltaTime * 2);
                }

            }
        }

        if(Actiontimeline != null)
        {
            if (playTimeline)
                return;

            int rev = 1;

            if (isReverseTimeline)
                rev = -1;

            timeTagetValue += Time.deltaTime * (timelineFaderValue*10*rev);
            SendFaderEvent("TimelineFader", timeTagetValue);
            Actiontimeline.time = Mathf.Lerp((float)Actiontimeline.time, timeTagetValue, Time.deltaTime * 5);
            //Debug.Log(Actiontimeline.time);
            Actiontimeline.Evaluate();

            if (timeTagetValue < 0)
                timeTagetValue = 0;
            if (timeTagetValue > Actiontimeline.duration)
                timeTagetValue = (float)Actiontimeline.duration;
        }

        //midiDevicePort.SendNoteOn(0, 30, 127);
         
    }

}

[System.Serializable]
public class DollyData
{
    public CinemachineVirtualCamera camera;
    public bool isEnabled;
    public enum PathType { cinemachine, stageTools, follow, NaN};
    public PathType typeOfPath = new DollyData.PathType();
    public DollyData(CinemachineVirtualCamera _camera, PathType _type, bool _isEnabled = false)
    {
        camera = _camera;
        isEnabled = _isEnabled;
        typeOfPath = _type;
    }
}