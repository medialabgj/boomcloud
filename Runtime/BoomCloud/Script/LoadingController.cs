﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingController : MonoBehaviour
{
    private static LoadingController instance;

    public static event System.Action sceneIsLoading;

    public static LoadingController GetInstance()
    {
        return instance;
    }


    private void OnEnable()
    {
        StageSynchronizer.PushEventStringReceived += ReceivedEvent;
    }

    private void OnDisable()
    {
        StageSynchronizer.PushEventStringReceived -= ReceivedEvent;
    }

    //public bool AutoLoadScene = false;
    //public int SceneNumber = 0;

    public bool loadAdditiveScene = false;


    public string[] sceneToLoad;    
    private int previoustSceneIndex = -1;


    private void ReceivedEvent(string identifier, string message)
    {
        if (identifier == "LoadingController" && message == "RELOAD" && BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.sender)
        {
            Reload(true);
            Debug.Log("RELOAD EVENT RECEIVED!");
        }
    }

    private void SendEvent(string message)
    {
        if (!BoomCloudSettings.GetInstance().isStageSyncLinked)
            return;

        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.receiver)
        {
            BoomCloudSettings.GetInstance().stageSynchronizer.PushEvent("LoadingController", message);
            Debug.Log("RELOAD EVENT SENT");
        }
    }

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        /*
        string[] sceneToLoad = {    "InputController",
                                    "QualisysMocap",
                                    "RokokoMocap",
                                    "Ui",
                                    "CameraController",
                                   // "QuestServer",
                                };

        foreach(string scene in sceneToLoad)
        {
            if (!SceneManager.GetSceneByName(scene).isLoaded)
            {
                SceneManager.LoadScene(scene, LoadSceneMode.Additive);
            }
        }
        */


        LoadStatupScene();

        /*
        if (AutoLoadScene)
            StartCoroutine("LoadScene", SceneNumber);
            */
        //LoadScene(0);
    }

    private void LoadStatupScene()
    {
        foreach (string scene in sceneToLoad)
        {
            if (!loadAdditiveScene)
            {
                LogicController.GetInstace().incomingSceneName.Add(scene);
            }
            else
            {
                if (!SceneManager.GetSceneByName(scene).isLoaded)
                    SceneManager.LoadScene(scene, LoadSceneMode.Additive);
            }

            // LogicController.GetInstace().dataScene.SpaceLoaderList.ForEach(l => Debug.Log(l.ToString()));
        }

        Invoke("turnLigntOn", 3f);
    }

    public void Reload(bool fromEvent = false)
    {
        sceneIsLoading?.Invoke();


        if (!fromEvent)
            SendEvent("RELOAD");

        UIController.GetInstance().reloadUI();
        DollyCamController.GetInstance().StopUpdate();
        DollyCamController.GetInstance().ResetDict();
        StartCoroutine("ReloadProject");

    }

    private IEnumerator ReloadProject() 
    {
        foreach (string scene in sceneToLoad)
        {
            SceneManager.UnloadSceneAsync(scene);
            yield return null;
            // LogicController.GetInstace().dataScene.SpaceLoaderList.ForEach(l => Debug.Log(l.ToString()));
        }

        LoadStatupScene();
        DollyCamController.GetInstance().ResumeUpdate();
        Invoke("turnLigntOn", 1f);
    }


    public void turnLigntOn()
    {
        LightProbes.Tetrahedralize();

    }

    public IEnumerator LoadScene(int index)
    {
        sceneIsLoading?.Invoke();

        if (index < sceneToLoad.Length && index > -1)
        {
            StartCoroutine(LoadNextScene(index));
            yield return new WaitForSeconds(0.2f);
            StartCoroutine(UnloadPreviousScene(previoustSceneIndex));
           // QualisysAutoConnect.GetInstance().reco();
            // ShadowSceneController.GetInstance().MoveCameraToScene(RealtimeScene[index].name);
            previoustSceneIndex = index;
        }
        else
        {
            Log.LogMe("Loading Error: No more scene to load. Current scene index: " + index);
        }

        yield return null;
    }



    public void ResetAll()
    {
        // SceneManager.UnloadSceneAsync("CameraController");
        // SceneManager.LoadScene("CameraController");
        foreach (string scene in sceneToLoad)
        {
            if (SceneManager.GetSceneByName(scene).isLoaded)
            {
                SceneManager.UnloadSceneAsync(scene);
            }
        }
       // BoomCloudSettings.GetInstance().resetCamera();
    }

    private IEnumerator LoadNextScene(int index)
    {
        /*
        if (index == 0 && SceneManager.GetSceneAt(0).isLoaded)
        {
             SmallReload();
            //Debug.LogWarning("Don't Reload BoomCloud MASTER SCENE! This Can cause issue");
            yield break;
        }
        */


        SceneManager.LoadSceneAsync(sceneToLoad[index], LoadSceneMode.Additive);
        //SceneManager.SetActiveScene(SceneManager.GetSceneByName(realtimeScene[index].name));
        Log.Tech("Loading " + sceneToLoad[index]);
        yield return null;
    }

    private void SmallReload()
    {
        LogicController.GetInstace().lateStartTime = 1;

        FindObjectOfType<SceneController>().Start();
    }

    private IEnumerator UnloadPreviousScene(int index)
    {
        
        if (index == -1)
            yield break;

        if (SceneManager.GetSceneByName(sceneToLoad[index]).IsValid())
        {
            //BoomCloudSettings.GetInstance().ManageCamerasTransition();
            SceneManager.UnloadSceneAsync(sceneToLoad[index]);

            Log.Tech("Unloading " + sceneToLoad[index]);
        }
        else
        {
            Log.Tech("Scene to unload: " + sceneToLoad[index] + " don't exist");
        }
        yield return null;
    }
}
