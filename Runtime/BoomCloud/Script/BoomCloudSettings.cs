using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class BoomCloudSettings : MonoBehaviour
{
    private static BoomCloudSettings instance;


    public StageSynchronizer stageSynchronizer;
    [HideInInspector]
    public bool isStageSyncLinked = false;

    [Serializable]
    public struct CameraRow
    {
        public int midiRow;
        //public int uiRow;

        public LayerMask layer;
        public LogicController.LedColorPicker cameraColor;
    }

    public List<CameraRow> cameraSettings;

    public int actionCueRow;
    public LogicController.LedColorPicker actionColor;

    public int audioCueRow;
    public LogicController.LedColorPicker audioColor;


    public bool ledDisplayCameraGroupe = false;
    public int cameraGroupeRow;

    public enum BoomCloudSyncMode { mirror, sender, receiver };
    public BoomCloudSyncMode boomcloudSyncMode;

    public static BoomCloudSettings GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;

        if (stageSynchronizer != null)
        {
            isStageSyncLinked = true;
        }
        else
        {
            Debug.LogWarning("BoomCloud: Stage Synchronoizer not enable, check settings in boomcloud Master to link stageSync if you wanna sync boomcloud worldwide");
        }

    }

    private void Start()
    {
        if (cameraSettings.Count > 4)
            Debug.LogError("BOOMCLOUD settings don't support more than 4 cameras at the moment");

        //activate the camera according to number of item in the camera settings
        for(int i = 0; i < cameraSettings.Count; i++)
        {
            int layer = cameraSettings[i].layer.value;

            CameraEnabler.GetInstance().cameras[i].gameObject.SetActive(true);
            CameraEnabler.GetInstance().cameras[i].gameObject.layer = LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(layer, 2)));
            CameraEnabler.GetInstance().cameras[i].cullingMask = CameraEnabler.GetInstance().cameras[i].cullingMask |= 1 << LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(layer, 2)));

            SpoutCameraEnabler.GetInstance().cameras[i].gameObject.SetActive(true);
        }
    }
}
