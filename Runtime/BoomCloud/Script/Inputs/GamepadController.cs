﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamepadController : MonoBehaviour
{
    public static int globalId = -1;

    public int id;


    private void Awake()
    {
        globalId++;
        id = globalId;
    }
    
}
