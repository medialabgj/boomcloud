﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Globalization;
using Cinemachine;

public class UIController : MonoBehaviour
{
    private static UIController instance;

    public static event System.Action UiPageChanged;

    public bool displayCameraGroup;
    public bool displayCameraCues;
    public bool displayAudioCues;
    public bool displayActionCues;
    public bool displayCuesNumber;

    public Text currentPageNumber;

    public Text logText;
    public Color cameraSelectionColor;

    public int numberofCueMax = 50;
    private List<cueNumber> maxCue = new List<cueNumber>();

    public bool EnableWorkingUI = true;
    public GameObject WorkingCameraUI;


    public Text audioTimer;

    public Text localIpAdresse;
    public Text remoteIpAdresse;
    public Text localTime;
    public Text date;
    public Text city;
    public Text country;
    public Text origin;

    public Text cameraSyncFeedBack;
    public Dropdown m_CameraSync;
    public BoomCloudConfigurator BoomCloudConfigurator;

    public Canvas canevas;
    float timer = 0;
    //Settings
    public static int elementsPerPage = 18;
    public int incrementPerPage = 3;
    public int currentUiPage = 1;
    private Vector2Int cuesDisplayedOnCurrentPage = new Vector2Int(0, elementsPerPage);
    private AudioSource currentAudio = new AudioSource();


    public Color[] uiColor = { Color.red, Color.blue, Color.yellow, Color.white, Color.green, Color.magenta, new Color(32f, 227f, 220f), new Color(110f, 12f, 22f), new Color(100f, 12f, 110f) };
    private Dictionary<int, GameObject> rowCameraDict = new Dictionary<int, GameObject>();
    private Dictionary<int, List<GameObject>> InstanciedUICameraDict = new Dictionary<int, List<GameObject>>();

    private Dictionary<DataScene.CueType, List<GameObject>> InstanciedUiObjectsdict = new Dictionary<DataScene.CueType, List<GameObject>>();
    private Dictionary<DataScene.CueType, GameObject> RowParentIdentifier = new Dictionary<DataScene.CueType, GameObject>();
    private Dictionary<int, GameObject> CameraRowLayerIdentifier = new Dictionary<int, GameObject>();

    public List<GameObject> camerasInfos;
    public bool disabledCameraPreview = false;
    public Dictionary<int, GameObject> cameraOutlineSelectDict = new Dictionary<int, GameObject>();

    [SerializeField]
    private GameObject leftMenu = null;

    [SerializeField]
    private GameObject RowCam = null;

    [SerializeField]
    private GameObject cueUITemplate = null;

    [SerializeField]
    private GameObject camUICueTemplate = null;


    public static UIController GetInstance()
    {
        return instance;
    }

    private void OnEnable()
    {
        SceneController.InitScene += UIinit;
        Application.logMessageReceived += LogCallback;
        CueController.cueIdentifier += UpdateActiveCue;
        CameraController.CamSelection += updateCameraSelection;
        CameraController.DollyCamEvent += CameraOutlineDollyUpdateManager;
    }

    private void OnDisable()
    {
        SceneController.InitScene -= UIinit;
        Application.logMessageReceived -= LogCallback;
        CueController.cueIdentifier -= UpdateActiveCue;
        CameraController.CamSelection -= updateCameraSelection;
        CameraController.DollyCamEvent -= CameraOutlineDollyUpdateManager;
    }

    private void Awake()
    {
        instance = this;
        LoadConfiguration();
    }

    private void Start()
    {
        camerasInfos.ForEach(c => c.gameObject.SetActive(false));

        for (int i = 0; i < BoomCloudSettings.GetInstance().cameraSettings.Count; i++)
        {
            if (!disabledCameraPreview)
                camerasInfos[i].SetActive(true);

            cameraOutlineSelectDict.Add(Mathf.RoundToInt(Mathf.Log(BoomCloudSettings.GetInstance().cameraSettings[i].layer.value, 2)), camerasInfos[i]);
        }


        //cameraOutlineSelectDict = cameraOutline.ToDictionary(c => Mathf.RoundToInt(Mathf.Log(c.layer.value, 2)), c => c.cameraOutline);

        cameraOutlineSelectDict.ToList().ForEach(c => {
            c.Value.GetComponent<Outline>().enabled = false;
            c.Value.GetComponentInChildren<Text>().enabled = false;
        });

        if (!EnableWorkingUI)
            WorkingCameraUI.GetComponent<Canvas>().enabled = false;

        if (displayCameraGroup)
            displayCameraCues = true;


        localIpAdresse.text = GetLocalIPv4();
        StartCoroutine(InternetQuery());
        origin.text = SystemInfo.deviceName;
        date.text = localTime.text = System.DateTime.Now.ToString("yyyy/MM/dd");

        ApplyConfiguration();
    }

    private IEnumerator InternetQuery()
    {
        GeoData geoData = new GeoData(this, GeoData.GetGeoLocation());
        yield return geoData.coroutine;
        GeoJson geoJson = JsonUtility.FromJson<GeoJson>(geoData.result.ToString());
        //  Debug.Log(geoData.result.ToString());

        remoteIpAdresse.text = geoJson.query;
        city.text = geoJson.city;
        country.text = geoJson.country;

    }
    public void LoadConfiguration()
    {
        bool loadConfig = BoomCloudConfigurator.LoadConfiguration();

        if (loadConfig)
        {
            cameraSyncFeedBack.color = Color.green;
        }
        else
        {
            cameraSyncFeedBack.color = Color.red;
        }
    }


    public void ApplyConfiguration()
    {
        BoomCloudSettings.GetInstance().boomcloudSyncMode = BoomCloudConfigurator.config.SyncMode;
        UpdateUICompenent();
    }

    public void UpdateUICompenent()
    {
        //Debug.Log("CONFIG INT VALUE " + BoomCloudConfigurator.config.SyncMode.ToString());
        m_CameraSync.value = (int)BoomCloudConfigurator.config.SyncMode;
    }

    public string GetLocalIPv4()
    {
        return Dns.GetHostEntry(Dns.GetHostName())
            .AddressList.First(
                f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            .ToString();
    }

    private void UIinit(DataScene dataScene)
    {
        UiInit();
    }

    private void Update()
    {
        if (currentAudio != null)
        {
            float audioTime = currentAudio.time;
            int sec = int.Parse(audioTime.ToString().Split('.')[0]);

            TimeSpan timeSpan = TimeSpan.FromSeconds(sec);
            string timeText = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);

            audioTimer.text = timeText;
        }
        else
        {
            timer += Time.deltaTime;

            int seconds = (int)timer % 60;
            int minure = (int)timer / 60;

            audioTimer.text = string.Format("{0:D2}:{1:D2}", minure, seconds);


            localTime.text = System.DateTime.Now.ToString("HH:mm:ss");
        }
    }

    public void resetTimer()
    {
        timer = 0;
    }

    public void goToNextPage()
    {
        currentUiPage += 1;
        // cuesDisplayedOnCurrentPage = new Vector2Int((currentUiPage - 1) * elementsPerPage, (currentUiPage - 1) * elementsPerPage + elementsPerPage);
        cuesDisplayedOnCurrentPage = DisplayedCuesOnCurrentPage();
        UpdateUI();
    }

    public void goToPreviousPage()
    {
        currentUiPage -= 1;
        if (currentUiPage < 1)
            currentUiPage = 1;
        // cuesDisplayedOnCurrentPage = new Vector2Int((currentUiPage - 1) * elementsPerPage, (currentUiPage - 1) * elementsPerPage + elementsPerPage);

        cuesDisplayedOnCurrentPage = DisplayedCuesOnCurrentPage();
        UpdateUI();
    }

    private Vector2Int DisplayedCuesOnCurrentPage()
    {
        int page = currentUiPage;
        return new Vector2Int((page - 1) * incrementPerPage, elementsPerPage + (page - 1) * incrementPerPage);
    }

    public void toggleWorkingUI()
    {
        if (WorkingCameraUI == null)
            return;
        if (EnableWorkingUI)
        {
            EnableWorkingUI = false;
            WorkingCameraUI.GetComponent<Canvas>().enabled = false;
            canevas.gameObject.SetActive(false);
            // ReelCameraUI.gameObject.SetActive(false);
            //workingCameraDot(0);
            //WorkingCameraUI.GetComponent<Canvas>().worldCamera = null;

        }
        else
        {
            WorkingCameraUI.GetComponent<Canvas>().enabled = true;
            EnableWorkingUI = true;
            canevas.gameObject.SetActive(true);
            //  ReelCameraUI.gameObject.SetActive(true);

        }
    }

    private void UiInit()
    {
        if (displayCuesNumber)
            CuesManager(DataScene.CueType.cueNumber, true);

        if (displayAudioCues)
            CuesManager(DataScene.CueType.audio, true);

        if (displayActionCues)
            CuesManager(DataScene.CueType.action, true);

        if (displayCameraGroup)
            CuesManager(DataScene.CueType.cameraGroupe, true);

        currentAudio = LogicController.dataScene.mainAudio;

        if (displayCameraCues)
        {
            CreateCamerasRow();
            UpdateCamerasCuesInRow();
        }

        UiPageChanged?.Invoke();

    }

    public void testingButton()
    {
        InstanciedUiObjectsdict[DataScene.CueType.audio].ToList().ForEach(x => Destroy(x));
        InstanciedUiObjectsdict[DataScene.CueType.audio].Clear();
        CuesManager(DataScene.CueType.audio);

        /*LogicController.dataScene.CuesPerAudio.ToList().ForEach(audiocue => {
            CuesManager(DataScene.CueType.action);
        });*/
    }
    private void UpdateUI()
    {
        currentPageNumber.GetComponent<Text>().text = currentUiPage.ToString();
        ResetUi();
        if (displayCuesNumber)
            CuesManager(DataScene.CueType.cueNumber);

        if (displayAudioCues)
            CuesManager(DataScene.CueType.audio);

        if (displayActionCues)
            CuesManager(DataScene.CueType.action);

        if (displayCameraGroup)
            CuesManager(DataScene.CueType.cameraGroupe);

        if (displayCameraCues)
            UpdateCamerasCuesInRow();
        //refreshCues();

        UiPageChanged?.Invoke();
    }

    public void reloadUI()
    {

        InstanciedUiObjectsdict.ToList().ForEach(x => {
            x.Value.ForEach(g => Destroy(g));
            x.Value.Clear();
        });

        InstanciedUICameraDict.ToList().ForEach(x => {
            x.Value.ForEach(g => Destroy(g));
            x.Value.Clear();
        });


        RowParentIdentifier.ToList().ForEach(x => Destroy(x.Value));
        RowParentIdentifier.Clear();

        rowCameraDict.ToList().ForEach(x => Destroy(x.Value));
        rowCameraDict.Clear();

        CameraRowLayerIdentifier.ToList().ForEach(x => Destroy(x.Value));
        CameraRowLayerIdentifier.Clear();


        timer = 0;
        // UiInit();
    }


    private void ResetUi()
    {
        InstanciedUiObjectsdict.ToList().ForEach(x => {
            x.Value.ForEach(g => Destroy(g));
            x.Value.Clear();
        });

        InstanciedUICameraDict.ToList().ForEach(x => {
            x.Value.ForEach(g => Destroy(g));
            x.Value.Clear();
        });


    }

    private void CuesManager(DataScene.CueType cueType, bool init = false)
    {
        string CategorieName = "NaN";
        dynamic listOfCues = null;
        switch (cueType)
        {
            case DataScene.CueType.cueNumber:
                CategorieName = "Cue N°";
                if (init == true)
                {
                    for (int i = 0; i < numberofCueMax; i++)
                    {
                        //  maxCue.ToList().Add(i);
                        maxCue.Add(new cueNumber());
                        maxCue[i].cueName = (i + 1).ToString();
                    }
                }
                listOfCues = maxCue.ToArray();
                break;
            case DataScene.CueType.audio:
                CategorieName = "Audio Cue";
                listOfCues = LogicController.dataScene.audioCues;
                break;
            case DataScene.CueType.action:
                CategorieName = "Action";
                listOfCues = LogicController.dataScene.actionsCues;
                break;
            case DataScene.CueType.cameraGroupe:
                CategorieName = "Cameras Cue";
                listOfCues = LogicController.dataScene.CamerasCuesList;
                break;
            case DataScene.CueType.camera:
                listOfCues = LogicController.dataScene.camerasState;
                return;
            default:
                CategorieName = "NaN";
                Debug.LogWarning("Error In Categorie of Cue");
                return;
        }

        if (init == true)
        {
            //Create Row For cues
            GameObject RowUi = (GameObject)Instantiate(RowCam);
            RowUi.transform.SetParent(leftMenu.transform, false);

            //Name of row for Cues
            GameObject name = (GameObject)Instantiate(cueUITemplate);
            name.transform.SetParent(RowUi.transform, false);
            name.GetComponentInChildren<CameraCueUI>().cueName.transform.GetComponent<Text>().text = CategorieName;

            name.GetComponentInChildren<Image>().color = Color.black;
            //name.transform.localScale = new Vector3(name.transform.localScale.x * canevas.scaleFactor, name.transform.localScale.y, name.transform.localScale.z);

            InstanciedUiObjectsdict[cueType] = new List<GameObject>();
            RowParentIdentifier[cueType] = RowUi;
        }

        UpdateAllCues(cueType, listOfCues);
    }

    //Update all Cue EXEPCT CAMERAS! Check UpdateCamerasCuesInRow for the update of the cameras UI
    private void UpdateAllCues(DataScene.CueType cueType, dynamic listOfCues)
    {
        int i = cuesDisplayedOnCurrentPage[0] + 1;
        // Iterate to create instanciate all the cues
        foreach (dynamic cueList in listOfCues)
        {
            if (cueType == DataScene.CueType.camera)
                continue;

            if (i < cuesDisplayedOnCurrentPage[0] || i > cuesDisplayedOnCurrentPage[1] || i > listOfCues.Length)
                return;

            GameObject ui = (GameObject)Instantiate(cueUITemplate);
            ui.transform.SetParent(RowParentIdentifier[cueType].transform, false);
            ui.GetComponentInChildren<CameraCueUI>().cueName.transform.GetComponent<Text>().text = listOfCues[i - 1].cueName;

            switch (cueType)
            {
                case DataScene.CueType.action:
                    ui.GetComponentInChildren<Image>().color = uiColor[(int)LogicController.dataScene.actionsCues[i-1].ledColor];
                    break;

                case DataScene.CueType.audio:
                    ui.GetComponentInChildren<Image>().color = Color.grey;
                    break;
            }

            int cueIndex = i;
            if (cueType == DataScene.CueType.cameraGroupe)
                cueIndex = i - 1;

            ui.GetComponent<CueIdentifier>().cueType = cueType;
            ui.GetComponent<CueIdentifier>().cueIndex = cueIndex;

            InstanciedUiObjectsdict[cueType].Add(ui);

            i++;
        }
    }

    private void refreshCues()
    {
        dynamic listOfCues = null;
        DataScene.CueType cueType = DataScene.CueType.NaN;

        InstanciedUiObjectsdict.ToList().ForEach(cues => {
            cueType = cues.Key;

            switch (cueType)
            {
                case DataScene.CueType.cueNumber:
                    listOfCues = maxCue;
                    break;
                case DataScene.CueType.audio:
                    listOfCues = LogicController.dataScene.audioCues;
                    break;
                case DataScene.CueType.action:
                    listOfCues = LogicController.dataScene.actionsCues;
                    break;
                case DataScene.CueType.cameraGroupe:
                    //listOfCues = LogicController.dataScene.CamerasCuesList;
                    return;
                case DataScene.CueType.camera:
                    listOfCues = LogicController.dataScene.camerasState;
                    return;
                default:
                    Debug.LogWarning("Error In Categorie of Cue");
                    return;
            }

            cues.Value.ForEach(cue => {
                int i = 0;
                Debug.Log(LogicController.dataScene.audiosCuesState.ToList()[i].Value.ToString());
                if (listOfCues is AudioCuesList[] && listOfCues.Length > i && LogicController.dataScene.audiosCuesState.ToList()[i].Value == DataScene.CueStatsType.activeCue)
                {
                    // UpdateActiveCue(listOfCues, i);  && LogicController.dataScene.CuesPerAudio.ToList().Count < i && LogicController.dataScene.CuesPerAudio.ToList()[i].Value == DataScene.CueStatsType.activeCue
                    // Debug.Log(LogicController.dataScene.CuesPerAudio.ToList()[i].Value.ToString());
                    //Debug.Log(i);
                    UpdateActiveCue(cueType, i);
                    i++;
                }
            });
        });
    }
    //Instaciate Row for each camera
    private void InstanciatCameraRowParent(int layer)
    {
        GameObject ui = (GameObject)Instantiate(RowCam);
        ui.transform.SetParent(leftMenu.transform, false);
        // ui.transform.localScale = new Vector3(ui.transform.localScale.x * canevas.scaleFactor, ui.transform.localScale.y, ui.transform.localScale.z);

        rowCameraDict.Add(layer, ui);
    }

    //Instanciate the cues for each camera
    private void CreateCamerasRow()
    {
        foreach (LayerMask layer in cameraOutlineSelectDict.Keys)
        {
            InstanciatCameraRowParent(layer);
        }
        foreach (KeyValuePair<int, GameObject> row in rowCameraDict)
        {
            foreach (KeyValuePair<int, List<CameraState>> camState in LogicController.dataScene.camerasState)
            {
                if (camState.Key == row.Key)
                {
                    InstanciedUICameraDict[row.Key] = new List<GameObject>();
                    GameObject camInfo = (GameObject)Instantiate(camUICueTemplate);
                    camInfo.transform.SetParent(row.Value.transform, false);
                    //camInfo.transform.localScale = new Vector3(camInfo.transform.localScale.x * canevas.scaleFactor, camInfo.transform.localScale.y, camInfo.transform.localScale.z);


                    string camName = null;

                    foreach (Camera cam in Camera.allCameras)
                    {
                        if (cam.gameObject.layer == row.Key)
                        {
                            camName = cam.name;
                        }
                    }

                    camInfo.GetComponentInChildren<Text>().text = camName;
                    camInfo.GetComponentInChildren<Text>().color = Color.white;
                    camInfo.GetComponentInChildren<Text>().fontSize = 18;
                    camInfo.GetComponentInChildren<Image>().color = Color.black;
                    if (camInfo.GetComponent<CueIdentifier>().numberPanel != null)
                        camInfo.GetComponent<CueIdentifier>().numberPanel.SetActive(false);

                    CameraRowLayerIdentifier.Add(row.Key, camInfo);
                }
            }
        }

    }

    public void UpdateCamerasCuesInRow()
    {
        foreach (KeyValuePair<int, GameObject> row in rowCameraDict)
        {
            foreach (KeyValuePair<int, List<CameraState>> camState in LogicController.dataScene.camerasState)
            {
                if (camState.Key == row.Key)
                {
                    int i = cuesDisplayedOnCurrentPage[0];

                    camState.Value.ForEach(c =>
                    {
                        if (i < cuesDisplayedOnCurrentPage[0] || i > cuesDisplayedOnCurrentPage[1] - 1 || i > LogicController.dataScene.camerasState[row.Key].Count - 1)
                            return;

                        GameObject ui = (GameObject)Instantiate(camUICueTemplate);
                        InstanciedUICameraDict[row.Key].Add(ui);
                        ui.transform.SetParent(row.Value.transform, false);
                        //ui.transform.localScale = new Vector3(ui.transform.localScale.x * canevas.scaleFactor, ui.transform.localScale.y, ui.transform.localScale.z);

                        //ui.GetComponentInChildren<Text>().text = LayerMask.LayerToName(row.Key);
                        if (camState.Value[i].cueState == DataScene.CueStatsType.noCue)
                        {
                            ui.GetComponentInChildren<Text>().text = "EMPTY";
                            ui.GetComponentInChildren<Image>().color = Color.gray;
                            ui.GetComponent<CueIdentifier>().cueType = DataScene.CueType.empty;

                        }
                        else
                        {
                            //  Debug.Log(c.cameraLink.name + "  " + i);
                            ui.GetComponentInChildren<Text>().text = LogicController.dataScene.camerasState[row.Key][i].cameraLink.gameObject.name;
                            ui.GetComponentInChildren<Image>().color = uiColor[(int)LogicController.dataScene.cameraColor[row.Key]];
                            ui.GetComponent<CueIdentifier>().cueType = DataScene.CueType.camera;
                        }

                        ui.GetComponent<CueIdentifier>().cameraLayer = row.Key;
                        ui.GetComponent<CueIdentifier>().cueIndex = i;
                        int currentActiveCue = camState.Value.IndexOf(camState.Value.First(c => c.cueState == DataScene.CueStatsType.activeCue));
                        //Debug.Log(c.cueState + " i " + i + " currentActiveCue  " + currentActiveCue + " cues Displayed " + cuesDisplayed);
                        //Debug.Log("Cue " + camState.Value.IndexOf(camState.Value.First(c => c.cueState == DataScene.CueStatsType.activeCue)) + " Delta " + delta);
                        if (currentActiveCue == i)
                        {
                            ui.GetComponentInChildren<Image>().color = Color.green;
                        }

                        i++;
                    });
                }
            }
        }
    }

    public void Outline(Vector2Int inOut)
    {
        InstanciedUICameraDict.ToList().ForEach(ui => {
            int i = 0 + cuesDisplayedOnCurrentPage[0];
            ui.Value.ForEach(g => {
                g.GetComponentInChildren<Outline>().enabled = false;
                if (i >= inOut[0] && i < inOut[1])
                {
                    g.GetComponentInChildren<Outline>().enabled = true;
                }
                i++;
            });
        });
    }


    public void toggleUiDisplay()
    {
        if (leftMenu.gameObject.activeSelf == true)
        {
            leftMenu.gameObject.SetActive(false);
        }
        else
        {
            leftMenu.gameObject.SetActive(true);
        }
    }


    public void UpdateActiveCue(DataScene.CueType cueType, int index)
    {

        if (index < cuesDisplayedOnCurrentPage[0] || index > cuesDisplayedOnCurrentPage[1])
            return;

        dynamic colorRef = null;

        switch (cueType)
        {
            case DataScene.CueType.audio:
                colorRef = (int)LogicController.dataScene.audioLedColor;
                break;
            case DataScene.CueType.action:
                //colorRef = (int)LogicController.dataScene.actionsCues[index].ledColor;
                colorRef = (int)LogicController.dataScene.actionsCues[index].ledColor;

                break;
            case DataScene.CueType.camera:
                break;
        }

        //InstanciedUiObjectsdict[cueType].ToList().ForEach(c => c.GetComponentInChildren<Image>().color = Color.grey);
        int i = 0;
        foreach(GameObject c in InstanciedUiObjectsdict[cueType])
        {
            c.GetComponentInChildren<Image>().color = uiColor[(int)LogicController.dataScene.actionsCues[i].ledColor];
            i++;
        }
        InstanciedUiObjectsdict[cueType][index - cuesDisplayedOnCurrentPage[0]].GetComponentInChildren<Image>().color = Color.green;
    }


    public void updateCameraSelection(string camName, int layer)
    {
        if (!CameraRowLayerIdentifier.ContainsKey(layer))
            return;

        CameraRowLayerIdentifier.Values.ToList().ForEach(c => c.GetComponentInChildren<Text>().color = Color.white);
        CameraRowLayerIdentifier[layer].GetComponentInChildren<Text>().color = cameraSelectionColor;

        UpdateCameraOutlineSelection(camName, layer);
        workingCameraDot(layer);
    }

    private void workingCameraDot(int layer)
    {
        if (WorkingCameraUI == null || !EnableWorkingUI)
        {
            return;
        }

        WorkingCameraUI.gameObject.layer = layer;
        Camera selectedCam = null;
        Camera[] allCameras = FindObjectsOfType<Camera>(); //will return an array of all GameObjects in the scene
        foreach (Camera camera in allCameras)
        {
            if (camera.gameObject.layer == layer)
            {
                selectedCam = camera;
            }
        }

        WorkingCameraUI.GetComponent<Canvas>().worldCamera = selectedCam;
        WorkingCameraUI.GetComponent<Canvas>().planeDistance = 1;
        WorkingCameraUI.GetComponent<Canvas>().sortingOrder = -100000;

    }
    /*
            if (index > cuesDisplayedOnCurrentPage[1]-1)
        {
            goToNextPage();
            return;
        }

        if (index<cuesDisplayedOnCurrentPage[0])
        {
            goToPreviousPage();
            return;
        }

        if (index<cuesDisplayedOnCurrentPage[0] || index> cuesDisplayedOnCurrentPage[1]-1 )
            return;

        if (InstanciedUICameraDict[layer][index - cuesDisplayedOnCurrentPage[0]].GetComponentInChildren<Image>().color == Color.grey)
            return;


        int i = 0;

InstanciedUICameraDict[layer].ForEach(c =>
        {


    if (c.GetComponentInChildren<Image>().color == Color.green && InstanciedUICameraDict[layer][index - cuesDisplayedOnCurrentPage[0]].GetComponentInChildren<Image>().color != Color.grey)
    {
        c.GetComponentInChildren<Image>().color = uiColor[(int)LogicController.dataScene.cameraLedColor[layer]];
    }

    i++;
});

    */

    public void UpdateUiCamera(int index, int layer)
    {

        if (index > cuesDisplayedOnCurrentPage[1] - 1)
        {
            goToNextPage();
            return;
        }

        if (index < cuesDisplayedOnCurrentPage[0])
        {
            goToPreviousPage();
            return;
        }

        /* This is useless... or is it??
        if (index < cuesDisplayedOnCurrentPage[0] || index > cuesDisplayedOnCurrentPage[1]-1 )
            return;
            */


        // Quick and dirtyyy
        if (InstanciedUICameraDict[layer][index - cuesDisplayedOnCurrentPage[0]].GetComponentInChildren<Image>().color == Color.grey)
            return;

        /* This is more clean but don't work.... so I check if the color is grey simply
        if (LogicController.dataScene.camerasState[layer][index - cuesDisplayedOnCurrentPage[0]].cueState == DataScene.CueStatsType.noCue)
            return;
            */


        //Reset the based color for the cues for revert previous green cues
        int i = 0;
        InstanciedUICameraDict[layer].ForEach(c =>
        {
            if (LogicController.dataScene.camerasState[layer][i + cuesDisplayedOnCurrentPage[0]].cueState != DataScene.CueStatsType.noCue && LogicController.dataScene.camerasState[layer][i + cuesDisplayedOnCurrentPage[0]].cueState != DataScene.CueStatsType.activeCue)
                c.GetComponentInChildren<Image>().color = uiColor[(int)LogicController.dataScene.cameraColor[layer]];

            i++;
        });

        //then colorize the green active one
        InstanciedUICameraDict[layer][index - cuesDisplayedOnCurrentPage[0]].GetComponentInChildren<Image>().color = Color.green;
    }

    public void UpdateCameraOutlineSelection(string cameraName, int cameraLayer)
    {
        ResetOutlineUI();
        cameraOutlineSelectDict[cameraLayer].GetComponent<Outline>().enabled = true;
        // cameraSelectedOutline[CameraIndex].SetActive(true);
    }

    private void CameraOutlineDollyUpdateManager(CinemachineVirtualCamera camera, int layer, DollyData.PathType pathType, bool inOut)
    {
        var color = Color.black;


        if (pathType == DollyData.PathType.cinemachine)
            color = Color.cyan;

        if (pathType == DollyData.PathType.stageTools)
            color = Color.blue;

        if (inOut)
        {
            cameraOutlineSelectDict[layer].GetComponent<Outline>().effectColor = color;
            // WorkingCameraUI.GetComponentInChildren<Image>().color = Color.red;
            cameraOutlineSelectDict[layer].GetComponentInChildren<Text>().enabled = true;

        }
        else
        {
            cameraOutlineSelectDict[layer].GetComponent<Outline>().effectColor = Color.red;
            // WorkingCameraUI.GetComponentInChildren<Image>().color = Color.white;
            cameraOutlineSelectDict[layer].GetComponentInChildren<Text>().enabled = false;

        }
    }


    private void LogCallback(string logString, string stackTrace, LogType type)
    {
        logText.text = logString;
        //Or Append the log to the old one
        //LogText.text += "\r\n" + logString;
    }

    private void ResetOutlineUI()
    {
        cameraOutlineSelectDict.Values.ToList().ForEach(c => c.GetComponent<Outline>().enabled = false);
    }

    public void DisableOutlineUI()
    {
        ResetOutlineUI();
    }

    public void OnCameraSyncValueChanged(int value)
    {
        switch (value)
        {
            case 0:
                BoomCloudSettings.GetInstance().boomcloudSyncMode = BoomCloudSettings.BoomCloudSyncMode.mirror;
                BoomCloudConfigurator.config.SyncMode = BoomCloudSettings.BoomCloudSyncMode.mirror;
                Debug.Log("BoomCloud Sync Set To  Mirror");
                break;
            case 1:
                BoomCloudSettings.GetInstance().boomcloudSyncMode = BoomCloudSettings.BoomCloudSyncMode.sender;
                BoomCloudConfigurator.config.SyncMode = BoomCloudSettings.BoomCloudSyncMode.sender;

                Debug.Log("BoomCloud Sync Set To  Sender");
                break;
            case 2:
                BoomCloudSettings.GetInstance().boomcloudSyncMode = BoomCloudSettings.BoomCloudSyncMode.receiver;
                BoomCloudConfigurator.config.SyncMode = BoomCloudSettings.BoomCloudSyncMode.receiver;
                Debug.Log("BoomCloud Sync Set To  Receiver");
                break;
        }
    }
}

public class cueNumber
{
    public string cueName;
}
