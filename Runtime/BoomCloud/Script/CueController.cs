﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using System.Linq;
using Cinemachine;


public class CueController : MonoBehaviour
{
    private static CueController instance;

    [HideInInspector]
    private int cueNumber = -1;

    private PlayableDirector MasterCamTimeline;
    private PlayableDirector VP_1CamTimeline;

    private GameObject dissolveGameObject;


    //DollyControl dollyCam = new DollyControl();
    private float MasterCamSpeed;
    private float VP_1CamSpeed;

    private DataScene datascene = new DataScene();

    public static event System.Action<DataScene.CueType, int> cueIdentifier;

    //used for having a tracked camera controlled with a fader
    private int cameraScale;
    private GameObject cameraRig;

    [HideInInspector]
    public int pageIncrementation = 0;

    public static CueController GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;
    }


    void OnEnable()
    {
        MidiDeviceController.ControlChange += OnControlChange;
        MidiDeviceController.DollyChange += OnDollySpeedChange;
        MidiDeviceController.TraverseQueue += traversCue;
        StageSynchronizer.PushEventIntReceived += ReceivedEvent;
    }

    void OnDisable()
    {
        MidiDeviceController.ControlChange -= OnControlChange;
        MidiDeviceController.DollyChange -= OnDollySpeedChange;
        MidiDeviceController.TraverseQueue -= traversCue;
        StageSynchronizer.PushEventIntReceived -= ReceivedEvent;
    }
    void OnControlChange(CueAction cueAction)
    {
        if(cueAction.type == CueType.Object)
        {
            goToCue(cueAction.value);
        }

        if(cueAction.type == CueType.Audio)
        {
            goToAudioCue(cueAction.value);
        }
    }

    private void traversCue(int direction)
    {
        if(direction == 1)
        {
            goToNextCue();
        }
        else
        {
            goToPreviousCue();
        }
    }

    private void goToNextCue()
    {
        cueNumber++;

        if (cueNumber < LogicController.dataScene.actionsCues.ToList().Count)
        {
            goToCue(cueNumber);
            updateCueName(LogicController.dataScene.actionsCues.ToList()[cueNumber].cueName);
        }
        else
        {
            cueNumber = LogicController.dataScene.actionsCues.ToList().Count - 1;
            Log.LogMe("No CUE in the scene");
        }
    }

    private void goToPreviousCue()
    {
        if (cueNumber > 0)
        {
            cueNumber--;
            goToCue(cueNumber);
            updateCueName(LogicController.dataScene.actionsCues.ToList()[cueNumber].cueName);
        }
        else
        {
            Log.LogMe("Cannot Go back, reset scene instead");
        }

    }
    private void updateCueName(string cueName)
    {
        //UIController.GetInstance().sceneCueStatus.text = cueName;
       // updateNextCueName();
    }

    private void ReceivedEvent(string identifier, int value)
    {

        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.sender)
        {
            if (identifier == "CueEvent")
                goToCue(value, true);

            if (identifier == "AudioEvent")
                goToAudioCue(value, true);

            Debug.Log("CUE EVENT RECEIVED: " + identifier + " cue index: " + value);
        }

    }

    private void SendEvent(string identifier, int index)
    {
        if (!BoomCloudSettings.GetInstance().isStageSyncLinked)
            return;

        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.receiver)
        {
            BoomCloudSettings.GetInstance().stageSynchronizer.PushEvent(identifier, index);
            //Debug.Log("CUE EVENT SENT");
        }

    }

    public void goToCue(int index, bool fromEvent = false)
    {
        
        if(!fromEvent)
            SendEvent("CueEvent", index);
            
        index = index + pageIncrementation;

        if (index >= LogicController.dataScene.actionsCues.ToList().Count)
        {
            Log.New("Cue doesn't exist" + index + " " + LogicController.dataScene.actionsCues.ToList().Count);
            return;
        }

        
        /**/
        cueIdentifier?.Invoke(DataScene.CueType.action, index);
        //ManagePropsList(index);
        ManageObjecToEnable(index);
        ManageObjecToDisable(index);
       // ManageObjectToDisplpayOnMasterVP(index);
       // ManageObjectToDisplpayOnVP_1(index);
       // ManageObjectToDisplpayOnVP_2(index);
       // ManageTimelineToStart(index);
       // ManageMasterCamDolly(index);
       // manageVP_1CamDolly(index);
        manageAudioToSync(index);
        manageObjectToMoveTolayer(index);
        manageCameraCullingChange(index);
    }

    private void manageCameraCullingChange(int index)
    {
        
        foreach (ChangeCameraCulling CameraCullingCues in LogicController.dataScene.actionsCues.ToList()[index].changeCameraCullings)
        {
            foreach(Camera cam in Camera.allCameras)
            {
                if (cam.gameObject.layer == CameraCullingCues.camera.gameObject.layer)

                    if (CameraCullingCues.layerToAdd != null)
                    {
                        CameraCullingCues.layerToAdd.ToList().ForEach(c => {
                            cam.cullingMask |= 1 << LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(c, 2)));
                           //Debug.Log("Adding Layer" + LayerMask.LayerToName((int)Mathf.Log(c.value, 2)));
                        });
                    }

                if (cam.gameObject.layer == CameraCullingCues.camera.gameObject.layer)
                {
                    if(CameraCullingCues.layerToRemove != null)
                    {
                        CameraCullingCues.layerToRemove.ToList().ForEach(c => {
                            cam.cullingMask = cam.cullingMask & ~(1 << LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(c, 2))));
                            //Debug.Log("Removing Layer" + LayerMask.LayerToName((int)Mathf.Log(c.value, 2)));
                        });
                    }
                }
            }
        }
        
    }

    private void manageAudioToSync(int index)
    {
        /*
        if (index != -1)
            // goToAudioCue(index);
            MidiLedController.GetInstance().externalAudioTrigger(index);
            */

        //syncro cue object avec Cue Camera   //&& datascene().CamerasCuesList[index].camera.ToString() == LayerMask.LayerToName(layer)
        if (LogicController.dataScene.actionsCues.ToList()[index].audioCueNameToSync != null)
        {
            //List<SceneCuesList> audioCue = CueController.GetInstance().LogicController.dataScene.actionsCues.ToList();
            int i = 0;
            //Debug.Log("AAAAAAAAAAA " + index );
            foreach (AudioCuesList cuesName in LogicController.dataScene.audioCues)
            {
                if (cuesName.cueName == LogicController.dataScene.actionsCues.ToList()[index].audioCueNameToSync)
                {
                    Debug.Log("STARTING AUDIO " + cuesName.cueName + " index: " + i);
                    goToAudioCue(i);
                    MidiLedController.GetInstance().externalAudioTrigger(i);
                }
                i++;
            }
        }
    }

    /*
    private void ManagePropsList(int index)
    {


        if (LogicController.dataScene.actionsCues.ToList()[index].objectToAssignToProps != null)
        {
            foreach (GameObject props in propsList)
            {
                foreach (Transform child in props.transform)
                {
                    //we don't disable object that are cinemachineCamera
                    if (child.GetComponent<Cinemachine.CinemachineVirtualCamera>() == null)
                    {
                      //  child.gameObject.SetActive(false);
                    }
                }
            }
        }

        int i = 0;
        foreach (GameObject propsToApply in LogicController.dataScene.actionsCues.ToList()[index].objectToAssignToProps)
        {
            Log.Tech("propsToApply: " + propsToApply + "to" + propsList[0].transform);
            //Vector3 relativePosition = propsToApply.transform.localPosition;

            
            if(propsToApply != null)
            {

                // Instantiate(propsToApply, propsList[i].transform);

                propsToApply.transform.SetParent(propsList[i].transform, false);
                var newPos = new Vector3(0, 0, 0);
                propsToApply.transform.localPosition = new Vector3(newPos.x, propsToApply.transform.localPosition.y, newPos.z );


            }
            

            /*
            propsToApply.transform.parent = propsList[i].transform;
            if(propsToApply.activeSelf != true)
            {
                propsToApply.SetActive(true);
            }
           // propsToApply.transform.localPosition = relativePosition;
           
            i++;
        }
    }

*/
    void MoveToLayer(Transform root, int layer)
    {
        int realLayerint = LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(layer, 2)));
        //make sure we don't move cinemachine camera to other layer
        if (!LogicController.dataScene.GetAllDifferentCamera().Contains(layer))
        {
            root.gameObject.layer = realLayerint;
        }

        //iterate through all childs
        foreach (Transform child in root)
            {
               if (!LogicController.dataScene.GetAllDifferentCamera().Contains(child.gameObject.layer))
                    {
                        child.gameObject.layer = realLayerint;
                    }

                MoveToLayer(child, layer);
            }
    }

    private void ManageObjectToHideOnAllScreen(int index)
    {
        foreach (GameObject objectToMove in LogicController.dataScene.actionsCues.ToList()[index].objectToHideOnAllScreen)
        {
            MoveToLayer(objectToMove.transform, 14);
        }
    }

    private void ManageObjectToDisplpayOnAllScreen(int index)
    {
        foreach (GameObject objectToMove in LogicController.dataScene.actionsCues.ToList()[index].objectToDisplpayOnAllScreen)
        {
            MoveToLayer(objectToMove.transform, 0);
        }
    }

    /*
    private void ManageTimelineToStart(int index)
    {
        foreach (GameObject timelineToStart in LogicController.dataScene.actionsCues.ToList()[index].timelineToStart)
        {
            timelineToStart.GetComponent<PlayableDirector>().Play();
        }
    }
    */
    private void manageObjectToMoveTolayer(int index)
    {
        foreach (MoveToLayer move in LogicController.dataScene.actionsCues.ToList()[index].objectToMoveToLayer)
        {
            foreach(GameObject objectToMove in move.objectsToMove)
            {
                MoveToLayer(objectToMove.transform,move.targetLayer);
            }
        }
    }

    private void ManageObjecToDisable(int index)
    {
       // Debug.Log("Disable");
        foreach (GameObject gameObject in LogicController.dataScene.actionsCues.ToList()[index].objectToHideOnAllScreen)
        {
            gameObject.SetActive(false);
        }
    }

    private void ManageObjecToEnable(int index)
    {
      //  Debug.Log("Enable");

        foreach (GameObject gameObject in LogicController.dataScene.actionsCues.ToList()[index].objectToDisplpayOnAllScreen)
        {
            gameObject.SetActive(true);
        }
    }


    private void OnDollySpeedChange(DollyControl dollyControl)
    {

        if(dollyControl.cam == SpeedFor.MasterVP)
        {
           // Debug.Log(dollyControl.value);

            MasterCamSpeed = dollyControl.value;
           // Debug.Log(MasterCamSpeed);
        }

        if (dollyControl.cam == SpeedFor.CamVP_1)
        {
            VP_1CamSpeed = dollyControl.value;
           // Debug.Log(VP_1CamSpeed);

        }


    }

    private  void Update()
    {
        if (MasterCamTimeline != null)
        {
           // Debug.Log("FIRE");
            MasterCamTimeline.playableGraph.Evaluate(Time.deltaTime * MasterCamSpeed);
        }

        if (VP_1CamTimeline != null)
        {
            VP_1CamTimeline.playableGraph.Evaluate(Time.deltaTime * VP_1CamSpeed);
        }
    }



    public void goToAudioCue(int index, bool fromEvent = false)
    {

        if (!fromEvent)
            SendEvent("AudioEvent", index);

        if (index >= LogicController.dataScene.audioCues.ToList().Count)
        {
            Log.New("Audio CUE doesn't exist" + index + " " + LogicController.dataScene.audioCues.ToList().Count);
            return;
        }

        cueIdentifier?.Invoke(DataScene.CueType.audio, index);

        if (LogicController.dataScene.audioCues.ToList()[index].audioToPlay != null)
         LogicController.dataScene.audioCues.ToList()[index].audioToPlay.ToList().ForEach(a => {
             a.source.time = a.startTime;
             a.source.volume = 0;
             a.source.Play();
             StartCoroutine(fadeMusic(a.source, a.transitionTime, a.volume)); ;
         });


        if (LogicController.dataScene.audioCues.ToList()[index].audioToStop != null)
            LogicController.dataScene.audioCues.ToList()[index].audioToStop.ToList().ForEach(a => {
                //a.Pause();
                StartCoroutine(fadeMusic(a.source, a.transitionTime, 0f));
            }); 
    }

    IEnumerator fadeMusic(AudioSource audioSource, float duration, float targetVolume)
    {
        float currentTime = 0;
        float start = audioSource.volume;

        if (duration == 0)
            duration = 0.01f;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }
        yield return null;
        if(audioSource.volume == 0)
              stopMusic(audioSource);
    }

    private void stopMusic(AudioSource audioSource)
    {
        audioSource.Stop();
    }

    public void makeDissolve(float value)
    {
        //var obj = dissolveGameObject.GetComponent<DissolveCharacterController>();

        // obj.dissolveCharacterAmount = value;
        Debug.Log("Dissolve not active... check with Pierre-Igor");
    }

    public void cameraScaleFactore(float value)
    {
        value = value * 10 * cameraScale;
        if (value < 1)
            value = 1;


        cameraRig.transform.localScale = new Vector3 (value,value,value);
        //Debug.Log(cameraRig.transform.localScale.x);
    }

    public void cameraRotationFactore(float value)
    {
        
        cameraRig.transform.rotation = Quaternion.Euler(0,Mathf.Lerp(0,360,value),0);
        Debug.Log(Mathf.Lerp(0, 360, value));
    }
}

