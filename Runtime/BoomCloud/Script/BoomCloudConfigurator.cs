using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class BoomCloudConfigurator : MonoBehaviour
{

    public  BoomCloudConfiguration config;
    const string fileName = "/Configurations/BoomCloudConfigLocal.json";
    string path => Application.streamingAssetsPath + fileName;



    public void SaveConfiguration()
    {
        File.WriteAllText(path, JsonUtility.ToJson(config));
        UIController.GetInstance().cameraSyncFeedBack.color = Color.green;
    }

    public bool LoadConfiguration()
    {
        if (File.Exists(path))
        {
            config = JsonUtility.FromJson<BoomCloudConfiguration>(File.ReadAllText(path));
            Debug.Log("BommCloud Config loaded  " + config.SyncMode.ToString());
            return true;
        }
        Debug.Log("BoomCloud Failed to Load Config");
        return false;

    }

}

