﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using System;
using UnityEngine.SceneManagement;
using System.Linq;
using UnityEngine.InputSystem;
using StageTools;
using UnityEngine.Playables;
public class CameraController : MonoBehaviour
{
    private static CameraController instance;

    private int CameraSelectionIndex = 0;

    private CinemachineVirtualCamera selectedCamera;
    public bool DisableCamSelection = false;
    private float speedBoost = 5;
    private float tiltSpeedDiviser = 1000;
    private float flyCam = 0;

    public bool fixedHeight = false;
    public float heightValue = 1.6f;

    public bool inverseAxis = false;
    public bool noSmooth = false;
    private bool cameraIsOnpath = false;


    Vector3 movementInput;
    Vector3 rotationInput;
    Vector3 movementDirection;
    Vector3 rotationDirection;
    Vector3 targetPosition;
    Vector3 targetRotation;
    float currentMovementSpeed = 0;
    float currentRotationSpeed = 0;
    float currentFlySpeed = 0;
    public float maxMovementSpeed = 30f;
    public float maxRotationSpeed = 360f;
    public float attenuation = 4f;

    int invAxe = 1;


    [TextArea]
    [Tooltip("Doesn't do anything. Just comments shown in inspector")]
    public string Notes = "Camera need to be on they own unique layer, and have culling mask set to hide other camera layer";

    public static event System.Action<string, int> CamSelection;
    public static event System.Action<CinemachineVirtualCamera, int, DollyData.PathType, bool> DollyCamEvent;

    [HideInInspector]
    public int increment = 0;

    private Dictionary<int, int> cameraState;
    public static CameraController GetInstance()
    {
        return instance;
    }

    public void updateMaxMovementSpeed(int increment)
    {
        if (increment > 0)
        {
            maxMovementSpeed = maxMovementSpeed * increment;
        }
        else
        {
            maxMovementSpeed = maxMovementSpeed / (increment*-1);
        }
        if (maxMovementSpeed < 1)
            maxMovementSpeed = 1f;
    }

    private void Awake()
    {
        instance = this;
        cameraState = new Dictionary<int, int>();
        //virtualCamerasCueList = new virtualCameraCueList[6];
    }

    void OnEnable()
    {
        MidiDeviceController.ControlChange += OnControlChange;
        StageSynchronizer.PushEventIntArrayReceived += ReceivedEvent;

    }

    void OnDisable()
    {
        MidiDeviceController.ControlChange -= OnControlChange;
        StageSynchronizer.PushEventIntArrayReceived -= ReceivedEvent;

    }


    private void LateUpdate()
    {
        if (DisableCamSelection)
            return;

        //Active camera move methode
        if (selectedCamera != null)
        {
            var camera = selectedCamera.gameObject;

            Vector2 rightStick = XboxController.GetInstance().m_RightStick;
            Vector2 leftStick = XboxController.GetInstance().m_LeftStick;
            Vector2 dPad = XboxController.GetInstance().m_DPad;

            float moveUp = XboxController.GetInstance().m_R2;
            float moveDown = XboxController.GetInstance().m_L2;
            flyCam = moveUp - moveDown;

            Vector2 translateAim = new Vector2(dPad.x, dPad.y);

            var xAngle = camera.transform.eulerAngles.z;

            // var  = camera.transform.localEulerAngles.x;
            //Debug.Log(xAngle);

            

            if (xAngle > 175 && xAngle < 185)
            {
                inverseAxis = true;
                invAxe = -1;
               // Debug.Log(camera.transform.rotation.x);
            }
            else
            {
                inverseAxis = false;
                invAxe = 1;

            }

            Vector3 orbitCamera = new Vector3(rightStick.y *-1, rightStick.x * invAxe, 0).normalized;
            Vector3 moveCamera = new Vector3(leftStick.x, 0, leftStick.y).normalized;
            Vector3 flyCamera = Vector3.up * flyCam;

            // selectedCamera.transform.Rotate(orbitCamera, Space.Self);
            // selectedCamera.transform.Translate(moveCamera, Space.Self);

            //var mouvementSpeed = new Vector3(leftStick.x, 0, leftStick.y);
            //var flySpeed = new Vector3(0, flyCam, 0);
            //float z = selectedCamera.transform.eulerAngles.z;
            // selectedCamera.transform.Rotate(0, 0, -z);
            // selectedCamera.m_Lens.Dutch = -z;
            /*********************/

            var controlInput = XboxController.GetInstance().controls;

            movementDirection = moveCamera;
            rotationDirection = orbitCamera;

            currentMovementSpeed = maxMovementSpeed * leftStick.magnitude;
            currentRotationSpeed = maxRotationSpeed * rightStick.magnitude;
            currentFlySpeed = maxMovementSpeed * Math.Abs(flyCam);

            targetPosition += camera.transform.TransformDirection(movementDirection) * currentMovementSpeed * Time.deltaTime;
            targetPosition += camera.transform.TransformDirection(flyCamera) * currentFlySpeed * Time.deltaTime;
            targetRotation += rotationDirection * currentRotationSpeed * Time.deltaTime;

            if (noSmooth)
            {
                camera.transform.position = targetPosition;
                camera.transform.rotation = Quaternion.Euler(targetRotation);
            }
            else
            {
                camera.transform.position = Vector3.Lerp(camera.transform.position, targetPosition, Time.deltaTime * attenuation);
                camera.transform.rotation = Quaternion.Slerp(camera.transform.rotation, Quaternion.Euler(targetRotation), Time.deltaTime * attenuation);
            }

            if (fixedHeight)
            {
                var pos = camera.transform.position;
                pos.y = heightValue;
                camera.transform.position = pos;
            }

            if (cameraIsOnpath)
            {
                selectedCamera.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition += flyCam/10;
            }

            /***********************/

            if (selectedCamera.GetComponent<CinemachineCameraOffset>() != null)
            {
                selectedCamera.GetComponent<CinemachineCameraOffset>().m_Offset.x += translateAim.x / -tiltSpeedDiviser * speedBoost;
                selectedCamera.GetComponent<CinemachineCameraOffset>().m_Offset.y += translateAim.y / -tiltSpeedDiviser * speedBoost;
            }
            
        }
    }

    public void SmoothToggle()
    {
        if (noSmooth)
        {
            noSmooth = false;
        }
        else
        {
            noSmooth = true;
        }
    }
    public void resetCameraTilt()
    {
        if (selectedCamera.GetComponent<CinemachineCameraOffset>() != null)
        {
            selectedCamera.GetComponent<CinemachineCameraOffset>().m_Offset.x = 0f;
            selectedCamera.GetComponent<CinemachineCameraOffset>().m_Offset.y = 0f;
        }
    }

    private void SelectCamera(int layer)
    {
        
        int activeIndex = LogicController.GetActiveCameraIndex(layer);
        selectedCamera = LogicController.dataScene.camerasState[layer][activeIndex].cameraLink;
        CamSelection?.Invoke(selectedCamera.name, layer);
        Log.Tech("Camera Selected: " + selectedCamera.name + " Camera layer: " + LayerMask.LayerToName(layer));

        targetRotation = selectedCamera.transform.rotation.eulerAngles;
        targetPosition = selectedCamera.transform.position;

        //check for cinemachine Path
        if (selectedCamera.GetCinemachineComponent<CinemachineTrackedDolly>() != null)
        {
            cameraIsOnpath = true;
        }
        else
        {
            cameraIsOnpath = false;
        }
    }


    public void CycleThoughCamera(int definedSelection = -1)
    {
        if (definedSelection == -1)
        {
            CameraSelectionIndex += 1;
            SelectCamera(LogicController.dataScene.GetAllDifferentCamera()[CameraSelectionIndex % LogicController.dataScene.GetAllDifferentCamera().Count]);
        }
        else
        {
            SelectCamera(LogicController.dataScene.GetAllDifferentCamera()[definedSelection]);
        }
    }

    void OnControlChange(CueAction cueAction)
    {
        if(cueAction.type == CueType.Cameras)
        {
            goToCameraCue(cueAction.value + increment, cueAction.layer);
        }        
    }

    private bool getNextActiveCue(DataScene.CueStatsType cueStats)
    {
        if(cueStats == DataScene.CueStatsType.cue)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void moveCameraCue(int delta, bool DontSyncEvent = false)
    {
        int[] layers = LogicController.dataScene.camerasState.Keys.ToArray();
        //layers.ToList().ForEach(s => { Debug.Log(s.ToString()); });
        foreach (int layer in layers)
        {
            int activeIndex = LogicController.GetActiveCameraIndex(layer);

            if (delta == 1 && activeIndex + 1 > LogicController.dataScene.camerasState[layer].Count - 1 || delta == -1 && activeIndex - 1 < 0)
                continue;

            if (delta == 1)
            {
                if (LogicController.dataScene.camerasState[layer][activeIndex + 1].cueState == DataScene.CueStatsType.noCue)
                {
                    Debug.Log("BoomCloud next cue is Disable go jump to first next available CUE");
                    while (!getNextActiveCue(LogicController.dataScene.camerasState[layer][activeIndex + 1].cueState))
                    {
                        activeIndex += 1;
                    }
                }

                activeIndex += 1;

            }

            if (delta == -1)
            {
                if (LogicController.dataScene.camerasState[layer][activeIndex - 1].cueState == DataScene.CueStatsType.noCue)
                {
                    Debug.Log("BoomCloud previous cue is Disable go jump to first previous available CUE");
                    while (!getNextActiveCue(LogicController.dataScene.camerasState[layer][activeIndex - 1].cueState))
                    {
                        activeIndex -= 1;
                    }
                }

                activeIndex -= 1;

            }

            if (activeIndex >= 0 && activeIndex < LogicController.dataScene.camerasState[layer].Count)
            {
               // Debug.Log("ok " + activeIndex);
                goToCameraCue(activeIndex, layer, DontSyncEvent);
                UIController.GetInstance().UpdateUiCamera(activeIndex, layer);
                MidiLedController.GetInstance().exteralMidiUpdate(activeIndex, layer);
            }
        }
    }

    public void goToNextCameraCue()
    {
        moveCameraCue(1);        
        Log.LogMe("Going to next Camera Cue");
    }

    public void goToPreviousCameraCue()
    {
        moveCameraCue(-1);
        Log.LogMe("Going to prev Camera Cue");
    }

    
    private void ReceivedEvent(string identifier, int[] indexLayer)
    {
        if(identifier == "CameraEvent" && BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.sender)
        {
            int index = indexLayer[0];
            int layer =  indexLayer[1];
            goToCameraCue(index, layer, true);
            UIController.GetInstance().UpdateUiCamera(index, layer);
            MidiLedController.GetInstance().exteralMidiUpdate(index, layer);
            Debug.Log("CAMERA EVENT RECEIVED: cue" + index + " for camera: " + LayerMask.LayerToName(index) );
        }
    }
    
    private void SendEvent(int[] indexLayer)
    {
        if(BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.receiver)
        {
            if (!BoomCloudSettings.GetInstance().isStageSyncLinked)
                return;

            BoomCloudSettings.GetInstance().stageSynchronizer.PushEvent("CameraEvent", indexLayer);
         // Debug.Log("CAMERA EVENT SENT!!");
        }

    }

    public void UnselectCamera()
    {
        selectedCamera = null;
    }

    public void DisableCameraSelection()
    {
        UnselectCamera();
        DisableCamSelection = true;
    }

    public void RenableCameraSelection()
    {
        DisableCamSelection = false;
    }

    public void goToCameraCue(int index, int layer, bool fromEvent = false)
    {
        if (index >= LogicController.dataScene.camerasState[layer].Count || LogicController.dataScene.camerasState[layer][index].cueState == DataScene.CueStatsType.noCue)
        {
            Log.LogMe("Camera Cue doesn't exist");
            return;
        }

        if (!fromEvent)
        {
            SendEvent(new int[] {index, layer});
           // stage().PushEvent("CameraEvent", index);

        }

        //Get link to actual virtual camera
        CinemachineVirtualCamera camera = LogicController.GetVirtualCamera(layer, index);
        //Set state of previous active Camera to cuue and reset priority
        LogicController.SetPreviousActiveCameraState(layer, index);
        //Set state of active camera to 1000 and activeCue State
        LogicController.SetActiveCameraState(layer, index);

        //update camera selection
        SelectCamera(layer);

        //syncro cue object avec Cue Camera   //&& LogicController.dataScene.CamerasCuesList[index].camera.ToString() == LayerMask.LayerToName(layer)
        if (LogicController.dataScene.CamerasCuesList[index].objectCueNameToSynchronize != null )
        {
            List<ActionCuesList> sceneCue = LogicController.dataScene.actionsCues.ToList();
            int i = 0;
            foreach (ActionCuesList cuesName in sceneCue)
            {
                if (cuesName.cueName == LogicController.dataScene.CamerasCuesList[index].objectCueNameToSynchronize)
                {
                    LogicController.SetActiveActionCueState(index);
                    CueController.GetInstance().goToCue(i);
                }
                i++;
            }
        }

        if(LogicController.dataScene.CamerasCuesList[index].layerToDisplay.FirstOrDefault() != 0)
        {
            foreach (Camera cam in Camera.allCameras)
            {
                if(cam.gameObject.layer == layer)
                {
                    foreach(LayerMask mask in LogicController.dataScene.CamerasCuesList[index].layerToDisplay)
                    {
                        cam.cullingMask |= 1 << LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(mask, 2)));
                        //Debug.Log("Matching camera " + cam.name + " in layer " + LayerMask.LayerToName(layer) + " Displaying layer " + LayerMask.LayerToName((int)Mathf.Log(mask, 2)));
                    }

                }
            }
        }

        if (LogicController.dataScene.CamerasCuesList[index].layerToHide.FirstOrDefault() != 0)
        {
            foreach (Camera cam in Camera.allCameras)
            {
                if (cam.gameObject.layer == layer)
                {
                    foreach(LayerMask mask in LogicController.dataScene.CamerasCuesList[index].layerToHide)
                    {
                        cam.cullingMask = cam.cullingMask &~ (1 << LayerMask.NameToLayer(LayerMask.LayerToName((int)Mathf.Log(mask, 2))));
                        //Debug.Log("Matching camera " + cam.name + " in layer " + LayerMask.LayerToName(layer) + " Hiding layer " + LayerMask.LayerToName((int)Mathf.Log(mask, 2)));
                    }
                }
            }
        }


        //Start waypoint path 
        if (camera.GetComponent<WaypointCameraFinder>() != null)
        {
            StageCameraPath path = camera.GetComponent<WaypointCameraFinder>().stageCameraPath;
            if(path != null)
            {
                selectedCamera = null;  //diable camera selction for avoiding error
                path.LaunchSequence();
            }
            else
            {
                Debug.LogWarning("Stage camera Path on Camera " + camera.name  + " is not set ");
            }

        }

        //Change Skybox auto if virtual cam got skybox
        if (camera.GetComponent<Skybox>() != null)
        {
            Material skybox = camera.GetComponent<Skybox>().material;
            CinemachineVirtualCamera vcam = camera;

            Camera[] allCameras = FindObjectsOfType<Camera>(); //will return an array of all GameObjects in the scene
            foreach (Camera cam in allCameras)
            {
                if (cam.gameObject.layer == vcam.gameObject.layer)
                {
                    cam.gameObject.GetComponent<Skybox>().material = skybox;
                }
            }
        }

        //Change Skybox auto if virtual cam got skybox
        if (camera.GetComponent<BlendSkyAutomator>() != null)
        {
            camera.GetComponent<BlendSkyAutomator>().startTransition();
        }

        //Camera Path Manageman/*
        DollyData.PathType pathType = DollyData.PathType.NaN;

        if (camera.GetCinemachineComponent<CinemachineTrackedDolly>() != null)
        {
            pathType = DollyData.PathType.cinemachine;
        }

        if (camera.GetComponent<PathWayPointManualCameraFinder>() != null)
        {
            pathType = DollyData.PathType.stageTools; 
        }

        if (camera.GetComponent<WaypointFollowOtherPath>() != null)
        {
            pathType = DollyData.PathType.follow;
        }

        switch (pathType)
        {
            case DollyData.PathType.cinemachine:
                DollyCamEvent?.Invoke(camera, layer, DollyData.PathType.cinemachine, true);
                break;
            case DollyData.PathType.stageTools:
                DollyCamEvent?.Invoke(camera, layer, DollyData.PathType.stageTools, true);
                selectedCamera = null;  //diable camera selction for avoiding error
                break;
            case DollyData.PathType.follow:
                DollyCamEvent?.Invoke(camera, layer, DollyData.PathType.follow, true);
                selectedCamera = null;  //diable camera selction for avoiding error
                break;
            case DollyData.PathType.NaN:
                DollyCamEvent?.Invoke(camera, layer, DollyData.PathType.NaN, false);
                break;
        }


    }

    public void resetCamera()
    {        
    }

    /*
     * 
     * Usefull if need to make camera transition between different loading scene
     * 
    public void ManageCamerasTransition()
    {
        if(LogicController.dataScene.previousActiveCamera != null) { 
            foreach (PreviousCameraState Camera in LogicController.dataScene.previousActiveCamera.Values.ToList())
            {
                //for managing transition, move the camera before the scene get deleted so cinemachine can make the transition
                Camera.cameraLink.transform.parent = null;
                DontDestroyOnLoad(Camera.cameraLink.gameObject);
                //destroy camera after 10second... need to be more accurate
                Destroy(Camera.cameraLink.gameObject, 10f);
            }
        }
    }
    */
}
