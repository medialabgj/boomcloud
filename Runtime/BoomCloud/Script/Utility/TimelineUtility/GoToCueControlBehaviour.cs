using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public class GoToCueControlBehaviour : PlayableBehaviour
{
    [SerializeField]
    private enum Direction { NaN, next, previous};

    [SerializeField]

    private Direction direction;

    [SerializeField]

    private int cueNumber;

    [SerializeField]

    private bool useNumber = false;


    private bool frameBeenProcesed = false;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        if (!Application.isPlaying)
        {
            return;
        }

        if (!frameBeenProcesed)
        {


            if (useNumber)
            {
                LogicController.dataScene.GetAllDifferentCamera().ForEach(c => {

                    goToCameraCue(cueNumber, c);
                });
                frameBeenProcesed = true;
                return;
            }


             if (direction == Direction.NaN)
             return;

            if (direction == Direction.next)
                CameraController.GetInstance().moveCameraCue(1, false);


            if (direction == Direction.previous)
                CameraController.GetInstance().moveCameraCue(-1, false);

            frameBeenProcesed = true;
        }
    }

    private void goToCameraCue( int index, int layer)
    {
        bool local = false;
        // Debug.Log("GO TO CUE " + cueType.ToString() + " at index " + cueIndex);
        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.receiver)
            local = true;

        CameraController.GetInstance().goToCameraCue(index, layer, local);
        UIController.GetInstance().UpdateUiCamera(index, layer);
        MidiLedController.GetInstance().exteralMidiUpdate(index, layer);

        // CueController.GetInstance().goToAudioCue(cueIndex - 1);
    }
}