using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System;

[Serializable]
public class WayPathControlClip : PlayableAsset, ITimelineClipAsset
{
    [SerializeField]
    private WayPathControlBehavior template = new WayPathControlBehavior();

    public ClipCaps clipCaps
        {get {return ClipCaps.None;}}
    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
          return ScriptPlayable<WayPathControlBehavior>.Create(graph, template);
    }
    
}
