using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
public class FixTimelinePositionJumpCue : MonoBehaviour
{
    public PlayableDirector timeline;
    public void SetTimelineToEnd()
    {
        timeline.time = timeline.duration;
    }

}
