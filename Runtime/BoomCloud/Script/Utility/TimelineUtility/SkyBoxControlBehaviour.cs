using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public class SkyBoxControlBehaviour : PlayableBehaviour
{
    public Material skybox = null;
    public float blendValue = 0f;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        if (skybox != null)
        {
            skybox.SetFloat("_Blend", blendValue);
        }
    }
}