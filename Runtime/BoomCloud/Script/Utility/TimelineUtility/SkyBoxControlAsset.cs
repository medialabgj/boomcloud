using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SkyBoxControlAsset : PlayableAsset
{
    public SkyBoxControlBehaviour template;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<SkyBoxControlBehaviour>.Create(graph, template);

        return playable;
    }
}
