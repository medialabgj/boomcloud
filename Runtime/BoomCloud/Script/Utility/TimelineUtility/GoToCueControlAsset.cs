using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class GoToCueControlAsset : PlayableAsset
{
    public GoToCueControlBehaviour template;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<GoToCueControlBehaviour>.Create(graph, template);

        return playable;
    }
}
