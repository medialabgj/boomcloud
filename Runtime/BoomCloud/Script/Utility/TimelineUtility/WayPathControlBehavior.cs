using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using StageTools;

[System.Serializable]
public class WayPathControlBehavior : PlayableBehaviour
{
    [SerializeField]
    private float pathValue = 0f;

    [SerializeField]
    private bool runOnEdit = false;

    private bool firstFrameHappened = false;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        var path = playerData as StageCameraPath;

        if (path == null)
            return;

        if (!firstFrameHappened)
        {
            pathValue = 0;
            firstFrameHappened = true;
        }
#if UNITY_EDITOR
        path.runInEditMode = runOnEdit;
#endif
        path.timeCursor = pathValue;
        path.UpdateTargetFromTime(pathValue);
    }
}
