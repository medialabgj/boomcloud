using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TimelineIDInteruptor : MonoBehaviour
{
    public static event System.Action<string> TimelineReadyEvent;

    public string timelineName;
    private void OnDisable()
    {
        TimelineReadyEvent?.Invoke(timelineName);

        Invoke("ReNable", 0.1f);
    }

    private void ReNable()
    {
        this.gameObject.SetActive(true);
    }

}
