﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxInteruptor : MonoBehaviour
{
    public Material skybox;
    private bool isApplicationQuitting = false;
    bool sceneIsUnloading = false;

    private void Awake()
    {
        Application.quitting += preventError;
        LoadingController.sceneIsLoading += preventError;
    }

    private void preventError()
    {
        sceneIsUnloading = true;
    }
    private void OnDisable()
    {
        if (isApplicationQuitting || sceneIsUnloading)
            return;

        RenderSettings.skybox = skybox;

        Camera[] allCameras = FindObjectsOfType<Camera>(); //will return an array of all GameObjects in the scene
        foreach (Camera cam in allCameras)
        {
           if(LogicController.dataScene.GetAllDifferentCamera().Contains(cam.gameObject.layer))
            {
                cam.gameObject.GetComponent<Skybox>().material = skybox;
            }
        }

        Invoke("ReNable", 0.1f);
    }

    private void ReNable()
    {
        this.gameObject.SetActive(true);
    }


    void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }
}
