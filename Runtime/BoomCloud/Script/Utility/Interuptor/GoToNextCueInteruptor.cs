using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToNextCueInteruptor : MonoBehaviour
{
    [SerializeField]
    private enum Direction { NaN, next, previous };

    [SerializeField]

    private Direction direction;

    private void OnDisable()
    {
        GoNextOrPreviousCue();
    }

    public void GoNextOrPreviousCue()
    {

     if (direction == Direction.NaN)
                return;

            if (direction == Direction.next)
                CameraController.GetInstance().moveCameraCue(1, true);


            if (direction == Direction.previous)
                CameraController.GetInstance().moveCameraCue(-1, true);


        Invoke("ReNable", 0.1f);
    }

    private void ReNable()
    {
        if(!gameObject.activeSelf)
        this.gameObject.SetActive(true);
    }
}




