using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class ChangeCinemachineRotationValue : MonoBehaviour
{
    public float speedValue = 0.05f;

    public void ChangeSpeedValue()
    {
        GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineOrbitalTransposer>().m_XAxis.m_InputAxisValue = speedValue;
    }

}
