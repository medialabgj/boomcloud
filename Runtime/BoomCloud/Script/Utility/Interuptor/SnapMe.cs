using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapMe : MonoBehaviour
{

    private bool isApplicationQuitting = false;
    bool sceneIsUnloading = false;
    public GameObject target;
    public GameObject source;
    private GameObject OriginalParent;
    private bool isSnaping = false;

    public bool absolutePosition = false;
    private GameObject followParent;

    private void Awake()
    {
        Application.quitting += preventError;
        LoadingController.sceneIsLoading += preventError;
    }

    private void Start()
    {
        if(OriginalParent != null)
        {
            OriginalParent = source.transform.parent.gameObject;
        }
        followParent = new GameObject();
        followParent.name = "FollowParent";
    }

    private void preventError()
    {
        sceneIsUnloading = true;
    }

    private void OnDisable()
    {
        if (isApplicationQuitting || sceneIsUnloading)
            return;

        snap();
    }


    private void snap()
    {

        if (!isSnaping)
        {
            isSnaping = true;

            source.transform.parent = followParent.transform;
            if (absolutePosition)
            {
                source.transform.localPosition = Vector3.zero;
            }
        }
        else
        {
            isSnaping = false;

            if (OriginalParent != null)
            {
                source.transform.parent = OriginalParent.transform;
            }
            else
            {
                source.transform.parent = null;
            }
        }


        Invoke("ReNable", 0.1f);
    }

    private void Update()
    {
        followParent.transform.position = target.transform.position;
        followParent.transform.rotation = target.transform.rotation;
    }

    private void ReNable()
    {
        this.gameObject.SetActive(true);
    }

    void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }

}
