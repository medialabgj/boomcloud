using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HideTaggedObjs : MonoBehaviour
{
    public GameObject[] obj;
    public string tagName;
    private bool isApplicationQuitting = false;
    private void OnDisable()
    {
        if (isApplicationQuitting)
            return;

        hide();
    }

    private void OnEnable()
    {
        show();
    }

    private void Start()
    {
         obj = GameObject.FindGameObjectsWithTag(tagName);
    }
    // Start is called before the first frame update
    public void hide()
    {
        if(obj.ToList() != null)
         obj.ToList().ForEach(g => g.SetActive(false));
    }

    public void EXThide(string tag)
    {
        var objs = GameObject.FindGameObjectsWithTag(tag);

        objs.ToList().ForEach(g => g.SetActive(false));
    }

    public void EXTShow(string tag)
    {
        var objs = GameObject.FindGameObjectsWithTag(tag);

        objs.ToList().ForEach(g => g.SetActive(true));
    }

    public void show()
    {
        //GameObject[] planes = GameObject.FindGameObjectsWithTag("Plane");
        obj.ToList().ForEach(g => {
            //obj.Add(g);
            g.SetActive(true);
            });
        
    }

    void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }
}
