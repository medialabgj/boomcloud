using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetScaleToZeroInteruptor : MonoBehaviour
{
    public GameObject tagetObject;
    private bool isApplicationQuitting = false;
    bool sceneIsUnloading = false;

    public static event System.Action PrepEarth;

    private void Awake()
    {
        Application.quitting += preventError;
        LoadingController.sceneIsLoading += preventError;
    }

    private void preventError()
    {
        sceneIsUnloading = true;
    }
    private void OnDisable()
    {
        if (isApplicationQuitting || sceneIsUnloading)
            return;

        //tagetObject.transform.localScale = Vector3.zero;

        PrepEarth?.Invoke();
        Invoke("ReNable", 0.1f);
    }

    private void ReNable()
    {
        this.gameObject.SetActive(true);
    }


    void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }
}

