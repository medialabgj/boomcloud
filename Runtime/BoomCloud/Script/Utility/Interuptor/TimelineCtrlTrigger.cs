using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
public class TimelineCtrlTrigger : MonoBehaviour
{
    public PlayableDirector timeline;

    public bool DisableTimeline = false;

    public float timelineTimeScale = 1f;

    public static event System.Action<PlayableDirector, float> TimelineEvent;

    private bool isApplicationQuitting = false;

    private void OnDisable()
    {
        if (isApplicationQuitting)
            return;

        sendTimeline();
        Invoke("ReNable", 0.1f);
    }

    private void ReNable()
    {
        this.gameObject.SetActive(true);
    }

    void sendTimeline()
    {
        if (DisableTimeline)
        {
            TimelineEvent?.Invoke(null, 0);
           // Debug.Log("Disable Timeline!");
        }
        else
        {
            TimelineEvent?.Invoke(timeline, timelineTimeScale);
            //Debug.Log("Init Timeline " + timeline.name);
            CameraController.GetInstance().DisableCameraSelection();
        }

    }

    void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }
}
