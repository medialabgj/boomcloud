using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SkyBoxBlendManualUpdate : MonoBehaviour
{
    private Material blendedSkybox;
    private void Start()
    {
        blendedSkybox = GetComponent<Skybox>().material;
        blendedSkybox.SetFloat("_Blend", 0);
        blendedSkybox.SetFloat("_Rotation", 245);
    }

    public void transitionSkyBox(float value)
    {
        blendedSkybox.SetFloat("_Blend", value);
    }
}

public class LightControlAsset : PlayableAsset
{
    public LightControlBehaviour template;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<LightControlBehaviour>.Create(graph, template);
        return playable;
    }
}


[System.Serializable]
public class LightControlBehaviour : PlayableBehaviour
{
    public Color color = Color.white;
    public float intensity = 1f;
}
