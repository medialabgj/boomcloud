using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixBlendedSkyboxOnActivate : MonoBehaviour
{
    private Material blendedSkybox;

    public void resetSkyBoxBlendOnActivate()
    {
        blendedSkybox = GetComponent<Skybox>().material;
        blendedSkybox.SetFloat("_Blend", 0);
    }
}
