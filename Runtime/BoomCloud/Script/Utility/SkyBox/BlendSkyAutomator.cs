using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendSkyAutomator : MonoBehaviour
{
    public float transitionTime = 15f;
    public float delay = 5f;
    public int from = 1;
    public int to = 0;
    private float blendValue = 0;
    [Header("Leave Empty if component Skybox is near")]
    public Material blendedSkybox;
    [Header("Hide object by combining with HideTaggedObjs script")]
    public string targetTag;
    [Header("If not on a Camera need a Trigger")]
    public bool TriggerOnDisabled = false;

    private void OnDisable()
    {
        if (TriggerOnDisabled)
            startTransition();
    }

    private void blendSky()
    {
        if(blendedSkybox == null)
            blendedSkybox = GetComponent<Skybox>().material;
        gameObject.SetActive(true);
        blendedSkybox.SetFloat("_Blend", 0);
        StartCoroutine(StartBlending(from, to, transitionTime));
    }

    public void startTransition()
    {
        Invoke("blendSky", 0.1f);
        HideOBJ();
    }

    private void HideOBJ()
    {
        if(GetComponent<HideTaggedObjs>() != null)
              GetComponent<HideTaggedObjs>().EXThide(targetTag);
    }

    private void ShowOBJ()
    {
        if (GetComponent<HideTaggedObjs>() != null)
            GetComponent<HideTaggedObjs>().EXTShow(targetTag);
    }

    IEnumerator StartBlending(float v_start, float v_end, float duration)
    {
        yield return new WaitForSeconds(delay);

        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            blendValue = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed += Time.deltaTime;
            blendedSkybox.SetFloat("_Blend", blendValue);
            yield return null;
        }
    }
}
