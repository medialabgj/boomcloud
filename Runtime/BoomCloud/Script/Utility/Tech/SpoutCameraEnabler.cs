using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpoutCameraEnabler : MonoBehaviour
{
    private static SpoutCameraEnabler instance;


    public List<GameObject> cameras = new List<GameObject>();

    public static SpoutCameraEnabler GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;

        foreach(GameObject c in cameras)
        {
            c.gameObject.SetActive(false);
        }
    }



}
