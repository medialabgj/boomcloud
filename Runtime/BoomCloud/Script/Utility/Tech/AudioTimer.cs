using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AudioTimer : MonoBehaviour
{
    public AudioSource audioSource;
    public Text text;

    private void Update()
    {
        text.text = audioSource.time.ToString();
    }


}
