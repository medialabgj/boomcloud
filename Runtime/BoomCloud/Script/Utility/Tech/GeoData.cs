using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


/// <summary>
/// The Geo data for a user.
/// 
/// http://ip-api.com/docs/api:json
/// 
/// <code>
/// {
/// 	"status": "success",
/// 	"country": "COUNTRY",
/// 	"countryCode": "COUNTRY CODE",
/// 	"region": "REGION CODE",
/// 	"regionName": "REGION NAME",
/// 	"city": "CITY",
/// 	"zip": "ZIP CODE",
/// 	"lat": LATITUDE,
/// 	"lon": LONGITUDE,
/// 	"timezone": "TIME ZONE",
/// 	"isp": "ISP NAME",
/// 	"org": "ORGANIZATION NAME",
/// 	"as": "AS NUMBER / NAME",
/// 	"query": "IP ADDRESS USED FOR QUERY"
/// }
/// </code>
/// 
/// </summary>
/// [Serializable]
/// 


[System.Serializable]
public class GeoData  {

    public Coroutine coroutine { get; private set; }
    public object result;
    private IEnumerator target;

    public GeoData(MonoBehaviour owner, IEnumerator target)
    {
        this.target = target;
        this.coroutine = owner.StartCoroutine(Run());
    }
    private IEnumerator Run()
    {
        while (target.MoveNext())
        {
            result = target.Current;
            yield return result;
        }
    }

    public static IEnumerator GetGeoLocation() { 

        UnityWebRequest www = UnityWebRequest.Get("http://ip-api.com/json" );
        yield return www.SendWebRequest();
        if (www.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.Log(www.error);
            yield return www.error;
        }
        else
        {
           // GeoJson geoJson = JsonUtility.FromJson<GeoJson>(www.downloadHandler.text);
		   // Debug.Log (geoJson.country);

          //  string[] Data = { geoJson.country, geoJson.query, geoJson.city };

        yield return www.downloadHandler.text;

		}
	}
}

[System.Serializable]
public  class GeoJson
{
    /// <summary>
    /// The status that is returned if the response was successful.
    /// </summary>
    public string city;
    public string country;
    public string query;

}