using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEnabler : MonoBehaviour
{
    private static CameraEnabler instance;


    public List<Camera> cameras = new List<Camera>();

    public static CameraEnabler GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;

        foreach(Camera c in cameras)
        {
            c.gameObject.SetActive(false);
        }
    }



}
