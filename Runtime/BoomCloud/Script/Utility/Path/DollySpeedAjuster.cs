using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollySpeedAjuster : MonoBehaviour
{
    public int dollySpeedDivider = 1;

    public static event System.Action<int> DollySpeedChanged;

    public void ChangeDollySpeed()
    {
        DollySpeedChanged?.Invoke(dollySpeedDivider);
    }

}
