using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System.Linq;

public class DollyTiltAutomator : MonoBehaviour
{
    public PointInTime[] pointIn;
    private float currentPosition;
    private int currentCueNumber = 0;
    private float transitionDistance = 8;

    float currentTiltValue = 0;
    float targetTiltValue = 0;
    float transitionTitleValue = 0;

    float currentPanValue = 0;
    float targetPanValue = 0;
    float transitionPanValue = 0;

    private float startTiltValue = 0;
    private float startPanValue = 0;

    private enum Direction { forward, backward }
    private Direction direction;

    private void Start()
    {
        startTiltValue = GetComponent<CinemachineRecomposer>().m_Tilt;
        startPanValue = GetComponent<CinemachineRecomposer>().m_Pan;
    }
    private void Update()
    {

        currentPosition = GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition;

        if (pointIn.Count() == 0 || currentCueNumber > pointIn.Count()-1)
            return;
        
        float nextCue = 100000;

        //Log.Tech(currentCueNumber);

        if (currentCueNumber  < pointIn.Count())
            nextCue = pointIn[currentCueNumber].Refposition;

        if (currentCueNumber + 1 < pointIn.Count() && currentPosition > nextCue + (pointIn[currentCueNumber + 1].Refposition - pointIn[currentCueNumber].Refposition)/2)
            currentCueNumber += 1;
        
        //�a sert a rien je crois...
        if (currentCueNumber != 0 && currentPosition < nextCue - (pointIn[currentCueNumber].Refposition - pointIn[currentCueNumber-1].Refposition) / 2)
            currentCueNumber -= 1;

        //transitionDistance = (nextCue - pointIn[currentCueNumber].Refposition )/ 2;
        //Debug.Log(pointIn[currentCueNumber].Refposition + "  " + pointIn[currentCueNumber+1].Refposition);

        //transitionDistance = (pointIn[currentCueNumber+1].Refposition - pointIn[currentCueNumber].Refposition) / 2;
        //Debug.Log(currentCueNumber);

        if (currentPosition < nextCue - transitionDistance) 
            direction = Direction.forward;

        if (currentPosition > nextCue + transitionDistance)
            direction = Direction.backward;



        //Debug.Log("CueIndex " + currentCueNumber + " next cue " + nextCue + " current cue " + currentPosition + " Direction " + direction.ToString() );

        //prepare data for transition before and after being close to cue, if we are bettwen two transition value then apply tilt
        if (currentPosition < nextCue - transitionDistance || currentPosition > nextCue + transitionDistance )
        {
            //Wait to be X distance before starting transition
            if(direction == Direction.forward)
            {
                targetTiltValue = pointIn[currentCueNumber].tiltValue;
                targetPanValue = pointIn[currentCueNumber].panValue;
            }
            else if(direction == Direction.backward)
            {
                if(currentCueNumber - 1 < 0)
                {
                    targetTiltValue = startTiltValue;
                    targetPanValue = startPanValue;
                }
                else
                {
                    targetTiltValue = pointIn[currentCueNumber-1].tiltValue;
                    targetPanValue = pointIn[currentCueNumber - 1].panValue;
                }
            }
            currentTiltValue = GetComponent<CinemachineRecomposer>().m_Tilt;
            currentPanValue = GetComponent<CinemachineRecomposer>().m_Pan;
            Log.Tech("PREP TRANS  taget: " + targetTiltValue);
        }
        else
        {
            float normalizedTranstion = 0;
            //Start transition
            if (direction == Direction.forward)
            {
                normalizedTranstion = Mathf.Clamp01((currentPosition - nextCue + transitionDistance) / transitionDistance);
            }
            else if (direction == Direction.backward)
            {
                normalizedTranstion = Mathf.Clamp01((nextCue - currentPosition + transitionDistance) / transitionDistance);
            }

            Log.Tech("N Formula: currentPosition:" + currentPosition + " nextCue:" + nextCue + " transitionDistance:" + transitionDistance + " Direction:" + direction.ToString());

            transitionTitleValue = currentTiltValue + ((targetTiltValue - currentTiltValue) * normalizedTranstion);
            transitionPanValue = currentPanValue + ((targetPanValue- currentPanValue) * normalizedTranstion);

            Log.Tech("APPLY TRANS" + "TransitionTiltValue " + transitionTitleValue + " N " + normalizedTranstion);
        }

        GetComponent<CinemachineRecomposer>().m_Tilt = Mathf.Lerp(GetComponent<CinemachineRecomposer>().m_Tilt, transitionTitleValue, Time.deltaTime * 2);
        GetComponent<CinemachineRecomposer>().m_Pan = Mathf.Lerp(GetComponent<CinemachineRecomposer>().m_Pan, transitionPanValue, Time.deltaTime * 2);


    }
}

[System.Serializable]
public class PointInTime
{
    public float Refposition;
    public float tiltValue;
    public float panValue;

    public PointInTime(float _Refposition, float _tiltValue, float _panValue)
    {
        Refposition = _Refposition;
        tiltValue = _tiltValue;
        panValue = _panValue;
    }
}