using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AutoDestroy : MonoBehaviour
{
    public int delayBeforeDestroy = 20;

    private void OnEnable()
    {
        Invoke("DestroyMe", delayBeforeDestroy);
    }

    private void DestroyMe()
    {
        Destroy(this.gameObject);
    }
}
