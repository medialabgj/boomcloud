using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCustomParent : MonoBehaviour
{
    public GameObject parent;
    private GameObject offset;
    public bool useOffset;
    private void Start()
    {
        offset = gameObject;
    }
    void Update()
    {
        if (useOffset)
        {
            transform.position = parent.transform.position + offset.transform.position;
            transform.rotation = parent.transform.rotation * offset.transform.rotation;
        }
        else
        {
            transform.position = parent.transform.position;
            transform.rotation = parent.transform.rotation;
        }
        Vector3 normalizeScale = new Vector3(transform.localScale.x * parent.transform.localScale.x, transform.localScale.y * parent.transform.localScale.y, transform.localScale.z * parent.transform.localScale.z);
        transform.localScale = normalizeScale;
    }
}
