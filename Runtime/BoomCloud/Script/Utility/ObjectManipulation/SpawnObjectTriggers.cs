using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnObjectTriggers : MonoBehaviour
{
    [SerializeField]
    private enum SpawnObjectManager { start, stop}
    [SerializeField]
    private SpawnObjectManager objectManager;


    private void OnDisable()
    {
        if (objectManager == SpawnObjectManager.start)
            SpawnSpaceObjects.GetInstance().StartSpawnedObject();

        if (objectManager == SpawnObjectManager.stop)
            SpawnSpaceObjects.GetInstance().StopSpawnedObject();

        Invoke("ReNable", 0.1f);
    }

    private void ReNable()
    {
        this.gameObject.SetActive(true);
    }

}
