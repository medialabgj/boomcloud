using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeController : MonoBehaviour
{
    public int fadingTime = 4;
    private Material BlackOutMAT;
    private float blendValue = 1;


    void OnEnable()
    {

        StageSynchronizer.PushEventStringReceived += ReceivedEvent;
    }

    void OnDisable()
    {

        StageSynchronizer.PushEventStringReceived -= ReceivedEvent;
    }


    private void Start()
    {
        BlackOutMAT = GetComponent<MeshRenderer>().material;
        blendValue = GetComponent<MeshRenderer>().material.color.a;
        StartFadeOut(true);
    }


    private void ReceivedEvent(string identifier, string value)
    {

        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.sender)
        {
            if (identifier == "FadeController")
            {
                switch (value)
                {
                    case "StartFadeIN":
                        StartFadeIn(true);
                        break;
                    case "StartFadeOUT":
                        StartFadeOut(true);
                        break;
                }
            }


            Debug.Log("STAGESYNC EVENT RECEIVED: " + identifier + " cue index: " + value);
        }

    }

    private void SendEvent(string identifier, string val)
    {
        if (!BoomCloudSettings.GetInstance().isStageSyncLinked)
            return;

        if (BoomCloudSettings.GetInstance().boomcloudSyncMode != BoomCloudSettings.BoomCloudSyncMode.receiver)
        {

            BoomCloudSettings.GetInstance().stageSynchronizer.PushEvent(identifier, val);

            Debug.Log("CUE EVENT SENT" + identifier + " " + val);
        }

    }

    public void StartFadeIn(bool fromEvent = false)
    {
        if (!fromEvent)
            SendEvent("FadeController", "StartFadeIN");

        StartCoroutine(StartBlending(0, 1, fadingTime));
    }

    public void StartFadeOut(bool fromEvent = false)
    {
        if (!fromEvent)
            SendEvent("FadeController", "StartFadeOUT");

        StartCoroutine(StartBlending(1, 0, fadingTime));
    }


    IEnumerator StartBlending(float v_start, float v_end, float duration)
    {
        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            blendValue = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed += Time.deltaTime;
            var tempColor = BlackOutMAT.color;
            tempColor.a = blendValue;
            BlackOutMAT.color = tempColor;
            yield return null;
        }

        if(v_end == 0)
        {
            var tempColor = BlackOutMAT.color;
            tempColor.a = 0;
            BlackOutMAT.color = tempColor;
        }

        if (v_end == 1)
        {
            BlackOutMAT.color = Color.black;
        }
    }


}
