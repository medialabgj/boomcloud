using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Klak.Motion;
public class SpawnSpaceObjects : MonoBehaviour
{
    private static SpawnSpaceObjects instance;

    public List<GameObject> objectToSpawn;
    private List<GameObject> spawnedObjects = new List<GameObject>();
    public GameObject spawnOrigin;
    public int instanciatDelay = 2;
    public int destroyTime = 6;
    public float speed = 0.02f;
    private float timer = 0;
    // public GameObject DummyParent;
    public GameObject DummyParent;
    private bool startStopSpawn = false;

    public static SpawnSpaceObjects GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;
    }

    public void StartSpawnedObject()
    {
        startStopSpawn = true;
    }

    public void StopSpawnedObject()
    {
        startStopSpawn = false;
    }


    private void Start()
    {
        timer = instanciatDelay;
    }
    private void Update()
    {
        if (!startStopSpawn)
            return;

        timer -= Time.deltaTime;

        if (timer <= 0f)
        {
            objectToSpawn.ForEach(o => {
                GameObject parent = Instantiate(DummyParent, spawnOrigin.transform);
                parent.AddComponent<AutoDestroy>();
                parent.GetComponent<AutoDestroy>().delayBeforeDestroy = destroyTime;
                var clone = Instantiate(o, spawnOrigin.transform);
                clone.transform.SetParent(parent.transform);
                clone.AddComponent<autoScale>();
                spawnedObjects.Add(parent);
            });

            timer = instanciatDelay;
        }

        spawnedObjects.ForEach(o => {
            if (o == null)
            {
                spawnedObjects = spawnedObjects.Where(item => item != null).ToList();

                return;
            }

            o.transform.position += new Vector3(0, speed, 0);
        });
    }
}

[System.Serializable]
public class autoScale : MonoBehaviour
{

    private void Start()
    {
        gameObject.transform.localScale = new Vector3(0,0,0);
    }

    private void Update()
    {
        if (gameObject.transform.localScale.x >= 1)
            return;

        gameObject.transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
    }

}

