using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
public class RemoteObjectController : MonoBehaviour
{
    public static event System.Action<PlayableDirector> RemoteObjects;

  //  public List<remoteDistantsObject> Objects = new List<remoteDistantsObject>();
    public PlayableDirector timeline = new PlayableDirector();
    public bool sendOnStartUp = false;

    /*
     * 
     * Faire list par type comme cue controller
     * 
     * 
     * */

    private void OnEnable()
    {
        SceneController.InitScene += Init;
    }

    private void OnDisable()
    {
        
        SceneController.InitScene -= Init;
        RemoteObjects?.Invoke(timeline);
    }

    private void Init(DataScene data)
    {
        //List<GameObject> objectToSend = null;

        //Objects.ForEach(t => objectToSend.Add(new GameObject(t.Objects)));
        /*
        foreach(remoteDistantsObject ObjectsList in Objects)
        {
            ObjectsList.OfType = ObjectsList.Objects.GetType().ToString();
        }
        */

        if(timeline != null && sendOnStartUp)
            RemoteObjects?.Invoke(timeline);

    }
}

[System.Serializable]
public class remoteDistantsObject
{
    public GameObject Objects;
    [HideInInspector]
    public string OfType;

    public remoteDistantsObject(GameObject _objects,  string _ofType)
    {
        Objects = _objects;
        OfType = _ofType;
    }
}

