using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
public class FollowRemoteParent : MonoBehaviour
{
    private GameObject parent;
    private GameObject offset;
    public string remoteParentTag;
    private bool starto;
    private void OnEnable()
    {
        TimelineCtrlTrigger.TimelineEvent += manageStarto;
        SetScaleToZeroInteruptor.PrepEarth += SetUpEarthScale;
    }

    private void OnDisable()
    {
        TimelineCtrlTrigger.TimelineEvent -= manageStarto;
        SetScaleToZeroInteruptor.PrepEarth -= SetUpEarthScale;
    }

    private void Start()
    {
        offset = gameObject;
        var obj = GameObject.FindGameObjectWithTag(remoteParentTag);
        parent = obj;
    }

    void SetUpEarthScale()
    {
        transform.localScale = Vector3.zero;
    }


    void manageStarto(PlayableDirector timeline, float timescale)
    {
        if(timeline != null)
        {
            starto = true;
        }
        else
        {
            starto = false;
        }
    }

    void Update()
    {
        
        if (starto)
        {
            transform.position = parent.transform.position + offset.transform.position;
            transform.rotation = parent.transform.rotation;
            transform.localScale = parent.transform.localScale;
        }
        
    }
}
