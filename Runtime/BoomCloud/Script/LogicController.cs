﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Cinemachine;


public class LogicController : MonoBehaviour
{
    private static LogicController instance;


    /*****SETTINGS*****/

    public enum LedColorPicker { red, blue, yellow, white, green, pink, lightBlue, darkRed, purple };


    [Serializable]
    public struct CameraRow
    {
        public int row;
        public LayerMask layer;
        public LedColorPicker cameraColor;
    }

    private List<CameraRow> cameraRow = new List<CameraRow>();
    private int actionCueRow;
    private LedColorPicker actionLedColor;

    private int audioCueRow;
    private LedColorPicker audioLedColor;

    /*Scene Loading cue -> Disabled
    public int sceneCueRow;
    public LedColorPicker sceneLedColor;
    */

    private bool ledDisplayCameraGroupe = false;
    private int cameraGroupeRow;
   // public LedColorPicker groupeColor;

    public static DataScene dataScene { get; private set; }

    [HideInInspector]
    public List<string> incomingSceneName = new List<string>();

    [HideInInspector]
    public float lateStartTime = 0.1f;

    public static LogicController GetInstace()
    {
        return instance;
    }

    private void OnEnable()
    {
        SceneController.InitScene += initScene;
    }

    private void OnDisable()
    {
        SceneController.InitScene -= initScene;
    }

    private void Awake()
    {
        instance = this;


        dataScene = new DataScene();
    }

    private void Start()
    {
        for(int i = 0; i < BoomCloudSettings.GetInstance().cameraSettings.Count; i++)
        {
            CameraRow camData = new CameraRow();

            camData.layer = BoomCloudSettings.GetInstance().cameraSettings[i].layer;
            camData.cameraColor = BoomCloudSettings.GetInstance().cameraSettings[i].cameraColor;
            camData.row = BoomCloudSettings.GetInstance().cameraSettings[i].midiRow;

            cameraRow.Add(camData);
        }

        /*
        BoomCloudSettings.GetInstance().cameraSettings.ForEach(c => {
            CameraRow camData = new CameraRow();

            camData.layer = c.layer;
            camData.cameraColor = c.cameraColor;
          //  camData.row = c.midiRow;

            cameraRow.Add(camData);

        });
        */

        actionCueRow = BoomCloudSettings.GetInstance().actionCueRow;
        actionLedColor = BoomCloudSettings.GetInstance().actionColor;
        audioCueRow = BoomCloudSettings.GetInstance().audioCueRow;
        audioLedColor = BoomCloudSettings.GetInstance().audioColor;
        ledDisplayCameraGroupe = BoomCloudSettings.GetInstance().ledDisplayCameraGroupe;
        cameraGroupeRow = BoomCloudSettings.GetInstance().cameraGroupeRow;
    }

    public void initScene(DataScene data)
    {
        dataScene = data;
        sortData();
    }

    private void sortData()
    {

        // Debug.Log("TEST " + LayerMask.LayerToName(Mathf.RoundToInt(Mathf.Log(cameraRow[0].layer.value, 2))));
        try
        {
            dataScene.CameraRowLayer = cameraRow.ToDictionary(c => c.row, c => Mathf.RoundToInt(Mathf.Log(c.layer.value, 2)));
        }
        catch (Exception e)
        {
            Debug.LogError("BOOMCLOUD ERROR: Camera midi row cannot be the same, change the camera settings in BoomCloudMaster " + e);
        }
        dataScene.CameraLayerRow = cameraRow.ToDictionary(c => Mathf.RoundToInt(Mathf.Log(c.layer.value, 2)), c => c.row);
        dataScene.cameraColor = cameraRow.ToDictionary(c => Mathf.RoundToInt(Mathf.Log(c.layer.value, 2)), c => c.cameraColor);
        dataScene.actionLedColor = actionLedColor;
        dataScene.actionRow = actionCueRow;
        dataScene.audioLedColor = audioLedColor;
        //dataScene.spaceLedColor = sceneLedColor;
        //dataScene.cameraGroupeColor = groupeColor;
        dataScene.audioRow = audioCueRow;
        //dataScene.sceneRow = sceneCueRow;
        dataScene.groupeRow = cameraGroupeRow;
        dataScene.sceneLoaderList = incomingSceneName;
        dataScene.useCamGroupOnLed = ledDisplayCameraGroupe;


        sortCameraData();
        sortCueData();

        initCamera();
        //InitCueScene();
        MidiLedController.GetInstance().UpdateMatrixLed();

    }




    private void sortCameraData()
    {
        //init cue list for each camera presente on the scene with default noCue
        foreach (int layer in dataScene.GetAllDifferentCamera())
        {
            dataScene.camerasState[layer] = new List<CameraState>();
            for (int e = 0; e < dataScene.CamerasCuesList.Count(); e++)
            {
                CameraState data = new CameraState();
                data.cueState = DataScene.CueStatsType.cue;
                //dataScene.CuesPerCamera[layer][e].cueState = new DataScene.CueStatsType();
                dataScene.camerasState[layer].Add(data);
                dataScene.camerasState[layer][e].cueState = DataScene.CueStatsType.noCue;
                //Debug.Log(dataScene.CuesPerCamera[layer][e].cueState);
            }
        }

        //sort what cue is active per camera by changing noCue to Cue 
        int i = 0;
        dataScene.CamerasCuesList.ToList().ForEach(c =>
        {
           // Log.Tech("Going though Cue " + c.cueName);
            c.UpdateDict();
            foreach (CinemachineVirtualCamera camera in c.virtualCamerasCue)
            {
                dataScene.camerasState[camera.gameObject.layer][i].cueState = DataScene.CueStatsType.cue;
                dataScene.camerasState[camera.gameObject.layer][i].cameraLink = camera;

            }
            i++;
        });


        dataScene.camerasState.ToList().ForEach(c =>
        {
            Log.Status("Total N° of cue:" + i + " Camera : " + LayerMask.LayerToName(c.Key) + "// " + String.Join(" , ", new List<CameraState>(c.Value).ConvertAll(e => e.cueState.ToString()).ToArray()));
        });
    }

    private void sortCueData()
    {
        int i = 0;
        dataScene.actionsCues.ToList().ForEach(c =>
        {
           // Log.Tech("Going though Cue " + c.cueName);
            dataScene.actionsCuesState[i] = new DataScene.CueStatsType();
            dataScene.actionsCuesState[i] = DataScene.CueStatsType.cue;

            i++;
        });

        i = 0;
        dataScene.audioCues.ToList().ForEach(c =>
        {
            // Log.Tech("Going though Cue " + c.cueName);
            dataScene.audiosCuesState[i] = new DataScene.CueStatsType();
            dataScene.audiosCuesState[i] = DataScene.CueStatsType.cue;

            i++;
        });

        i = 0;
        dataScene.sceneLoaderList.ForEach(c =>
        {
            dataScene.spacesCuesState[i] = new DataScene.CueStatsType();
            dataScene.spacesCuesState[i] = DataScene.CueStatsType.cue;
            /*// Make first CUE green
            if(i == 0)
                dataScene.CuesPerSpace[i] = DataScene.CueStatsType.activeCue;
                */

            i++;
        });

        i = 0;
        dataScene.actionsCuesState.ToList().ForEach(c =>
        {
            //Log.Status("Total N° of Objcet cue:" + c.Key + " " + dataScene.SceneCue[i].cueName + " // " + String.Join(" , ", new DataScene.CueStatsType(c.Value).ConvertAll(e => e.ToString()).ToArray()));
            i++;
        });


        //CueController.GetInstance().InitCueScene(dataScene);
    }

    private void initCamera()
    {
        foreach(CinemachineVirtualCamera camera in dataScene.CamerasCuesList[0].virtualCamerasCue)
        {
            //Va activer les caméras du 1er CUE
            camera.Priority = 1000;
            //  datascene().ActiveCamera[camera.gameObject.layer].cameraLink= camera;
            dataScene.camerasState[camera.gameObject.layer][0].cueState = DataScene.CueStatsType.activeCue;
            dataScene.previousActiveCamera[camera.gameObject.layer] = new PreviousCameraState();
            dataScene.previousActiveCamera[camera.gameObject.layer].cameraLink = camera;
            dataScene.previousActiveCamera[camera.gameObject.layer].previousIndex = 0;
            //  previousActiveCamera[camera.gameObject.layer] = camera;
        }

    }

    /*
    public void InitCueScene()
    {
        ResetCueList();
        cuesLists = dataScene.actionsCues.ToList();
        propsList = dataScene.PropsList.ToList();
        audioCueList = dataScene.audioCues.ToList();
        dissolveGameObject = dataScene.dissolveController;
        // MidiLedController.GetInstance().initLedCue(new CueAction(CueType.Object , cuesLists.Count));
    }

    private void ResetCueList()
    {
        Log.Tech("Reseting Cue Liste");
        cueNumber = -1;
        propsList = null;
        cuesLists.Clear();
    }
    */

    public static CinemachineVirtualCamera GetVirtualCamera(int layer, int index)
    {
        return dataScene.camerasState[layer][index].cameraLink;
    }

    public static void SetPreviousActiveCameraState(int layer, int index)
    {
        dataScene.camerasState[layer][dataScene.previousActiveCamera[layer].previousIndex].cueState = DataScene.CueStatsType.cue;
        dataScene.camerasState[layer][dataScene.previousActiveCamera[layer].previousIndex].cameraLink.Priority = 10;
        dataScene.previousActiveCamera[layer].cameraLink = GetVirtualCamera(layer,index);
        dataScene.previousActiveCamera[layer].previousIndex = index;
    }

    public static void SetActiveCameraState(int layer, int index)
    {
        dataScene.camerasState[layer][index].cameraLink.Priority = 1000;
        dataScene.camerasState[layer][index].cueState = DataScene.CueStatsType.activeCue;
    }


    public static void SetActiveActionCueState(int index)
    {
        for (int e = 0; e < dataScene.actionsCuesState.Count; e++)
        {
            dataScene.actionsCuesState[e] = DataScene.CueStatsType.cue;
        }
        dataScene.actionsCuesState[index] = DataScene.CueStatsType.activeCue;
    }

    public static void SetActiveAudioCueState(int index)
    {
        for (int e = 0; e < dataScene.audiosCuesState.Count; e++)
        {
            dataScene.audiosCuesState[e] = DataScene.CueStatsType.cue;
        }
        dataScene.audiosCuesState[index] = DataScene.CueStatsType.activeCue;
    }

    //Space cues refere to scene, called space to avoid conflict so space == scene in project
    public static void SetActiveSpaceCueState(int index)
    {
        for (int e = 0; e < dataScene.spacesCuesState.Count; e++)
        {
            dataScene.spacesCuesState[e] = DataScene.CueStatsType.cue;
        }
        dataScene.spacesCuesState[index] = DataScene.CueStatsType.activeCue;
    }

    public static int GetActiveCameraIndex(int layer)
    {
        return dataScene.camerasState[layer].IndexOf(dataScene.camerasState[layer].First(c => c.cueState == DataScene.CueStatsType.activeCue));
    }

    //  public void Set

}
