﻿using UnityEngine;
//using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

public class DuplicatorController : MonoBehaviour
{
    private static DuplicatorController instance;

    public string DanserName;
    public GameObject dancerToTimeDuplicate;
    public int numberOfDuplication;
    public int delayInFrame;

    private int startFrame;
    private int visibleAvatar = 0;
    private int margin = 5;
    private int displayDelay;

    List<GameObject> listOfDanserToDuplicate;

    private bool duplicationStarted = false;

    public static DuplicatorController GetInstance()
    {
        return instance;
    }
    private void OnEnable()
    {
        MidiDeviceController.DeleteClonedDanser += deleteAllInstance;
    }
    private void OnDisable()
    {
        MidiDeviceController.DeleteClonedDanser -= deleteAllInstance;
    }

    private void Awake()
    {
        instance = this;

    }
    void Start()
    {
        displayDelay = delayInFrame;
        listOfDanserToDuplicate = new List<GameObject>();
       // StartDuplication();
       Invoke("StartDuplication", 2f);
    }

    public void StartDuplication()
    {
        duplicationStarted = true;
        startFrame = Time.frameCount;
        StartCoroutine(AutoDuplication());
        Log.LogMe("Duplicator - Start of duplication");
    }

    private void Update()
    {
        if (duplicationStarted)
        {
            if (Time.frameCount == displayDelay + startFrame + margin)
            {
                showAvatar();
                displayDelay += delayInFrame;
            }
        }
    }

    public void deleteAllInstance()
    {
        listOfDanserToDuplicate.ForEach(o => Destroy(o));
        listOfDanserToDuplicate.Clear();
        visibleAvatar = 0;
    }

    public void showAvatar()
    {
        if (visibleAvatar < numberOfDuplication)
        {

            foreach (Transform child in listOfDanserToDuplicate[visibleAvatar].transform)
            {
                if (child.gameObject.GetComponent<SkinnedMeshRenderer>() == true)
                {
                    child.gameObject.GetComponent<SkinnedMeshRenderer>().enabled = true;
                    Debug.Log("Duplicator - Showing copy of: " + child.parent.name);
                }
            }
            visibleAvatar++;
        }
        else
        {
            Log.LogMe("Duplicator - No more avatar to show");
            duplicationStarted = false;
        }
    }

    IEnumerator AutoDuplication()
    {
        int delay = 0;
        Transform parent = dancerToTimeDuplicate.transform.parent;
        for (int i = 0; i < numberOfDuplication; i++)
        {
            var newAvatar = Instantiate(dancerToTimeDuplicate, parent.position, parent.rotation, parent);
            foreach (Transform child in newAvatar.transform)
            {
                if (child.gameObject.GetComponent<SkinnedMeshRenderer>() == true)
                {
                    child.gameObject.GetComponent<SkinnedMeshRenderer>().enabled = false;
                }
            }

            yield return new WaitForSeconds(0.001f);

            listOfDanserToDuplicate.Add(newAvatar);
            delay += delayInFrame;
            //newAvatar.GetComponent<RTSkeletonRB>().FrameDelay = delay;
            yield return new WaitForSeconds(0.001f);

        }
        yield return null;
    }
}

