﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RtMidi.LowLevel;
using System.Linq;
using System;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
public enum LedDataType { PassiveColor, ActiveColor };

[System.Serializable]
public struct RowColumn
{
    public int Row { get; set; }
    public int Column { get; set; }

    public RowColumn(int row, int column)
    {
        Row = row;
        Column = column;
    }
}


public class MidiLedController : MonoBehaviour
{
    [SerializeField]
    private string targetDevice = "APC40 mkII";
    [SerializeField]
    private enum PortSelection { port0, port1, port2 };
    [SerializeField]
    private PortSelection portSelection;
    public Vector2 matrixSize = new Vector2(8, 8);
    [SerializeField]
    private int midiLedStartingNoteValue = 0;
    [SerializeField]
    private int matrixRightOffset = 0;
    [SerializeField]
    private int midiLedChannelOut = 0;
    //[HideInInspector]
    public List<int> ledColorValue => new List<int> { 5, 45, 74, 119, 122, 53, 78, 121, 69 };

    public int page = 0;
    public int incrementPerPage = 4;
    private int increment = 0;

    private Vector2Int currentDisplayedRange;

    bool noMidi = true;

    bool resetSpaceLed = true;
    //int cueSceneStartRow = 0;
    //SETTINGS
    //Row, passive color, active color, last active -1 == null; REF: https://d2r1vs3d9006ap.cloudfront.net/s3_images/1365219/TechHippAPC40MK2RGBVelocityColorChart2016-02-13.jpg?1455410962

    // List<int> previousCameraCueData;
    private bool gridStatus;
    private int[] gridStatusColor = { 122, 46 };

    MidiOutPort midiDevicePort;

    MidiProbe _probe;
    List<MidiOutPort> _ports = new List<MidiOutPort>();

    private static MidiLedController instance;


    public static event System.Action<int> CueSelection;
    public static event System.Action<int> UpdatePage;


    public static MidiLedController GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        instance = this;

        currentDisplayedRange = new Vector2Int(0, (int)matrixSize[1]);

    }

    private void OnEnable()
    {
        UIController.UiPageChanged += updateOutlineUI;
    }

    private void OnDisable()
    {
        UIController.UiPageChanged -= updateOutlineUI;
        ClearLedPanel(true);

    }


    private void OninitCamera()
    {

    }
    private void Start()
    {
        _probe = new MidiProbe(MidiProbe.Mode.Out);
        ScanPorts();
        // LogicController.dataScene.setMatrixSize(matrixSize);

        if (_ports.Count > 0)
        {
            midiDevicePort = _ports[(int)portSelection];
            noMidi = false;
            Log.LogMe("Device: " + targetDevice + " founded  on port" + _ports[(int)portSelection]);
        }
        else
        {
            noMidi = true;
            Debug.LogWarning("BoomCloud: No MIDI LED device found!");
        }

        if (noMidi)
            return;

        StartCoroutine(initLedControler());
        //StartCoroutine(checkLedNumber());

        InputSystem.onDeviceChange += (device, change) =>
        {
            var midiDevice = device as Minis.MidiDevice;
            if (midiDevice == null) return;

            midiDevice.onWillNoteOn += (note, velocity) => {

                if (string.IsNullOrEmpty(targetDevice) || !note.device.description.product.Contains(targetDevice))
                {
                    return;
                }

                MatrixNote(note.noteNumber);

                Log.Tech("Device " + note.device.description.product + " note matching target " + targetDevice);
            };
        };
    }
    void MatrixNote(int noteIndex)
    {
        int row = GetRowColumIndexByMidiValue(noteIndex).Row;
        int index = GetRowColumIndexByMidiValue(noteIndex).Column + increment;
        if (!LogicController.dataScene.CameraRowLayer.ContainsKey(row) && LogicController.dataScene.actionRow != row && LogicController.dataScene.audioRow != row && LogicController.dataScene.sceneRow != row && LogicController.dataScene.groupeRow != row)
            return;
        /*
        if (LogicController.dataScene.useCamGroupOnLed && LogicController.dataScene.groupeRow != row)
            return;
            */
        if (LogicController.dataScene.CameraRowLayer.ContainsKey(row))
        {
            int layerKey = LogicController.dataScene.CameraRowLayer[LogicController.dataScene.CameraRowLayer.Keys.FirstOrDefault(key => key == row)];
            CameraController.GetInstance().goToCameraCue(index, layerKey);
            UIController.GetInstance().UpdateUiCamera(index, layerKey);
            UpdateActiveLed(noteIndex, DataScene.CueType.camera, layerKey);

            //UIController.GetInstance().UIpdateCameraCuePosition(layerKey);
            CueSelection?.Invoke(layerKey);

            //  if (LogicController.dataScene.CamerasCuesList[index].objectCueNameToSynchronize != null && LogicController.dataScene.CamerasCuesList[index].camera.ToString() == LayerMask.LayerToName(layerKey)) 
            //UpdateActiveLed(noteIndex, -1);

        }

        if (row == LogicController.dataScene.actionRow)
        {
            UpdateActiveLed(noteIndex, DataScene.CueType.action);
            CueController.GetInstance().goToCue(index);
        }

        if (row == LogicController.dataScene.audioRow)
        {

            UpdateActiveLed(noteIndex, DataScene.CueType.audio);
            CueController.GetInstance().goToAudioCue(index);
        }

        if (row == LogicController.dataScene.sceneRow)
        {

            //  UpdateActiveLed(noteIndex, -3);
            //   LoadingController.GetInstance().StartCoroutine("LoadScene",index);
        }


        if (row == LogicController.dataScene.groupeRow && LogicController.dataScene.useCamGroupOnLed)
        {
            LogicController.dataScene.GetAllDifferentCamera().ForEach(c => {

                goToCameraGroupeCue(index, c);

            });
        }

        // Debug.Log("PRESSING ON LAYER: " + LayerMask.LayerToName(layerKey));


    }

    private void goToCameraGroupeCue(int index, int layer)
    {
        // Debug.Log("GO TO CUE " + cueType.ToString() + " at index " + cueIndex);
        CameraController.GetInstance().goToCameraCue(index, layer);
        UIController.GetInstance().UpdateUiCamera(index, layer);
        exteralMidiUpdate(index, layer);

        // CueContro    ller.GetInstance().goToAudioCue(cueIndex - 1);
    }

    public void externalAudioTrigger(int index)
    {
        if (noMidi)
            return;
        // MatrixNote(GetMidiValueByRow(LogicController.dataScene.audioRow)[index]-increment);

        LogicController.SetActiveAudioCueState(index);

        UpdateActiveLed(GetMidiValueByRow(LogicController.dataScene.audioRow)[index], DataScene.CueType.audio);
    }

    public void exteralMidiUpdate(int index, int layer, DataScene.CueType cueType = DataScene.CueType.NaN)
    {
        if (noMidi)
            return;

        //a tester
        if (index > currentDisplayedRange[1] - 1)
        {
            UpdateCurrentPage(1);
            return;
        }

        if (index < currentDisplayedRange[0])
        {
            UpdateCurrentPage(-1);
            return;
        }

        if (index < currentDisplayedRange[0] || index > currentDisplayedRange[1] - 1)
        {
            foreach (KeyValuePair<int, List<CameraState>> data in LogicController.dataScene.camerasState)
            {
                int i = 0;
                data.Value.ForEach(c => {
                    sendColorToDevice(c.cueState, DataScene.CueType.camera, i, data.Key);
                    i++;
                });
            }
            return;
        }
        // Debug.Log("index " + index + " curent diplaying " + currentDisplayedRange);

        //fix camera update
        if (layer == -1)
        {
            UpdateActiveLed(GetMidiValueByRow(index - increment)[index - increment-1], DataScene.CueType.action, layer);
        }
        else
        {
            UpdateActiveLed(GetMidiValueByRow(index - increment)[index - increment], DataScene.CueType.camera, layer);
        }


    }

    private void UpdateActiveLed(int index, DataScene.CueType typeOfCue, int layer = -10)
    {
        if (noMidi)
            return;

        var deltaIndex = GetRowColumIndexByMidiValue(index).Column + increment;


        if (typeOfCue == DataScene.CueType.space)
        {
            if (deltaIndex >= LogicController.dataScene.spacesCuesState.Count)
                return;

            LogicController.SetActiveSpaceCueState(index);

            int e = 0;
            foreach (KeyValuePair<int, DataScene.CueStatsType> data in LogicController.dataScene.spacesCuesState)
            {
                sendColorToDevice(data.Value, DataScene.CueType.space, e);
                e++;
            }
            return;
        }


        if (typeOfCue == DataScene.CueType.audio)
        {
            if (deltaIndex >= LogicController.dataScene.audioCues.Length)
                return;

            LogicController.SetActiveAudioCueState(deltaIndex);

            int e = 0;
            foreach (KeyValuePair<int, DataScene.CueStatsType> data in LogicController.dataScene.audiosCuesState)
            {
                sendColorToDevice(data.Value, DataScene.CueType.audio, e);
                e++;
            }
            return;
        }

        if (typeOfCue == DataScene.CueType.action)
        {

            if (deltaIndex > LogicController.dataScene.actionsCues.Length)
                return;
            LogicController.SetActiveActionCueState(deltaIndex);


            int e = 0;
            foreach (KeyValuePair<int, DataScene.CueStatsType> data in LogicController.dataScene.actionsCuesState)
            {
                sendColorToDevice(data.Value, DataScene.CueType.action, e);

                e++;
            }

            return;
        }

        if (typeOfCue == DataScene.CueType.camera)
        {

            if (deltaIndex >= LogicController.dataScene.camerasState[layer].Count)
                return;

            DataScene.CueStatsType currentState = LogicController.dataScene.camerasState[layer][GetRowColumIndexByMidiValue(index).Column + increment].cueState;

            if (currentState == DataScene.CueStatsType.noCue)
                return;

            foreach (KeyValuePair<int, List<CameraState>> data in LogicController.dataScene.camerasState)
            {
                int i = 0;
                data.Value.ForEach(c => {
                    sendColorToDevice(c.cueState, DataScene.CueType.camera, i, data.Key);
                    i++;
                });
            }
        }

    }

    void ScanPorts()
    {
        for (var i = 0; i < _probe.PortCount; i++)
        {
            var name = _probe.GetPortName(i);
            if (IsRealPort(name))
            {
                _ports.Add(new MidiOutPort(i));
            }

        }
    }

    private List<int> GetMidiValueByRow(int index)
    {
        List<int> rowsValue = new List<int>();

        if (index > matrixSize.y || index < 0)
        {
            Log.Error("Row index is out of range, check midi controller settings! indexRow: " + index);
            return null;
        }

        int delta = midiLedStartingNoteValue + (index * (int)matrixSize.y) + (index * matrixRightOffset);
        for (int i = delta; i < delta + (int)matrixSize.x; i++)
        {
            rowsValue.Add(i);
        }

        return rowsValue;
    }


    private RowColumn GetRowColumIndexByMidiValue(int index)
    {
        int column = (index - midiLedStartingNoteValue) % ((int)matrixSize.x + matrixRightOffset);
        int row = (int)Mathf.Floor((index - midiLedStartingNoteValue) / (matrixSize.y + matrixRightOffset));

        RowColumn rowColumn = new RowColumn(row, column);

        Log.Tech("PUSHING ROW: " + rowColumn.Row + " AT COLOUMN " + rowColumn.Column);

        return rowColumn;
    }


    public void ClearLedPanel(bool fullclear = false, int from = -1, int to = -1, bool onlyCuePanel = false)
    {
        int range = 127;

        if (from == -1)
        {
            from = 0;
        }

        if (to == -1)
        {
            to = range;
        }

        if (onlyCuePanel == true)
        {
            range = 40;
        }

        for (int i = from; i < to; i++)    //Erase all led
        {
            if (noMidi)
                return;

            midiDevicePort.SendNoteOn(midiLedChannelOut, i, 0);
        }

        if (fullclear == false)
            initConstantLed();
    }

    public void ClearRow(int row)
    {
        var values = GetMidiValueByRow(row);
        values.ForEach(v => midiDevicePort.SendNoteOn(midiLedChannelOut, v, 0));
    }

    public void UpdateCurrentPage(int pageIncrementation)
    {
        if (page + pageIncrementation < 0)
        {
            page = 0;
        }
        else
        {
            page += pageIncrementation;
        }

        increment = page * incrementPerPage;
        currentDisplayedRange = new Vector2Int(increment, (int)matrixSize[1] + increment);
        //Debug.Log(currentDisplayedRange);
        UpdatePage?.Invoke(increment);
        //UIController.GetInstance().UIpdateAudioCuePosition(LogicController.dataScene.CuesPerAudio.FirstOrDefault(a => a.Value == DataScene.CueStatsType.activeCue).Key);
        //UIController.GetInstance().updateInstanciedCueUI(increment);
        UpdateMatrixLed();
        updateOutlineUI();
        //UIController.GetInstance().Outline(currentDisplayedRange);
    }


    bool IsRealPort(string name)
    {
        return !name.Contains("Through") && !name.Contains("RtMidi") && name.Contains(targetDevice);
    }

    private IEnumerator checkLedNumber()
    {
        ClearLedPanel();

        foreach (int rowToLight in GetMidiValueByRow(0))
        {
            midiDevicePort.SendNoteOn(0, rowToLight, 10);
        }

        yield return null;
    }

    public void ledColorStatus(bool status)
    {
        if (status)
        {
            //   midiDevicePort.SendNoteOn(midiLedChannelOut, 82, gridStatusColor[0]);
        }
        else
        {
            //   midiDevicePort.SendNoteOn(midiLedChannelOut, 82, gridStatusColor[1]);
        }
    }

    IEnumerator initLedControler()
    {
        ClearLedPanel();

        for (int i = 0; i < matrixSize.y; i++)
        {
            foreach (int rowToLight in GetMidiValueByRow(i))
            {
                midiDevicePort.SendNoteOn(midiLedChannelOut, rowToLight, 37);
            }
        }

        yield return new WaitForSeconds(0.01f);

        ClearLedPanel();
        // initConstantLed();
    }

    private void initConstantLed()
    {
        //SUS MAE Duplication

        if (noMidi)
            return;

        if (targetDevice == "LPMiniMK3 MIDI")
        {
            midiDevicePort.SendNoteOn(midiLedChannelOut, 93, 127);  //Next
            midiDevicePort.SendNoteOn(midiLedChannelOut, 94, 127);  //Previous

            midiDevicePort.SendNoteOn(midiLedChannelOut, 98, 127);  //Toggle Grid
            midiDevicePort.SendNoteOn(midiLedChannelOut, 97, 126);  //Toggle Grid
        }

        if (targetDevice == "Launchpad")
        {
            midiDevicePort.SendNoteOn(midiLedChannelOut, 8, 127);  //Next
            midiDevicePort.SendNoteOn(midiLedChannelOut, 24, 127);  //Previous
        }

        //midiDevicePort.SendNoteOn(midiLedChannelOut, 81, 80);

        /*
        midiDevicePort.SendNoteOn(midiLedChannelOut, 71, 30);
        midiDevicePort.SendNoteOn(midiLedChannelOut, 61, 40);
        */
        //  midiDevicePort.SendNoteOn(midiLedChannelOut, 93, 127);
    }

    private void updateOutlineUI()
    {
        if (noMidi)
            return;

        UIController.GetInstance().Outline(currentDisplayedRange);
    }
    public void UpdateMatrixLed()
    {
        // LogicController.dataScene.GetAllDifferentCamera().ForEach(c => ClearRow(GetMidiValueByRow(LogicController.dataScene.CameraLayerRow[c])));
        //  ClearRow(GetMidiValueByRow(0));
        if (noMidi)
            return;

        foreach (int row in LogicController.dataScene.CameraLayerRow.Values) { ClearRow(row); }
        ClearRow(LogicController.dataScene.actionRow);
        ClearRow(LogicController.dataScene.audioRow);
        if (resetSpaceLed)
            ClearRow(LogicController.dataScene.sceneRow);

        if (LogicController.dataScene.useCamGroupOnLed)
            ClearRow(LogicController.dataScene.groupeRow);




        foreach (KeyValuePair<int, List<CameraState>> data in LogicController.dataScene.camerasState)
        {
            int i = 0;
            data.Value.ForEach(c => {
                sendColorToDevice(c.cueState, DataScene.CueType.camera, i, data.Key);
                i++;
            });
        }

        int e = 0;
        foreach (KeyValuePair<int, DataScene.CueStatsType> data in LogicController.dataScene.actionsCuesState)
        {
            //-1 tricks for sending color value to scene Cue Data
            sendColorToDevice(data.Value, DataScene.CueType.action, e);
            e++;
        }

        e = 0;
        foreach (KeyValuePair<int, DataScene.CueStatsType> data in LogicController.dataScene.audiosCuesState)
        {
            //-1 tricks for sending color value to scene Cue Data
            sendColorToDevice(data.Value, DataScene.CueType.audio, e);
            e++;
        }

        e = 0;
        if (resetSpaceLed)
            foreach (KeyValuePair<int, DataScene.CueStatsType> data in LogicController.dataScene.spacesCuesState)
            {
                //-1 tricks for sending color value to scene Cue Data
                sendColorToDevice(data.Value, DataScene.CueType.space, e);
                e++;
            }

        resetSpaceLed = false;

        e = 0;
        LogicController.dataScene.CamerasCuesList.ToList().ForEach(c => {
            {
                sendColorToDevice(DataScene.CueStatsType.cue, DataScene.CueType.cameraGroupe, e);
                e++;
            }
        });

    }

    private void sendColorToDevice(DataScene.CueStatsType cueState, DataScene.CueType typeOfCue, int index, int layer = -10)
    {
        //Debug.Log("coming from " + cueState.ToString() + " layer " + layer +" at index " +  index + " " + currentDisplayedRange);
        if (index < currentDisplayedRange[0] || index >= currentDisplayedRange[1])
            return;

        int row = 0;
        int colorData = 0;

        switch (typeOfCue)
        {
            case DataScene.CueType.action:
                row = LogicController.dataScene.actionRow;
                // colorData = ledColorValue[(int)LogicController.dataScene.actionLedColor];
                colorData = ledColorValue[(int)LogicController.dataScene.actionsCues[index].ledColor];
                break;
            case DataScene.CueType.audio:
                row = LogicController.dataScene.audioRow;
                colorData = ledColorValue[(int)LogicController.dataScene.audioLedColor];
                break;
            case DataScene.CueType.space:
                row = LogicController.dataScene.sceneRow;
                colorData = ledColorValue[(int)LogicController.dataScene.spaceLedColor];
                break;
            case DataScene.CueType.cameraGroupe:
                if (!LogicController.dataScene.useCamGroupOnLed)
                    return;
                row = LogicController.dataScene.groupeRow;
                colorData = 1; //grey color
                break;
            case DataScene.CueType.camera:
                row = LogicController.dataScene.CameraLayerRow[layer];
                colorData = ledColorValue[(int)LogicController.dataScene.cameraColor[layer]];
                break;
            default:
                Debug.LogWarning("BoomCloud Type of Cue not found");
                return;
        }


        int delta = midiLedStartingNoteValue + (row * (int)matrixSize.y) + (row * matrixRightOffset);
        int color = 0;

        int midiIndex = index - currentDisplayedRange[0];

        switch (cueState)
        {
            case DataScene.CueStatsType.activeCue:
                color = 122;                //Green for active color 122MKIII   60on launchpad
                break;
            case DataScene.CueStatsType.cue:
                color = colorData;
                break;
            case DataScene.CueStatsType.noCue:
                color = 0;
                break;
            case DataScene.CueStatsType.disable:
                color = 0;
                break;
        }
        midiDevicePort.SendNoteOn(midiLedChannelOut, delta + midiIndex, color);
    }


    public void manualMidiSend(int index, int row, int color = -1)
    {

        if (noMidi)
            return;


        if (row > (int)matrixSize.y)
        {
            Debug.LogWarning("row is bigger than Matrix Size Y: row " + row + " Matrix Size Y: " + (int)matrixSize.y);
            return;
        }

        if (index > (int)matrixSize.x)
        {
            Debug.LogWarning("index is bigger than Matrix Size X: index " + index + " Matrix Size Y: " + (int)matrixSize.x);
            return;
        }

        //Debug.Log("Sending index: " + index + " Row: " + row + " Color: " + color);

        int delta = midiLedStartingNoteValue + (row * (int)matrixSize.y) + (row * matrixRightOffset);
        if (color == -1)
        {
            color = 122;
        }
        midiDevicePort.SendNoteOn(midiLedChannelOut, delta + index, color);
    }

    void OnDestroy()
    {
        _probe?.Dispose();
        DisposePorts();
    }



    void DisposePorts()
    {
        foreach (var p in _ports) p?.Dispose();
        _ports.Clear();
    }
}
