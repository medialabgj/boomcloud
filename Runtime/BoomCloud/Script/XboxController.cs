﻿using UnityEngine;
using UnityEngine.InputSystem;
public class XboxController : MonoBehaviour
{
    [HideInInspector]
    public XboxInput controls;
    [HideInInspector]
    public Vector2 m_LeftStick;
    [HideInInspector]
    public Vector2 m_RightStick;
    [HideInInspector]
    public Vector2 m_DPad;
    [HideInInspector]
    public float m_R2;
    [HideInInspector]
    public float m_L2;

    private static XboxController instance;

    public static XboxController GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;
        controls = new XboxInput();
        controls.XboxController.LeftStick.performed += ctx => m_LeftStick = ctx.ReadValue<Vector2>();
        controls.XboxController.LeftStick.canceled += ctx => m_LeftStick = Vector2.zero;

        controls.XboxController.RightStick.performed += ctx => m_RightStick = ctx.ReadValue<Vector2>();
        controls.XboxController.RightStick.canceled += ctx => m_RightStick = Vector2.zero;

        controls.XboxController.DPad.performed += ctx => m_DPad = ctx.ReadValue<Vector2>();
        controls.XboxController.DPad.canceled += ctx => m_DPad = Vector2.zero;

        controls.XboxController.R2.performed += ctx => m_R2 = ctx.ReadValue<float>();
        controls.XboxController.R2.canceled += ctx => m_R2 = 0;

        controls.XboxController.L2.performed += ctx => m_L2 = ctx.ReadValue<float>();
        controls.XboxController.L2.canceled += ctx => m_L2 = 0;
    }

    private void Start()
    {
        foreach (Gamepad pad in Gamepad.all)
        {
            Log.LogMe(" name " + pad.name + " device " + pad.device + " displayName " + pad.displayName + " description " + pad.description + " deviceId " + pad.deviceId );
        }
    }
    private void OnR1()
    {
        CameraController.GetInstance().CycleThoughCamera();
       // Debug.Log(input.id);
    }
    private void OnL1()
    {
        //Debug.Log(context.control.device);
        CameraController.GetInstance().resetCameraTilt();
    }

    private void OnY()
    {
        CameraController.GetInstance().updateMaxMovementSpeed(2);
        //Debug.Log("Y");
        //CameraController.GetInstance().CycleThoughCamera(0);
    }

    private void OnX()
    {
        CameraController.GetInstance().SmoothToggle();
        //CameraController.GetInstance().CycleThoughCamera(1);
    }

    private void OnA()
    {
        //Debug.Log("A");
        CameraController.GetInstance().updateMaxMovementSpeed(-2);

        // CameraController.GetInstance().CycleThoughCamera(2);
    }

    private void OnB()
    {
        //Debug.Log("A");
        UIController.GetInstance().toggleWorkingUI();

        // CameraController.GetInstance().CycleThoughCamera(2);
    }

    private void OnControlsChanged()
    {
        Log.LogMe("Xbox Controller Connected");
    }
    private void OnDeviceLost()
    {
        Log.LogMe("Xbox Controller Disconnected");
    }

    private void OnDeviceRegained()
    {
        Log.LogMe("Xbox Controller Reconnected");
    }

    private void OnToggleGrid()
    {
        ObjectController.GetInstance().ToggleGrid();
    }

    private void OnEnable()
    {
        controls.XboxController.Enable();
    }
    private void OnDisable()
    {
        controls.XboxController.Disable();
    }
}
