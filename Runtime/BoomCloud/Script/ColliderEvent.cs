using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
public class ColliderEvent : MonoBehaviour
{
    public string eventToTrigger;

    public static event System.Action CameraCollide;

    public UnityEvent OnCollision;
    public bool localEventOnly = true;


    public void OnTriggerEnter(Collider other)
    {
        CameraCollide?.Invoke();
        OnCollision.Invoke();

        int i = 0;
        Debug.Log("COLLISION!!");

        if (eventToTrigger == null)
            return;


       LogicController.dataScene.actionsCues.ToList().ForEach(a => {
            if(a.cueName == eventToTrigger)
            {
                Debug.Log("Found matching cue " + LogicController.dataScene.actionsCues[i].cueName);
                LogicController.SetActiveActionCueState(i);
                CueController.GetInstance().goToCue(i, localEventOnly); //Only local event on collision!
                return;
            }
            i++;
        });
    
    }
}
