﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;
using Cinemachine;
using System.Linq;


public class SceneController : MonoBehaviour
{
    [Header("MAIN AUDIO")]
    public AudioSource mainAudio = null;
    [Header("CUE TO START WITH SCENE (NUMBER)")]
    public int cueToStart =-1;
    [Header("CAMERA CUES")]
    public VirtualCameraCueList[] cuesCamerasList = new VirtualCameraCueList[0];
    [Header("QUALISYS PROPS")]
    public GameObject[] PropsList;
    [Header("ACTION CUE")]
    public ActionCuesList[] actionCuesList = new ActionCuesList[0];
    [Header("AUDIO CUE")]
    public AudioCuesList[] audioCuesList = new AudioCuesList[0];
    /*
    [Header("DISSOLVE GAMEOBJECT")]
    public GameObject dissolveGameObject;
    */
    public static event System.Action<DataScene> InitScene;

    private List<GameObject> gridObject;

    DataScene dataScene = new DataScene();

    float lateStartTime; 

    private void Awake()
    {
        gridObject = new List<GameObject>();
    }

    public void Start()
    {
        lateStartTime = LogicController.GetInstace().lateStartTime;
        StartCoroutine(LateStart());
    }
    private bool DoesTagExist(string aTag)
    {
        try
        {
            GameObject.FindGameObjectsWithTag(aTag);
            return true;
        }
        catch
        {
            return false;
        }
    }
    IEnumerator LateStart()
    {
        yield return new WaitForSeconds(lateStartTime);

        dataScene.CamerasCuesList = cuesCamerasList;
        dataScene.actionsCues = actionCuesList;
        dataScene.PropsList = PropsList;
        dataScene.audioCues = audioCuesList;
        dataScene.mainAudio = mainAudio;
        if (DoesTagExist("Grid"))
        {
            GameObject.FindGameObjectsWithTag("Grid").ToList().ForEach(g => gridObject.Add(g));
            dataScene.Grid = gridObject;
        }

        /*
        if(dissolveGameObject != null)
        dataScene.dissolveController = dissolveGameObject;
        */
        //LoadingController.GetInstance().realtimeScene.ForEach(s => dataScene.SpaceLoaderList.Add(s.name));

        if (MidiLedController.GetInstance() != null)
        {
            dataScene.matrix = MidiLedController.GetInstance().matrixSize;
        }        

        InitScene?.Invoke(dataScene);

        //Init scene and make it active
        
        if (SceneManager.GetSceneByName(this.gameObject.scene.name).isLoaded)
        {
            SceneManager.SetActiveScene(this.gameObject.scene);
        }
        
        foreach (VirtualCameraCueList cameraCueList in cuesCamerasList)
        {
            cameraCueList.UpdateDict();
        }

        yield return null;
        // ObjectController.GetInstance().randomizeAlienAvatar();
        if(cueToStart != -1 && CueController.GetInstance() != null)
        {
            CueController.GetInstance().goToCue(cueToStart);
           // MidiLedController.GetInstance().startUpCueUpdate(cueToStart);
        }
        LogicController.GetInstace().lateStartTime = 0;
    }
}



