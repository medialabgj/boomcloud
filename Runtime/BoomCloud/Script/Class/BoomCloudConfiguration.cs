using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct BoomCloudConfiguration {

    public BoomCloudSettings.BoomCloudSyncMode SyncMode;
}
