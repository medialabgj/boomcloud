﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioCuesList
{
    [Header("Nom du Cue")]
    public string cueName;

    [Header("Audio To Play")]
    public AudioPlaySettings[] audioToPlay;

    [Header("Audio To Stop")]
    public AudioStopSettings[] audioToStop;

}

[System.Serializable]
public class AudioStopSettings
{
    [Header("Audio Source")]
    public AudioSource source;

    [Header("Fade Out Time")]
    public int transitionTime;
}


[System.Serializable]
public class AudioPlaySettings
{
    [Header("Audio Source")]
    public AudioSource source;

    [Header("Fade In Time")]
    public int transitionTime;

    [Header("Start Time in Seconds")]
    public int startTime = 0;

    [Header("Volume (0-1)")]
    public float volume = 1;
}