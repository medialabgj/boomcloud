﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Cinemachine;
using System;
using System.Linq;

public class DataScene
{
    public enum CueStatsType { noCue, cue, activeCue, disable, previousCue, customColor };

    public struct CueStats
    {
        public CueStatsType state;

        public CueStats(CueStatsType _cueStats)
        {
            state = _cueStats;
        }
    }

    public enum CueType { NaN, audio, camera, action, cameraGroupe, cueNumber, empty, space };

    public VirtualCameraCueList[] CamerasCuesList { get; set; }
    public Dictionary<int, PreviousCameraState> previousActiveCamera { get; set; } = new Dictionary<int, PreviousCameraState>();
    public Dictionary<int, int> CameraRowLayer { get; set; } = new Dictionary<int, int>();
    public Dictionary<int, int> CameraLayerRow { get; set; } = new Dictionary<int, int>();

    public Dictionary<int, LogicController.LedColorPicker> cameraColor { get; set; } = new Dictionary<int, LogicController.LedColorPicker>();
    public LogicController.LedColorPicker actionLedColor;
    public LogicController.LedColorPicker audioLedColor;
    public LogicController.LedColorPicker spaceLedColor;
    //public LogicController.LedColorPicker cameraGroupeColor;

    public int actionRow { get;  set; }
    public int audioRow { get; set; }
    public int sceneRow { get; set; }
    public int groupeRow { get; set; }
    public bool useCamGroupOnLed { get; set; }
    public AudioSource mainAudio { get; set; }
    public Vector2 matrix { get; set; }
    public ActionCuesList[] actionsCues { get; set; }
    public GameObject[] PropsList;
    public List<GameObject> Grid;
    public Dictionary<int, List<CameraState>> camerasState { get; set; } = new Dictionary<int, List<CameraState>>();
    public Dictionary<int, CueStatsType> actionsCuesState { get; set; } = new Dictionary<int, CueStatsType>();
    public Dictionary<int, CueStatsType> audiosCuesState { get; set; } = new Dictionary<int, CueStatsType>();
    public Dictionary<int, CueStatsType> spacesCuesState { get; set; } = new Dictionary<int, CueStatsType>();
    public List<string> sceneLoaderList { get; set; } = new List<string>();
    public GameObject dissolveController { get; set; }
    public AudioCuesList[] audioCues { get; set; }

    //Return the layer index of camera inside Cue List
    public List<int> GetAllDifferentCamera() 
    {
        //// OLD WAY
        if(CamerasCuesList == null)
        {
            Debug.LogError("No Camera Cue set -> you need to add at least one Camera on the SceneController CUE script");
            return null;
        }

        var allCameraLayerIndex = new List<int>();

        CamerasCuesList.ToList().ForEach(c => {
            int i = 0;
            c.virtualCamerasCue.ToList().ForEach(cc => {
                if (cc == null)
                {
                    Debug.LogError("BoomCloud: Missing element N°  "+  i  +"  in cue " + c.cueName + " please delete entry or fill with valide virtual Camera");
                }
                    

                allCameraLayerIndex.Add(cc.gameObject.layer);
                i++;
            });
        });

        return allCameraLayerIndex.Distinct().ToList();

        /*
        var layerIndexList = new List<int>();

        BoomCloudSettings.GetInstance().cameraSettings.ForEach(c => layerIndexList.Add(Mathf.RoundToInt(Mathf.Log(c.layer.value, 2))));
        return layerIndexList.Distinct().ToList();
        */

    }
}


public class CameraState
{
    public DataScene.CueStatsType cueState { get; set; } = new DataScene.CueStatsType();
    public CinemachineVirtualCamera cameraLink { get; set; }

}

public class PreviousCameraState
{
    public int previousIndex { get; set; }
    public CinemachineVirtualCamera cameraLink { get; set; } 
}
