using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CueIdentifier : MonoBehaviour
{
    //public enum CueType {NaN, audio, camera, action, scene, cameraGroupe };

    //public CueType cueType;

    public int cueIndex;

    public int cameraLayer;

    public DataScene.CueType cueType;

    public GameObject numberPanel;

    public void goToCue()
    {
        switch (cueType)
        {
            case DataScene.CueType.audio:
                goToAudioCue();
                break;


            case DataScene.CueType.camera:
                goToCameraCue(cameraLayer);
                break;

            case DataScene.CueType.action:
                goToActionCue();
                break;

            case DataScene.CueType.cameraGroupe:
                goToCameraGroupe();
                break;

            case DataScene.CueType.NaN:
                Debug.LogWarning("Cue Type not assigned");
                break;

            case DataScene.CueType.empty:
               // Debug.Log("Empty Cue");
                break;
        }

    }

    private void goToAudioCue()
    {
        //Debug.Log("GO TO CUE " + cueType.ToString() + " at index " + cueIndex);
        CueController.GetInstance().goToAudioCue(cueIndex - 1);
        MidiLedController.GetInstance().exteralMidiUpdate(cueIndex -1, -2);
    }

    private void goToCameraGroupe()
    {
        /*
        LogicController.dataScene.GetAllDifferentCamera().ForEach(c => {

            goToCameraCue(c);
            Debug.Log("GADC FUNCTION " + c);

        });
        */

        BoomCloudSettings.GetInstance().cameraSettings.ForEach(c => goToCameraCue(Mathf.RoundToInt(Mathf.Log(c.layer.value, 2))));


    }

    private void goToCameraCue(int layer)
    {
        // Debug.Log("GO TO CUE " + cueType.ToString() + " at index " + cueIndex);
        CameraController.GetInstance().goToCameraCue(cueIndex, layer);
        UIController.GetInstance().UpdateUiCamera(cueIndex, layer);
        MidiLedController.GetInstance().exteralMidiUpdate(cueIndex, layer);

        // CueController.GetInstance().goToAudioCue(cueIndex - 1);
    }

    private void goToActionCue()
    {
        //Debug.Log("GO TO CUE " + cueType.ToString() + " at index " + cueIndex);
        CueController.GetInstance().goToCue(cueIndex - 1);
        MidiLedController.GetInstance().exteralMidiUpdate(cueIndex, -1);
        // CueController.GetInstance().goToAudioCue(cueIndex - 1);
    }

}
