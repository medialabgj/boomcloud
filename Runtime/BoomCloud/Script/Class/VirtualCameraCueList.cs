﻿using Cinemachine;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;


[System.Serializable]
public class VirtualCameraCueList
{
    [Header("CAMERA CUE")]
    public string cueName;
    public CinemachineVirtualCamera[] virtualCamerasCue;
    [Header("Layer To Display")]
    public LayerMask[] layerToDisplay;
    [Header("Layer To Hide")]
    public LayerMask[] layerToHide;
    [Header("Ajouter nom du cue à démarrer & caméra correspondante")]
    public string objectCueNameToSynchronize;
    public Dictionary<int, CinemachineVirtualCamera> virtualCamerasCueDict = new Dictionary<int, CinemachineVirtualCamera>();


    public void UpdateDict()
    {
        virtualCamerasCueDict = virtualCamerasCue.ToDictionary(c => c.gameObject.layer, c => c);        
    }
}
