﻿using UnityEngine;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

[System.Serializable]
public class ActionCuesList
{
    [Header("Cue Name")]
    public string cueName;
    //public int cueID;
    //[Header("CUE pour la scene")]
    [Header("Object to display on All Screen")]
    public GameObject[] objectToDisplpayOnAllScreen;
    [Header("Object to hide on All Screen")]
    public GameObject[] objectToHideOnAllScreen;
    [Header("Object to move on different layer")]
    public MoveToLayer[] objectToMoveToLayer;
    [Header("Camera Change Culling")]
    public ChangeCameraCulling[] changeCameraCullings;
    [Header("Audio CUE to syncronize")]
    public string audioCueNameToSync;
    [Header("Cue Color")]
    public LogicController.LedColorPicker ledColor;
}


[System.Serializable]
public class MoveToLayer 
{
    [Header("Object To Move")]
    public GameObject[] objectsToMove;
    [Header("Destination Layer")]
    public LayerMask targetLayer;
}


[System.Serializable]
public class ChangeCameraCulling
{
    [Header("Camera To Change")]
    public CinemachineVirtualCamera camera;
    [Header("Layer to add")]
    public LayerMask[] layerToAdd;
    [Header("Layer to remove")]
    public LayerMask[] layerToRemove;
}