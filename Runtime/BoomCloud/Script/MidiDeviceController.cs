﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using Minis;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.Events;

public enum CueType { Scene, Cameras, Object, Audio }

public enum CueDirection { Previous = -1, Next = 1}

public enum SpeedFor { MasterVP, CamVP_1, All }

public struct CueAction
{
    public CueType type;
    public int value;
    public int layer;

    public CueAction(CueType _cueType, int _value = -1, int _layer = -1)
    {
        type = _cueType;
        value = _value;
        layer = _layer;
    }
}


public struct DollyControl
{
    public SpeedFor cam;
    public float value;

    public DollyControl(SpeedFor _cam, float _value = -1)
    {
        cam = _cam;
        value = _value;
    }
}

public class MidiDeviceController : MonoBehaviour
{
    private MidiInput controls;
    [HideInInspector]
    public MidiValueControl m_TestCtrl;

    public string targetDevice;

    public static event System.Action<CueAction> ControlChange;
    public static event System.Action ToggleGrid;
    public static event System.Action DeleteClonedDanser;


    public static event System.Action <DollyControl> DollyChange;


    //public static event System.Action ConnectQualysis;
    public static event System.Action<int> TraverseQueue;

    private void Awake()
    {
        controls = new MidiInput();
    }


    void Start()
    {
        InputSystem.onDeviceChange += (device, change) =>
        {
            var midiDevice = device as Minis.MidiDevice;
            if (midiDevice == null) return;

                midiDevice.onWillNoteOn += (note, velocity) => {

                    if (string.IsNullOrEmpty(targetDevice) || !note.device.description.product.Contains(targetDevice))
                    {
                        return;
                    }

                    Log.LogMe("Device " + note.device.description.product + " note matching target " + targetDevice);
                };
        };
    }
    void OnNoteOn()
    {
        Debug.Log("FIRE");
    }

    void MatrixMide(int noteIndex)
    {

    }

    void OnTestCTRL(InputValue value)
    {
       // var data = context.ReadValue<MidiValueControl>();

      //  Debug.Log(value.Get());
    }

    /***********************
    //MidiValueControl
    private void OnMasterDollySpeed(InputValue value)
    {
        Debug.Log(value);
    }

    private void VP_1DollySpeed(float e)
    {
       // Debug.Log(e);
    }

    *******************/
    /**********NEW WAY *********/

    private void OnLaunchPadActions(InputValue value)
    {
      // Debug.Log(value.Get<MidiDevice>().GetNote(value));
    }



    /***********OLD WAY *********/
    private void OnLoadShadowScene()
    {
        LoadingController.GetInstance().LoadScene(0);       
    }

    private void OnLoadParticlesScene()
    {
        LoadingController.GetInstance().LoadScene(1);
    }
    private void OnLoadStudioScene()
    {
        LoadingController.GetInstance().LoadScene(2);
    }
    private void OnLoadAvatarScene()
    {
        LoadingController.GetInstance().LoadScene(3);
    }

    private void OnLoadGrillesScene()
    {
        LoadingController.GetInstance().LoadScene(4);
    }
    private void OnLoadCameraTest()
    {
        LoadingController.GetInstance().LoadScene(5);
    }

    private void OnLoadPanScene()
    {
        LoadingController.GetInstance().LoadScene(6);
    }

    private void OnGoToNextCameraCue()
    {
        CameraController.GetInstance().goToNextCameraCue();
    }

    private void OnGoToPreviousCameraCue()
    {
        CameraController.GetInstance().goToPreviousCameraCue();
    }

    private void OnConnectToQualisys()
    {        
       // QualisysAutoConnect.GetInstance().Connect();
    }
    private void OnResetScene()
    {
        LoadingController.GetInstance().ResetAll();
    }

    private void OnGoToNextCue()
    {
        TraverseQueue?.Invoke(1);
        //CueController.GetInstance().goToNextCue();
    }

    private void OnGoToPreviousCue()
    {
        TraverseQueue?.Invoke(-1);
        //CueController.GetInstance().goToPreviousCue();
    }

    private void On6dofBool()
    {
       // var onoff = RTClient.GetInstance().Toggle6Dof;
       // RTClient.GetInstance().Toggle6Dof = !onoff;
    }

    /*
    private void OnRefreshAim()
    {
      //  RTClient.GetInstance().RefreshAim();
    }
    */

    /******SCENE CUE******/

    private void OnGoToSceneCue_1()
    {
        //CueController.GetInstance().goToCue(0);
        ControlChange?.Invoke(new CueAction(CueType.Object, 0));
    }

    private void OnGoToSceneCue_2()
    {
        // CueController.GetInstance().goToCue(1);
        ControlChange?.Invoke(new CueAction(CueType.Object, 1));

    }

    private void OnGoToSceneCue_3()
    {
        //CueController.GetInstance().goToCue(2);
        ControlChange?.Invoke(new CueAction(CueType.Object, 2));

    }

    private void OnGoToSceneCue_4()
    {
        // CueController.GetInstance().goToCue(3);
        ControlChange?.Invoke(new CueAction(CueType.Object, 3));

    }

    private void OnGoToSceneCue_5()
    {
        // CueController.GetInstance().goToCue(4);
        ControlChange?.Invoke(new CueAction(CueType.Object, 4));

    }

    private void OnGoToSceneCue_6()
    {
        // CueController.GetInstance().goToCue(5);
        ControlChange?.Invoke(new CueAction(CueType.Object, 5));

    }

    private void OnGoToSceneCue_7()
    {
        //CueController.GetInstance().goToCue(6);
        ControlChange?.Invoke(new CueAction(CueType.Object, 6));
    }

    private void OnGoToSceneCue_8()
    {
        //CueController.GetInstance().goToCue(7);
        ControlChange?.Invoke(new CueAction(CueType.Object, 7));
    }

    /******CAMERA CUE*****/

    private void OnGoToVP_1CameraCue_1()
    {
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 0, 8));
        //CameraController.GetInstance().goToCameraCue(-1, 8);
    }
    private void OnGoToVP_1CameraCue_2()
    {
        //CameraController.GetInstance().goToCameraCue(0, 8);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 1, 8));
    }
    private void OnGoToVP_1CameraCue_3()
    {
        //CameraController.GetInstance().goToCameraCue(1, 8);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 2, 8));
    }
    private void OnGoToVP_1CameraCue_4()
    {
        //CameraController.GetInstance().goToCameraCue(2, 8);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 3, 8));
    }
    private void OnGoToVP_1CameraCue_5()
    {
        //CameraController.GetInstance().goToCameraCue(3, 8);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 4, 8));
    }
    private void OnGoToVP_1CameraCue_6()
    {
        //CameraController.GetInstance().goToCameraCue(4, 8);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 5, 8));
    }
    private void OnGoToVP_1CameraCue_7()
    {
        //CameraController.GetInstance().goToCameraCue(5, 8);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 6, 8));
    }
    private void OnGoToVP_1CameraCue_8()
    {
        //CameraController.GetInstance().goToCameraCue(6, 8);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 7, 8));
    }

    private void OnGoToMasterCameraCue_1()
    {
        //CameraController.GetInstance().goToCameraCue(-1,10);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 0, 10));
    }
    private void OnGoToMasterCameraCue_2()
    {
        //CameraController.GetInstance().goToCameraCue(0, 10);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 1, 10));
    }
    private void OnGoToMasterCameraCue_3()
    {
        //CameraController.GetInstance().goToCameraCue(1, 10);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 2, 10));
    }
    private void OnGoToMasterCameraCue_4()
    {
        //CameraController.GetInstance().goToCameraCue(2, 10);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 3, 10));
    }
    private void OnGoToMasterCameraCue_5()
    {
        //CameraController.GetInstance().goToCameraCue(3, 10);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 4, 10));
    }
    private void OnGoToMasterCameraCue_6()
    {
        //CameraController.GetInstance().goToCameraCue(4, 10);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 5, 10));
    }
    private void OnGoToMasterCameraCue_7()
    {
        //CameraController.GetInstance().goToCameraCue(5, 10);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 6, 10));
    }
    private void OnGoToMasterCameraCue_8()
    {
        //CameraController.GetInstance().goToCameraCue(6, 10);
        ControlChange?.Invoke(new CueAction(CueType.Cameras, 7, 10));
    }


    public void OnToggleGrid()
    {
       // ObjectController.GetInstance().ToggleGrid();
        ToggleGrid?.Invoke();
       // Debug.Log("TOGGLE");
    }

    private void OnLoopSus()
    {
      //  DuplicatorController.GetInstance().ToggleStopWatch();
    }

    private void OnDuplicateSUS()
    {
        ObjectController.GetInstance().DuplicationStart("SUS");
    }

    private void OnDuplicateMAE()
    {
        ObjectController.GetInstance().DuplicationStart("MAE");
    }

    private void OnDuplicateGIL()
    {
        ObjectController.GetInstance().DuplicationStart("GIL");
    }

    private void OnDeleteDanser()
    {
        DeleteClonedDanser?.Invoke();
    }

    private void OnAvatarSelectorSUS(InputValue value)
    {
        ObjectController.GetInstance().navigateThroughAvatar(value.Get<float>(), "SUS");
    }

    private void OnAvatarSelectorMAE(InputValue value)
    {
        ObjectController.GetInstance().navigateThroughAvatar(value.Get<float>(), "MAE");
    }

    private void OnAvatarSelectorRUD(InputValue value)
    {
        ObjectController.GetInstance().navigateThroughAvatar(value.Get<float>(), "RUD");
    }

    private void OnAvatarSelectorJOZ(InputValue value)
    {
        ObjectController.GetInstance().navigateThroughAvatar(value.Get<float>(), "JOZ");
    }

    private void OnAvatarSelectorDYA(InputValue value)
    {
        ObjectController.GetInstance().navigateThroughAvatar(value.Get<float>(), "DYA");
    }

    private void OnAvatarSelectorVIC(InputValue value)
    {
        ObjectController.GetInstance().navigateThroughAvatar(value.Get<float>(), "VIC");
    }

    private void OnNextPage()
    {
        MidiLedController.GetInstance().UpdateCurrentPage(1);
    }

    private void OnPreviousPage()
    {
        MidiLedController.GetInstance().UpdateCurrentPage(-1);
    }

    private void OnMasterCamDollyTimeline(InputValue value)
    {
        DollyChange?.Invoke(new DollyControl(SpeedFor.MasterVP , value.Get<float>()));

        //Debug.Log(value.Get());
    }

    private void OnVP_1CamDollyTimeline(InputValue value)
    {
        DollyChange?.Invoke(new DollyControl(SpeedFor.CamVP_1, value.Get<float>()));

        // Debug.Log(value.Get());
    }

    private void OnALLCamDollyTimeline(InputValue value)
    {
        DollyChange?.Invoke(new DollyControl(SpeedFor.All, value.Get<float>()));

        // Debug.Log(value.Get());
    }

    private void OnDissolve(InputValue value)
    {
        CueController.GetInstance().makeDissolve(value.Get<float>());
    }

    private void OnUIToggle()
    {
        UIController.GetInstance().toggleUiDisplay();
    }

    private void OnFaderChange(InputValue value)
    {
       // CueController.GetInstance().cameraScaleFactore(value.Get<float>());
       // Debug.Log(value.Get<float>());
    }

    private void OnRotationChange(InputValue value)
    {
        CueController.GetInstance().cameraRotationFactore(value.Get<float>());
        //Debug.Log(value.Get<float>());
    }

    private void OnKeyNextCue()
    {
        CameraController.GetInstance().goToNextCameraCue();
    }

    private void OnKeyPreviousCue()
    {
        CameraController.GetInstance().goToPreviousCameraCue();
    }
}
